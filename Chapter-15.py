#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys
import math
import re # regular expression
from decimal import *
from lib import *

# Chapter 15: Electric Field of Distributed Charges
# from Matter & Interactions (4th ed., 2015) 
# by Ruth W. Chabay and Bruce A. Sherwood

def Problem22():
  print "A plastic rod 1.6 m long is rubbed all over with wool, and acquires a charge of -9E-8 coulombs. We choose the center of the rod to be the origin of our coordinate system, with the x-axis extending to the right, the y-axis extending up, and the z-axis out of the page. In order to calculate the electric field at location A = < 0.7, 0, 0 > m, we divide the rod into 8 pieces, and approximate each piece as a point charge located at the center of the piece."
  L = 1.6 # length of rod, in meters
  Q = -9.0e-8 # charge of rod in coulombs
  A = (0.7, 0, 0) # observation location, in meters
  n = 8.0 # sub-sections of rod

  print "(a) What is the length of one of these pieces?"
  
  length = L / n
  print "{} m".format(pPrint(length))


  # Note: segment 8 is at the bottom (in the -y direction)
  print "(b) What is the location of the center of piece number 8?"

  rmid8 = (0.0, (-1) * ((3.0 * length) + (length / 2.0)), 0.0)
  print "{} m".format(pPrint(rmid8))


  print "(c) How much charge is on piece number 8? (Remember that the charge is negative.)"

  deltaQ = (length / L) * Q
  print "{} coulombs".format(pPrint(deltaQ))
  

  print "(d) Approximating piece 8 as a point charge, what is the electric field at location A due only to piece 8?"
  x = A[0]
  y = rmid8[1]
  E = ((ke * (x * deltaQ) / (x**2 + y**2)**(3.0/2.0)), (ke * (-y * deltaQ) / (x**2 + y**2)**(3.0/2.0)), 0.0)
  print "E 8 = {} N/C".format(pPrint(E))
  

def Problem31():
  print "Two rings of radius 5 cm are 18 cm apart and concentric with a common horizontal x-axis. The ring on the left carries a uniformly distributed charge of +33 nC, and the ring on the right carries a uniformly distributed charge of -33 nC. What is the magnitude and direction of the electric field on the x-axis, halfway between the two rings?"
  R = 5.0e-2 # radius of rings in meters
  d = 18.0e-2 # distance between rings in meters
  qL = 33.0e-9 # charge of ring on left, in coulombs (given in nano coulombs)
  qR = -33.0e-9 # charge of ring on right, in coulombs (given in nano coulombs)

  z = d / 2.0
  Eleft = ke * (qL * z) / (R**2 + z**2)**(3.0/2.0)
  Eright = ke * (qR * z) / (R**2 + z**2)**(3.0/2.0)
  Enet = Eleft + abs(Eright)

  print "{} N/C".format(pPrint(Enet))
  
def Problem28():
  print "A strip of invisible tape 0.11 m long by 0.012 m wide is charged uniformly with a total net charge of 7 nC (nano = 1 ×10−9) and is suspended horizontally, so it lies along the x axis, with its center at the origin, as shown in the figure below. Calculate the approximate electric field at location <0, 0.04, 0> m (location A) due to the strip of tape. Do this by dividing the strip into three equal sections, as shown in the figure below, and approximating each section as a point charge. (Assume each point charge is located at the center of the section it approximates. Express your answer in vector form.)"
  L = 0.11 # length of tape, in meters
  W = 0.012 # width of tape, in meters
  Q = 7.0e-9 # charge of tape, in coulombs (provided in nano coulombs) 
  A = (0.0, 0.04, 0.0) # observation location in meters
  
  q = Q / 3.0 # charge of a single segment
  length = L / 3.0 # length of a single segment

  r1 = subtractVectors(A, (length * -1.0, 0.0, 0.0))
  r1mag = magnitudeOfVector(r1)
  r1hat = unitVector(r1)
  E1 = multiplyVectorByScalar(r1hat, ke * q / r1mag**2)
  print "(a) What is the approximate electric field at due to piece 1?"
  print "E1 = {} N/C".format(pPrint(E1))


  E2 = (0, ke * (q / A[1]**2), 0)
  print "(b) What is the approximate electric field at A due to piece 2?"
  print "E2 = {} N/C".format(pPrint(E2))


  r3 = subtractVectors(A, (length, 0.0, 0.0))
  r3mag = magnitudeOfVector(r3)
  r3hat = unitVector(r3)
  E3 = multiplyVectorByScalar(r3hat, ke * q / r3mag**2)
  print "(c) What is the approximate electric field at A due to piece 3?"
  print "E3 = {} N/C".format(pPrint(E3))

  Enet = addVectors(addVectors(E1, E2), E3)
  print "(d) What is the approximate net electric field at A?"
  print "E net = {} N/C".format(pPrint(Enet))



def Problem36():
  R = 42.0e-2 # disk radius, in meters (given in cm)
  Q = 3.0e-6 # charge across disk, in coulombs
  z = 4.0e-3 # observation location, in meters (given in mm)

  vacuumPermittivity = 8.85e-12 # the ε0 in Coulomb's constant
  A = math.pi * R**2 # A for area

  Efirst = ((Q/A)/(2.0 * vacuumPermittivity)) * (1.0 - (z / (R**2 + z**2)**(1.0/2.0)))
  print "E first = {} N/C".format(pPrint(Efirst))

  Esecond = ((Q/A)/(2.0 * vacuumPermittivity)) * (1.0 - (z / R))
  print "E second = {} N/C".format(pPrint(Esecond))

  Ethird = ((Q/A)/(2.0 * vacuumPermittivity))
  print "E third = {} N/C".format(pPrint(Ethird))


  print "For the same disk, calculate E at a distance of 5 cm (50 mm) using all three equations."
  z = 5.0e-2 # new observation location, in meters (given in cm)

  Efirst = ((Q/A)/(2.0 * vacuumPermittivity)) * (1.0 - (z / (R**2 + z**2)**(1.0/2.0)))
  print "E first = {} N/C".format(pPrint(Efirst))

  Esecond = ((Q/A)/(2.0 * vacuumPermittivity)) * (1.0 - (z / R))
  print "E second = {} N/C".format(pPrint(Esecond))

  Ethird = ((Q/A)/(2.0 * vacuumPermittivity))
  print "E third = {} N/C".format(pPrint(Ethird))



def Problem40(): 
  print "A thin circular sheet of glass of diameter 6 meters is rubbed with a cloth on one surface, and becomes charged uniformly. A chloride ion (a chlorine atom which has gained one extra electron) passes near the glass sheet. When the chloride ion is near the center of the sheet, at a location 0.8 mm from the sheet, it experiences an electric force of 6E-15 N, toward the glass sheet. In this problem, use the value ε0 = 8.85e-12 C2/(N·m2)"

  R = 6.0 / 2.0 # radius of disk, in meters 
  z = 0.8e-3 # obs. location, in meters
  F = 6.0e-15 # electrical force, in Newtons

  q = chargeOfElectron # charge of chloride ion (due to one extra electron), in coulombs

  vacuumPermittivity = 8.85e-12 # the ε0 in Coulomb's constant
  A = math.pi * R**2 # A for area

  print "(c) How much charge is on the surface of the glass disk? (give amount, including sign, and correct units)"
  # F = q * E
  # (E = Q/A / 2ε0)
  # F = q * Q/A / 2ε0
  # q * Q/A = F * 2ε0
  # Q = (F * 2ε0 * A) / q
  Q = (F * (2.0 * vacuumPermittivity) * A) / q
  print "{} C".format(pPrint(Q))


def Problem45():
  print "In a cathode-ray tube (CRT), an electron travels in a vacuum and enters a region between two \"deflection\" plates which have equal and opposite charges. The dimensions of each plate are L = 11 cm by d = 4 cm, and the gap between them is h = 2.5 mm. (Note: the diagram is not drawn to scale and the direction of the electric field may not be correct, depending on your randomization.)"
  L = 11.0e-2 # length of plate, in meters (given in cm)
  d = 4.0e-2 # depth of plate, in meters (given in cm)
  h = 2.5e-3 # height between plates, in meters (given in mm)

  print "During a 0.001 s interval while it is between the plates, the change of the momentum of the electron Δp is < 0, 9.76E-17, 0 > kg m/s."
  deltaT = 0.001 # time interval seconds
  deltaP = (0.0, 9.76e-17, 0.0) # change in momentum of electron, in kg m/s
  q = chargeOfElectron 
  
  print "What is the electric field between the plates?"
  
  # F = delta p / delta t
  # F = E * q
  # E = F / q
  # E = delta p / delta t * q
  E = divideVectorByScalar(deltaP, deltaT * q)

  print "E = {} N/C".format(pPrint(E))

  # E = q / epsilon-zero * A
  # q = E * epsilon-zero * A
  print "What is the charge (both magnitude and sign) of the upper plate?"
  vacuumPermittivity = 8.85e-12 # the ε0 in Coulomb's constant
  A = L * d
  Emag = magnitudeOfVector(E)
  q = Emag * vacuumPermittivity * A
  print "q = {} C".format(pPrint(q))


def Problem51():
  print "A thin plastic spherical shell of radius 5 cm has a uniformly distributed charge of −25 nC on its outer surface. A concentric thin plastic spherical shell of radius 8 cm has a uniformly distributed charge of +64 nC on its outer surface. Find the magnitude and direction of the electric field at distances of 1.1 cm, 6.4 cm, and 12.2 cm from the center. See the figure below."
  R1 = 5.0e-2 # inner sphere radius
  Q1 = -25e-9 # inner sphere charge
  R2 = 8.0e-2 # outer sphere radius
  Q2 = 64.0e-9 # outer sphere charge

  r1 = 1.1e-2 
  r2 = 6.4e-2
  r3 = 12.2e-2  

  E1 = 0 # Gauss's law - inside the sphere charge is 0

  E2 = ke * Q1 / r2**2 
  print "E (r=6.4 cm): magnitude {} N/C".format(pPrint(abs(E2)))

  E3 = ke * (Q1 + Q2) / r3**2
  print "E (r = 12.2 cm): magnitude {} N/C".format(pPrint(abs(E3)))

  

  

def Problem60():
  print "A very thin glass rod 4 m long is rubbed all over with a silk cloth (see figure below). It gains a uniformly distributed charge +2.7 × 10−6 C. Two small spherical rubber balloons of radius 1.2 cm are rubbed all over with wool. They each gain a uniformly distributed charge of -4.8 × 10−8 C. The balloons are near the midpoint of the glass rod, with their centers 3 cm from the rod. The balloons are 2 cm apart (4.4 cm between centers)."
  L = 4.0 # meters
  q1 = 2.7e-6 # charge of rod in coulombs
  R = 1.2e-2 # meters
  q2 = -4.8e-8 # charge of spheres in coulombs
  rtorod = 3.0e-2 # distance between center of balloon and rod, in meters
  d = 2.0e-2 # distance between centers of balloons, in meters 

  # another example: 
  # q1 = 2.3e-6
  # q2 = -3.0e-8

  # another example: 
  # q1 = 1.6e-6
  # q2 = -7.0e-8
  # print "E sphere should be 4.36e5"
  # print "E rod should be -2.4e5"
  
  print "(a) Find the magnitude of the electric field at the location marked by the ×, 0.6 cm to the right of the center of the left balloon."
  displacement = 0.6e-2 # displacement from center of sphere in meters

  # !!! remember to treat the right balloon as a point charge: 
  # distance to the right sphere is to its center, not surface
  rtosphere = displacement + d + R
  # print "r to sphere = {} m".format(pPrint(rtosphere))

  # Esphere = k * q / |r|^2
  Espheremag = ke * (abs(q2) / rtosphere**2)
  # print "Esphere = {}".format(pPrint(Espheremag))

  # E rod = k * Q / r * sqrt(r^2 + (L/2)^2)
  # Erod = ke * (q1 / (r2 * math.sqrt(r2**2 + (L/2.0)**2)))
  # E rod = ke * (2 Q / L) / r
  Erodmag = ke * (2.0 * q1 / L) / rtorod
  # print "Erod = {}".format(pPrint(Erodmag))

  Enetmag = math.sqrt(Erodmag**2 + Espheremag**2)
  print "{} N/C".format(pPrint(Enetmag))
  
  # down and to the right, so the degrees will be negative
  print "{} degrees".format(pPrint(math.degrees(math.atan(Erodmag / Espheremag)) * -1.0)) 

  print "(b) Next, a neutral hydrogen atom is placed at that same location (marked by the ×). Draw a diagram showing the effect on the hydrogen atom while it is at that location."

  print "The polarizability of atomic hydrogen has been measured to be 7.4 × 10−41 C · m/(N/C). What is the distance between the center of the proton and the center of the electron cloud in the hydrogen atom?"
  alpha = 7.4e-41
  # dipole moment = polarizability * electric field
  p = alpha * Enetmag
  # dipole moment = charge * distance
  # distance = dipole moment / charge
  d = p / abs(chargeOfElectron)
  print "{} m".format(pPrint(d))
  

  




# 15.1 - 15.3
Problem22()
Problem31()
Problem28()

# 15.4 - 15.6 
Problem36()
Problem40()
Problem45()
Problem51()
Problem60()





