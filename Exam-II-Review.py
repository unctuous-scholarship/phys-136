#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys
import math
import re # regular expression
from decimal import *
from lib import *

def Question1():
  # diagram: https://imgur.com/a/F3SzVBP
  # shows electron with velocity:
  v = 3.0e6 # velocity in meters / second
  # at a 60 degree angle
  theta = 60 # degrees
  # to the magnetic field at a location
  r = 5.0e-10 # distance between electron and obs. location A
  print "Calculate the magnitude of the magnetic field at location A."
  # Biot-Savart's law:
  # μ_0 / 4π  (q v x rhat) / rmag^2 
  # |B| = (μ_0 / 4π) * (|q| |v| |rhat| sin(θ)) / |r|^2

  B = (1.0e-7 * abs(chargeOfElectron) * v * math.sin(math.radians(theta))) / r**2

  print "{}".format(pPrint(B))
  # answer is 0.2

  print "In reference to the previous question, what is the direction of the magnetic field at location A?"
  # sweep from velocity vector (+y) to position vector (+x)
  # thumb ends up pointing into the page
  # since it's an electron, flip the direction:
  print "out of the page" 

def Question3():
  print "How does the magnetic field of a moving point charge fall off with distance at a given angle: like 1/r, 1/r^2, or 1/r^3 ?"
  # ✓
  print "1/r^2"

def Question4():
  print "Consider a circuit with two bulbs, 1 and 2, which are identical in dimensions (the filaments both have the same length and cross sectional area), but the filaments are made of different metals.  The electron mobility in the metal used for Bulb 2 is three times larger than the electron mobility in the metal used for Bulb 1, but both metals have the same number of mobile electrons per cubic meter.  What is the relationship between E1 and E2?"
  # i = n A u E
  # since the two circuits have the same i values:
  # i_1 = i_2
  # n A u_1 E_1 = n A u_2 E_2
  # u_2 = 3 * u_1
  # u_1 E_1 = (3 * u_1) E_2
  # E_1 = 3 E_2

  # ✓
  print "E_1 = 3E_2"

def Question5():
  print "When you bring a current-carrying wire down onto the top of a compass, aligned with the original direction of the needle and 5.0 mm above the needle, the needle deflects by 10.0°. At this location, the horizontal component of the Earth's magnetic field has a value of BEarth = 2.0 x 10-5 T. Calculate the amount of current flowing in the wire."
  r = 5.0e-3 # distance the wire is above the needle, in meters
  theta = 10 # degrees
  Bearth = 2.0e-5 # magnetic field of earth, in teslas
  # Bwire / Bearth = tan(θ)
  # Bwire = tan(θ) * Bearth
  Bwire = math.tan(math.radians(theta)) * Bearth 
  
  # 17.7, pg. 685
  # Magnetic Field of a Straight Wire
  # when wire length >> r
  # Bwire = μ_0 / 4π * 2 * I / r

  # so: 
  # I = Bwire * r / (μ_0 / 4π * 2)
  I = Bwire * r / (1.0e-7 * 2.0)
  print "{}".format(pPrint(I))
  # answer is 0.088

def Question6():
  print "The electron current in a horizontal metal wire is 3.7 x 10^18 electron/s. What is the magnitude of the conventional current?"
  i = 3.7e18
  I = i * abs(chargeOfElectron)
  # 0.59 ✓
  print "{}".format(pPrint(I))

  print "In reference to the previous question, if electrons are flowing to the right, what is the direction of the conventional current?"
  # ✓
  print "left"

def Question8():
  print "The vectors a and b both lie in the xz plane.  The vector c equals a x b. Which of these statements is true?"
  # ✓
  print "The z and x components of c are zero"

def Question9():
  print "If v = <390, -480, 333> m/s and r̂ = <0.577, 0.577, -0.577>, what is v x r?"
  v = (390.0, -480.0, 333.0)
  r = (0.577, 0.577, -0.577)
  print "{}".format(pPrint(crossProduct(v, r)))
  # ✓
  print "<84.8, 417, 502> m/s"


def Question10():
  print "A compass originally points north; at this location the horizontal component of the Earth's magnetic field has a magnitude of 2.0 x 10-5 T. A bar magnet is aligned east-west, pointing at the center of the compass. When the center of the magnet is 0.25 m from the center of the compass, the compass deflects 70°. What is the magnetic dipole moment of the bar magnet?"
  Bearth = 2.0e-5 # earth's magnetic field in teslas
  r = 0.25 # distance between center of compass and magnet
  theta = 70 # deflection in degrees

  # Magnetic Dipole Moment: 
  # μ = I A

  # Bwire / Bearth = tan(θ)
  # Bwire = tan(θ) * Bearth
  B = math.tan(math.radians(theta)) * Bearth

  # Bmagnet = μ_0 / 4π * 2μ/r^3
  # so:
  # μ = Bmagnet * r^3 / (μ_0 / 4π) * 2 
  mu = (B * r**3) / (2.0 * 1.0e-7)
  print "μ = {} A m^2".format(pPrint(mu))
  # ✓
  # with two significant figures: μ = 4.3


def Question11():
  print "In the following circuit, all of the wires are made of the same material, but one wire has a smaller radius than the other wires."
  # Diagram: https://imgur.com/a/0gVXA6O
  print "The radius of the thinner wire is 0.22 mm and the radius of the thick wire is 0.55 mm.  There are 4.0 x 1028 mobile electrons per cubic meter of this material and the electron mobility is 6.0 x 10-4 (m/s)/(N/C).  If 6.0 x 1018 electrons pass location D each second, what is the magnitude of the electric field at location B?"
  Rthin = 0.22e-3 # radius of thinner wire in meters
  Athin = math.pi * Rthin**2 # area of cross section of thin wire, in meters squared
  Rthick = 0.55e-3 # radius of the thicker wire in meters 
  Athick = math.pi * Rthick**2 # area of cross section of thick wire, in meters squared
  n = 4.0e28 # electron density in mobile electrons / m^3
  u = 6.0e-4 # electron mobility in (m/s) / (N/C)
  i = 6.0e18 # electron current in electrons / second
  # find electric field in the thick region marked by B
  # E = i / (n * A * u)
  Ethick = i / (n * Athick * u)
  print "{}".format(pPrint(Ethick))
  # ✓
  # with 2 significant figures: E_thick = 0.26

  print "In reference to the previous question, what would the magnitude of the electric field at location D be?"
  Ethin = i / (n * Athin * u)
  print "{}".format(pPrint(Ethin))
  # ✓
  # with 2 significant figures: E_thin = 1.6


def Question13():
  print "The drift speed in copper wire is 7.2 x 10-5 m/s for a typical electron current.  If the mobility of the mobile electrons in copper is 4.5 x 10-3 (m/s)/(N/C), calculate the magnitude of the electric field inside the wire."
  v = 7.2e-5 # drift speed in copper wire
  u = 4.5e-3 # mobility of mobile electrons in copper
  # v = u * E
  # E = v / u
  E = v / u
  print "{}".format(pPrint(E))
  # ✓
  print "0.016 V/m"

def Question14():
  print "There are very roughly the same number of iron atoms per m^3 as there are copper atoms per m^3, but copper is a much better conductor than iron.  How does u_iron compare to u_copper?"
  # density is the same, so assuming area of cross-section is constant, the mobility is the difference - copper being more mobile than iron
  # ✓
  print "u_iron < u_copper"

def Question15():
  print "A current carrying wire is laid on a table, oriented north-south. Electrons in the wire are flowing south.  What is the direction of the magnetic field at a location directly underneath the wire, due only to these moving electrons?"
  # See Figure 17.26 
  # https://imgur.com/a/bmPkmmh
  # thumb points the direction of conventional current (North in this case)
  # fingers curl around the wire, and point in the direction of the magnetic field (West in this case) 
  # ✓
  print "West"
  print "In reference to the previous question, if a compass is placed under the wire, in which direction will the needle point?"
  # see figure 17.16, the conventional current is to the right
  # ✓
  print "Northwest"

def Question17():
  print "A long, straight current-carrying wire is oriented vertically, with the current flowing up the wire. On the left side of the wire, the magnetic field generated by the wire points"
  # ✓
  print "out of the page" 

def Question18():
  print "Two long wires lie very close together and carry a conventional current as shown below."
  # diagram: https://imgur.com/a/G9kzAs1
  print "Each wire has a semi-circular kink, one of radius R1 and the other of radius R2.  What is the magnetic field at the common center of the two semi-circular arcs?"
  # ✓
  print "<0, 0, -(μ_0 / 4π) * I * π * (1/R_1 + 1/R_2)>" 

def Question19():
  print "How long does it take to establish the steady state in a circuit?"
  # ✓
  print "On the order of nanoseconds."

def Question20():
  print "Consider the following circuit."
  # diagram: https://imgur.com/a/j6jZCXy
  print "VC - VF = 8.0 V and VD - VE = 4.5 V, what is VC - VD ?"
  # The sum of the potential differences along a round-trip path must be zero.
  # ΔV = 0
  # C to D which is V_D - V_C 
  # The question asks for V_C - V_D
  # (V_C - V_F) + (V_D - V_E) + (V_D - V_C) = 0
  # 8 + 4.5 + (V_D - V_C) = 0
  # (V_D - V_C) = -8 - 4.5
  # V_C - V_D = 8 - 4.5
  V_CD = 8.0 - 4.5
  print "{}".format(pPrint(V_CD)) 
  # ✓
  # with two sig figs: 3.5
  

#########################################
# 2020-03-27 Part B of the Exam II Review
#########################################

def Question1B():
  print "You have three capacitors C1= 15 pF, C2= 90 pF and C3= 30 pF. How can you arrange them to get an equivalent capacitance of 30 pF?"
  C1 = 15.0
  C2 = 90.0
  C3 = 30.0

  # Capacitance:
  # C = Q / |ΔV| = (ε_0 * A) / s
  # Q is positive charge on the positive plate of the capacitor
  # |ΔV| is the potential difference across the gap
  # ε_0 is the vacuum permittivity gap
  # A is the area of one of the plates of the capacitor
  # s is the width of the gap between the plates

  # when capacitors are in parallel, their areas are combined
  
  # 1/Ceq = 1/C_2 + 1/(C3 + C1)
  Ceq = 1.0 / (1.0/(C2) + 1.0/(C3 + C1))
  print "Req = {}".format(pPrint(Ceq))
  # ✓
  # figure that is the answer: https://imgur.com/a/kpiUvrb


  

def Question2B():
  print "Consider the two capacitors below.  "
  # figure shows two capacitors in series: https://imgur.com/a/iC0uEkq

  print " C1 = 28 μF and C2 = 194 μF, what is the equivalent capacitance in μF? There is no need to convert."
  # C1 = 28.0
  # C1 = 55.0
  C1 = 86.0
  # C2 = 194.0
  # C2 = 125.0
  C2 = 133.0

  # capacitors are in series, so:
  # C_total = 1 / (1/C1 + 1/C2 ... + 1/Cn)
  # C_total = (C1 * C2 * ... * Cn) / (C1 + C2 + ... + Cn)
  Ceq = (C1 * C2) / (C1 + C2)
  # ✓
  print "2B: {} μF".format(pPrint(round(Ceq)))
  # NEAREST WHOLE NUMBER
  


  

def Question3B():
  print "Two capacitors have the same voltage applied across them, but the second has four times the capacitance of the first. Its charge is"
  # Q = C |ΔV|
  # ✓
  print "four times as much as the first. "


  

def Question4B():
  print "How much charge does a 0.43 F capacitor hold when 160 V is applied across it?"
  # C = 0.43 # capacitance in Farads
  # C = 0.42
  C = 0.58
  # V = 160.0 # potential difference across capacitor 
  # V = 140.0
  V = 350.0
  Q = C * V
  print "4B: {} C".format(pPrint(round(Q)))
  # NEAREST WHOLE NUMBER


  

def Question5B():
  print "Four resistors are shown in the circuit below.  What is the current through the 40 Ω resistor?"
  R1 = 20.0 # resistance in ohms
  R2 = 40.0 # ohms
  V = 120.0 # volts
  # figure of resistors:  https://imgur.com/a/dgPXhx1

  # I is constant within a single loop 
  # there are two resistors in the right-handed loop
  # Req = R1 + R2
  Req = R1 + R2
  # I = |ΔV| / R
  I = (V / Req)
  print "{} A".format(pPrint(I))
  # ✓
  # 2.0 A
  


  

def Question6B():
  print "What is the current through the 20 ohm resistor above?"
  # one sig. fig.
  R = 20.0
  V = 6.0
  I = V / R
  print "{}".format(pPrint(I))
  # ✓
  # 20 ohms, 6 V = 0.3 Amps


  

# 7B:
# When a washing machine, refrigerator, or vacuum cleaner is first turned on, lights in the same circuit will 
# dim briefly because of the IR2 drop produced in feeder lines by the large current drawn by the motor.

# When a vacuum cleaner, refrigerator, or washing machine is first turned on, lights in the same circuit dim briefly due to the IR drop produced in feeder lines by the large current drawn by the motor. 



  
def Question8B():
  print "Three resistors are connected in parallel. Calculate their equivalent resistance if:"

  print "R1 = 150 Ohms"
  # R1 = 150.0
  # R1 = 101.0
  R1 = 137.0

  print "R2 = 83 Ohms"
  # R2 = 83.0
  # R2 = 88.0
  R2 = 71.0

  print "R3 = 241 Ohms"
  # R3 = 241.0
  # R3 = 214.0
  R3 = 211.0

  Req = 1.0 / ((1.0/R1) + (1.0/R2) + (1.0/R3))
  print "8B: {} Ohms".format(pPrint(round(Req)))
  # ✓
  # R1 = 150, R2 = 83, R3 = 241, Req = 44
  # NEAREST WHOLE NUMBER



  
def Question9B():
  print "What is the equivalent resistance of the circuit shown below?"
  R1 = 5.0 # series
  R2 = 4.0 # parallel
  R3 = 12.0 # parallel
  R4 = 8.0 # series
  Req = R1 + R4 + (1.0 / ((1.0/R2) + (1.0/R3)))
  print "{} Ohms".format(pPrint(Req))
  # ✓
  # 16



  
def Question10B():
  print "The current flowing through R4 is 3.0 A.  What is the current flowing through R2?"
  R1 = 2.0 # series
  R2 = 3.0 # parallel
  R3 = 6.0 # parallel
  R4 = 4.0 # series
  Req = R1 + R4 + (1.0 / ((1.0/R2) + (1.0/R3)))
  print "Req = {}".format(pPrint(Req))
  I = 3.0 
  V = 24.0 # volts
  # current is same in series, splits along branches into parallel resistors
  Itotal = V / Req
  print "I_total = {}".format(pPrint(Itotal))
  proportional = R3 / R2
  # R2 / Rtotal = I2 / Itotal
  I2 = (R2 / Req) * Itotal
  print "I_2 = {}".format(pPrint(I2))
  I3 = (R3 / Req) * Itotal
  print "I_3 = {}".format(pPrint(I3))
  # ✓
  # should be 2


  
def Question11B():
  print "After 1.5 time constants have elapsed, what percentage of the final voltage, ε, is on an initially uncharged capacitor C, charged through a resistance, R?"
  # time constant is RC
  # t = 1.5 * (RC)
  # 1 - e^(-t / RC) = %
  percent = (1.0 - math.exp(-1.5)) * 100.0
  print "{}%".format(pPrint(percent))
  # ✓
  # 77.7
  
  

def Question12B():
  print "Three resistors are connected in series. Calculate their equivalent resistance if:"

  print "R1 = 176.5 Ohms"
  # R1 = 176.5
  # R1 = 111.4
  R1 = 124.6

  print "R2 = 90.9 Ohms"
  # R2 = 90.9
  # R2 = 71.9
  R2 = 82.4

  print "R3 = 359.7 Ohms"
  # R3 = 359.7
  # R3 = 307.8
  R3 = 268.2

  Req = R1 + R2 + R3
  print "12B: {} Ohms".format(pPrint(round(Req)))
  # ✓
  # R1 = 176.5, R2 = 90.9, R3 = 359.7, Req = 627
  # NEAREST WHOLE NUMBER


# 13B:

# ✓
# - less than

# In the following circuit, the potential difference across points K and J is _less-than_ the potential difference across points B and C.
# R = 6 Ohms between K and J
# R = 8 Ohms between B and C
# we know emf = R * I
# so with smaller R, you get smaller emf




# 14B:
# ✓
# The device shown below uses the _Hall_Effect_ to determine of blood flow rate.


  

# 15B:
# Which of the following charges has the largest mass assuming a and b have identical charges and velocities? 
# a
# because the radius of the curvature is larger,  
# which happens when the momentum is greater,
# which happens when the mass is greater (if all else is constant)

# 16B:
# Determine the direction of the magnetic field B that produces the magnetic force on a positive charge shown in each of the three figures below, assuming the velocity is perpendicular to the magnetic field.

# see figure 20.4
# use right hand rule, sweep from F to v, thumb is B

# Figure (a): into the page 
# Figure (b): left
# Figure (c): out of the page

# 17B:
# Consider a positively charged particle moving with a velocity v.  What is the direction of the magnetic force, F felt by the charge due to the current I in the long, straight wire?

# using thumb pointing the direction of I
# my fingers curl into the page on the left side of the wire
# so B is into the page at the charge

# F_magnetic = q v x B
# using right hand rule:
# - index pointing up with v
# - middle pointing into the page with B 
# - thumb pointing to the left

# left


def Question18B():
  print "Consider a long straight wire oriented vertically. If a 600 A current flows south to north through the wire, what is the magnitude of the magnetic field 3.00 cm to the right of the wire?"
  # I is flowing in the +y direction, up
  # B is into the page (-z) to the right of the wire
  I = 600.0 # amps
  r = 3.0e-2 # length of wire in meters

  # Magnetic Field of a Wire
  # where radius r << length of wire L
  # B_wire = μ_0 / 4π * (2I) / R

  B = 1.0e-7 * 2.0 * I / r

  print "18B: {} teslas".format(pPrint(B))
  # should be 0.004

# 19B:
# In reference to the previous question, how does the magnetic field point at this location?
# right hand rule, I goes up so thumb goes up
# fingers rotate / wrap around the wire, on the right they point into the page
# out of the page
  

def Question20B():
  print "Consider an electric generator at a hydroelectric facility. If a 445 turn coil has an area of 0.14 m2 and is rotating at 300 rad/s in a magnetic field of 0.500 T, what is the EMF generated when 2.23 s have elapsed?"
  # N = 445.0
  N = 434.0
  A = 0.14 # area is meters squared
  omega = 300.0 # angular frequency in radians per second
  B = 0.5 # magnetic field in teslas
  t = 2.23 # time elapsed in seconds
  # emf = omega * B * A * sin(omega * t)  
  emf = N * omega * B * A * math.sin(omega * t)
  print "20B: {} volts".format(pPrint(round(emf)))
  

def Question21B():
  print "A proton moving with a velocity of 1.50 x 106 m/s in the xy plane experiences a force of 2.33 x 10-13 N pointing in + z direction (out of the page) when moving in the presence of an applied magnetic field, B. What is the magnitude of the applied magnetic field?"
  v = 1.5e6 # velocity in m/s
  F = 2.33e-13 # force in newtons
  theta = 90 - 37 # angle from v to B, in degrees

  # F_mag = qv x B
  # F_magnetic = |qv| |B| sin(theta)
  # B = F_mag / |qv| sin(theta)
  B = F / (chargeOfProton * v * math.sin(math.radians(theta)))
  print "21B: {} teslas".format(pPrint(B))
  # 1.22
  # 3 SIGNIFICANT DIGITS
  

# 22B
# Consider the following bar magnet with two poles, N (north) and S (south).
# If you were to cut the magnet in half, 
# ✓
# both halves would have N and S


def Question23B():
  print "You connect a 9.00 V battery to a capacitor consisting of two circular plates of radius 0.0800 m separated by an air gap of 6.30 mm.  What is the charge in nanoCoulombs on the positive plate?"
  # note: nano = 1 x 10^-9
  emf = 9.0
  R = 0.08 # radius in meters
  A = math.pi * R**2
  s = 6.3e-3 # air gap in meters

  # Q = C * |ΔV|
  # we need capacitance

  # E = (Q/A) / ε_0
  # |ΔV| = E * s = ((Q/A) / ε_0) * s
  # C = Q / |ΔV|  
  # C = (ε_0 * A) / s
  C = (vacuumPermittivity * A) / s

  nanoQ = (C * emf) / 1.0e-9
  print "23B: {} nanocoulombs".format(pPrint(nanoQ))
  # 0.254
  # 3 SIGNIFICANT DIGITS

def Question24B():
  print "A 47.0 cm long high-resistance wire with rectangular cross section 7.00 mm by 3.00 mm is connected to a 12.0 V battery through an ammeter, as shown below."
  L = 47.0e-2
  A = 7.0e-3 * 3.0e-3 
  emf = 12.0 

  print "The resistance of the wire is 57.0 ohms.  The resistance of the ammeter and the battery are negligible.  Leads from a voltmeter are placed/connected at points A and C.  The distance along the wire between the voltmeter connections is d = 6.00 cm."
  R = 57.0 # ohms
  d = 6.0e-2

  # ΔV = E * L
  # E = ΔV / L
  E = emf / L
  print "Calculate the electric field at Location B."
  print "24B: E = {} coulombs".format(pPrint(E))
  # 25.5
  # 3 SIGNIFICANT DIGITS

  print "In reference to the previous question, what would the voltmeter read?"
  # ΔV = E * L
  V = E * d
  print "25B: {} volts".format(pPrint(V))
  # 1.53
  # 3 SIGNIFICANT DIGITS

def Question26B():
  print "A neutral metal rod of length 0.350 m slides horizontally at a constant speed of 5.00 m/s on frictionless insulating rails through a region of uniform magnetic field of magnitude 0.700 T, directed into the page as shown in the diagram below."
  L = 0.35 # meters
  v = (-5.0, 0.0, 0.0) # m/s
  B = (0.0, 0.0, -0.7)
  print "What is the magnetic force on a mobile electron inside the rod?"
  # Fmag = qv x B
  Fmag = crossProduct(multiplyVectorByScalar(v, chargeOfElectron), B)
  print "26B: {} Newtons".format(pPrint(Fmag))
  # < 0, 5.60E-19, 0 > N


def Question27B():
  print "A bar magnet whose dipole moment is < 9.00, 0, 1.90 > A.m2 is suspended from a thread in a region of an externally applied field of < 0.900, 0, 0 > T. What is the torque that acts on the bar magnet?"
  mu = (9.0, 0.0, 1.9) # magnetic dipole moment in Amp * meters squared
  B = (0.9, 0.0, 0.0) # magnetic field in teslas
  # τ = μ x B
  torque = crossProduct(mu, B)
  print "27B: {} Newton meters".format(pPrint(torque))
  # < 0, 1.71, 0 > N.m 



def Question28B():
  print "A bar magnet whose magnetic dipole moment is 11.0 A.m2 is aligned with an applied magnetic field of 3.30 T.  How much work must you do to rotate the bar magnet to point 180o to point in the direction opposite to the magnetic field?"
  mu = 11.0
  B = 3.3
  theta = 180 # degrees
  # work = U_m = -μ * B * cosθ
  work = -1.0 * mu * B * math.cos(math.radians(theta))
  print "28B: {} joules".format(pPrint(work * 2.0))
  # 72.6 J 


def Question29B():
  print "In order to bend an electron in a cathode ray tube so that the electron hits of the top of the screen, a magnetic field is applied.  If the electron is accelerated through a voltage difference of 14960 V, it has a speed of 0.710 x 108 m/s (we can treat the electron non-relativistically, although that's starting to push the limits).  Under these circumstance, one needs a radius of curvature, r = 22.0 cm, as shown below."
  V = 14960.0 # volts
  v = 0.71e8 # m/s
  r = 22.0e-2 # radius of curvature
  m = massOfElectron
  
  print "What is the magnetic field required to make the electron hit the top of the screen?"

  p = m * v

  # p = m * v

  # p = |q| B R
  # so:
  # B = p / (q * R)

  B = p / (chargeOfProton * r)
  print "29B: |B| = {}".format(pPrint(B))
  # < 0, 0, -0.00184 > T 


def Question30B():
  print "An 11.0 cm bar with a rectangular cross section 3.00 cm high and 2.00 cm deep is connected to a 1.20 V battery and an ammeter.  The resistance of the connecting wires, the ammeter, and the internal resistance of the battery are all negligible.  An externally applied uniform field of 2.70 T points out of the page, perpendicular to the bar.  A voltmeter is connected across the bar, with the leads carefully placed directly across from each other as shown below.  The voltmeter is set to the microvolt range."
  L = 11.0e-2 # length of bar
  height = 3.0e-2
  depth = 2.0e-2
  A = height * depth
  V = 1.2 # volts
  B = 2.7 # magnetic field in teslas

  # note: micro = 1 x 10^-6 

  print "The mobile charges in the bar have +e charge and their mobility is 4.00 x 10-5 (m/s)/(V/m)."
  q = chargeOfProton
  u = 4.0e-5

  print "What will the voltmeter display?"
  
  # v = ΔV⊥  / Bd
  # v = u E
  # v = I / (q n A)
  # E = ΔV / Δl
  # E∥ = ΔV∥ / length
  Eparallel = V / L
  v = u * Eparallel
  Vhall = v * B * height
  print "30B: V = {} V".format(pPrint(Vhall / (1.0e-6)))
  # -35.3
  # 3 SIGNIFICANT DIGITS 
  
  

Question1()
Question5()
Question6()
Question9()
Question10()
Question11()
Question13()
Question20()

Question1B()
Question2B()
# Question3B()
Question4B()
Question5B()
Question6B()
Question8B()
Question9B()
Question10B()
Question11B()
Question12B()
Question18B()
Question20B()
Question21B()
Question23B()
Question24B()
Question26B()
Question27B()
Question28B()
Question29B()
Question30B()
