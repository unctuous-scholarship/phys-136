#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys
import math
import re # regular expression
from decimal import *
from lib import *

# Chapter 16: Electric Potential
# from Matter & Interactions (4th ed., 2015) 
# by Ruth W. Chabay and Bruce A. Sherwood


def Problem22():
  print "An electron passes through a region in which there is an electric field, and while it is in the region its kinetic energy decreases by 2.6 × 10-17 J. Initially the kinetic energy of the electron was 4.5 × 10-17 J. What is the final speed of the electron? (You can use the approximate (nonrelativistic) equation here.)"

  Ki = 4.5e-17 # initial kinetic energy, in Joules
  deltaK = -2.6e-17 # change in kinetic energy, in Joules
  K = Ki + deltaK
  # approximate kinetic energy of a particle:
  # K = 1/2 m v^2, for v << c 
  v = math.sqrt((K * 2.0) / massOfElectron)
  print "v = {} m/s".format(pPrint(v))


def Problem24():
  print "The electric potential energy of a system of two particles is given by this expression: Uel = (1 / 4 π ε_0) * (q1 q2) / r"

  print "(a) What is the electric potential energy of two protons, separated by a distance of 4.5 nanometers?"
  r = 4.5e-9 # distance in meters
  U1 = ke * (chargeOfProton * chargeOfProton) / r
  print "U = {} J".format(pPrint(U1))

  print "(b) What is the electric potential energy of a proton and an electron separated by the same distance?"
  # should be the negative of the same
  U2 = U1 * -1.0 
  print "U = {} J".format(pPrint(U2))


def Problem27():
  print "A capacitor with a gap of 4 mm has a potential difference from one plate to the other of 32 volts. What is the magnitude of the electric field between the plates?"
  V = 32.0 # potential difference, in volts
  l = 4.0e-3 # gap, in meters  
  # Potential Difference in a Uniform Field:
  # ΔV = E * Δl 
  # E = ΔV / Δl
  E = V / l
  print "{} V/m".format(pPrint(E))


def Problem31():
  print "Locations A, B, and C are in a region of uniform electric field, as shown in the diagram above. Location A is at < -0.3, 0, 0 > m. Location B is at < 0.4, 0, 0 > m. In the region the electric field E = < -750, 200, 0> N/C."
  A = (-0.3, 0.0, 0.0)
  B = (0.4, 0.0, 0.0)
  E = (-750.0, 200.0, 0.0)
  print "For a path starting at A and ending at B, calculate:"

  print "The displacement vector Δl"
  deltaL = subtractVectors(B, A)
  print "Δl = {} m".format(pPrint(deltaL))

  print "(b) the change in electric potential:"
  # ΔV = E * Δl 
  deltaV = abs(dotProduct(E, deltaL))
  print "ΔV = {} V".format(pPrint(deltaV))

  print "(c) the potential energy change for the system when a proton moves from A to B:"
  # ΔU = q * ΔV 
  deltaU = chargeOfProton * deltaV
  print "ΔU = {} J".format(pPrint(deltaU))
  
  print "(c) the potential energy change for the system when an electron moves from A to B:"
  print "ΔU = {} J".format(pPrint(-1.0 * deltaU))


def Problem40():
  print "In an old-style television picture tube (not in a modern flat-panel TV) electrons are boiled out of a very hot metal filament placed near a negative metal plate (see the figure). These electrons start out nearly at rest and are accelerated toward a positive metal plate. They pass through a hole in the positive plate on their way toward the picture screen. If the high-voltage supply in the television set maintains a potential difference of 12800 volts between the two plates, what speed do the electrons reach? (You can use the nonrelativistic approximation here.)"
  V = 12800.0 # volts
  # approximate kinetic energy of a particle:
  # K = 1/2 m v^2, for v << c 
  # K + U = 0
  # U = q * V
  U = chargeOfElectron * V
  v = math.sqrt(2.0 * (-1.0 * U) / massOfElectron)
  print "v = {} m/s".format(pPrint(v))


def Problem41():
  print "A capacitor is made of two very large disks of radius R carrying uniformly distributed charges Q_A = Q and Q_B = -Q. The plates are parallel and 0.1 millimeters apart. The potential difference between the plates is V_B - V_A = -5 volts."
  deltaV = -5.0 # volts
  deltaL = 0.1e-3 # distance between plates, in meters
  
  print "(b) Determine value of Q that would make V_B - V_A = -5 volts assuming R = 20 m."
  R = 20.0 # radius in meters

  # Electric Field of a Capacitor:
  # E = (Q / A) / ε_0
  # where A = area, i.e. π * R^2
  A = math.pi * R**2

  # Potential Difference in a Uniform Field:
  # ΔV = E * Δl 
  # E = ΔV / Δl
  E = deltaV / deltaL

  # so, Q = E * A * ε_0
  Q = E * A * vacuumPermittivity
  print "|Q| = {} C".format(pPrint(abs(Q)))



# from sympy import symbols, diff
# def Problem48():
#   print "If throughout a particular region of space the potential can be expressed as V = 3xz + 2y - 5z, what are the vector components of the electric field at location < x, y, z>?"
#   # E = -∇V
#   # E = <-dV/dx, -dV/dy, -dV/dz>
#   x,y,z = symbols('x y z', real=True)
#   # V = 3 * x * z + 2 * y - 5 * z
#   Ex = -1 * diff(3 * x * z + 2 * y - 5 * z, x)
#   Ey = -1 * diff(3 * x * z + 2 * y - 5 * z, y)
#   Ez = -1 * diff(3 * x * z + 2 * y - 5 * z, z)
#   # print "<-3z, -2, 5 - 3x>".format(pPrint(E))
#   print "<{}, {}, {}>".format(Ex, Ey, Ez)



def Problem60():
  print "A dipole is centered at the origin, with its axis along the y axis, so that at locations on the y axis, the electric field due to the dipole is given by E = <0, k * 2qs / y^3, 0> V/m"

  print "The charges making up the dipole are q1 = +9 nC and q2 = -9 nC, and the dipole separation is s = 5 mm (see figure below). What is the potential difference along a path starting at location P1 = < 0, 0.02, 0 > m and ending at location P2 = < 0, 0.06, 0 > m?"
  q1 = 9.0e-9 # positive charge of dipole, in coulombs
  q2 = -q1
  s = 5e-3 # dipole separation, in meters
  P1 = (0.0, 0.02, 0.0) # starting location, in meters
  P2 = (0.0, 0.06, 0.0) # ending location, in meters
  
  # q1 = 6.0e-9
  # q2 = -q1
  # s = 4.0e-3

  # ΔV = ∫ (from P1 to P2) of (k * 2qs / y^3) dy
  # ΔV = (k * 2qs) * ∫_{P1}^{P2} (1/ y^3) dy
  # ΔV = (k * 2qs) * [- 1/ 2y^2]_{P1}^{P2}
  # ΔV = (k * 2qs) * (-1/ 2(P2)^2) - (-1/2(P1)^2)

  deltaV = ke * (2.0 * q1 * s) * ((-1.0 / (2.0 * P1[1]**2)) - (-1.0 / (2.0 * P2[1]**2)))

  print "{} V".format(pPrint(deltaV))



def Problem65():
  print "In a region of space there are several stationary charged objects. Along a path from A - B - C - D as shown in the figure below, you measure the following potential differences:"

  print "VB - VA = 12V"
  print "VC - VB = -2V"
  print "VD - VC = -16V"
  VAtoB = 12.0 # volts
  VBtoC = -2.0 # volts
  VCtoD = -16.0 # volts

  print "(a) What is the potential difference VA - VD?"
  # V_{D to A} + V_{A to D} = 0
  # V_{D to A} = -V_{A to D}
  VAtoD = VAtoB + VBtoC + VCtoD
  VDtoA = -1.0 * VAtoD
  print "VA - VD = {} V".format(pPrint(VDtoA))



def Problem66():
  print "A capacitor consists of two charged disks of radius 4.8 m separated by a distance s = 2 mm (see the figure). The magnitude of the charge on each disk is 54 µC. Consider points A, B, C, and D inside the capacitor, as shown in the diagram. The distance s1 = 1.4 mm, and the distance s2 = 0.7 mm. (Assume the +x axis is to the right, the +y axis is up, and the +z axis is out.)"

  R = 4.8 # radius of disks in meters
  s = 2.0e-3 # distance between disks of capacitor, in meters
  Q = 54.0e-6 # charge of disks in coulombs
  s1 = 1.4e-3 # meters
  s2 = 0.7e-3 # meters

  print "(a) What is the electric field inside the capacitor?"
  # Electric Field of a Capacitor:
  # E = (Q / A) / ε_0
  # where A = area, i.e. π * R^2
  A = math.pi * R**2 
  E = ((Q / A) / vacuumPermittivity, 0, 0)

  print "E = {} N/C".format(pPrint(E))


  print "(b) First, calculate the potential difference VB - VA."

  print "What is Δl along this path?"

  deltaL = (-s1, 0.0, 0.0)

  print "Δl = {} m".format(pPrint(deltaL))


  print "What is VB - VA?"
  print "VB - VA = {}".format(pPrint(abs(deltaL[0] * E[0])))

  print "(c) Next, calculate the potential difference VC - VB."
  print "What is Δl along this path?"
  deltaL = (0, -s2, 0)
  print "Δl = {} m".format(pPrint(deltaL))


  print "What is VC - VB?"
  # perpendicular to E, so it's 0
  print "VC - VB = {}".format(pPrint(abs(deltaL[1] * E[1])))

  print "(d) Finally, calculate the potential difference VA - VC." 
  print "What is Δl along this path?"
  deltaL = (s1, s2, 0)
  print "Δl = {} m".format(pPrint(deltaL))

  print "What is VA - VC?"
  print "VA - VC = {}".format(pPrint(-1.0 * deltaL[0] * E[0]))
  

def Problem68():
  print "Four voltmeters are connected to a circuit as shown in the diagram. As is usual with voltmeters, the reading on the voltmeter is positive if the negative lead (black wire, usually labeled COM) is connected to a location at lower potential, and the positive lead (red) is connected to a location at higher potential."
  print "The circuit contains two devices whose identity is unknown, and a rod of length d = 6 cm made of a conducting material. At a particular moment, the readings observed on the voltmeters are:"
  d = 6.0e-2 # rod length in meters
  print "Voltmeter A: 1.4 volts"
  VA = 1.4
  print "Voltmeter B: 7 volts"
  VB = 7.0
  print "Voltmeter C: 2.5 volts"
  VC = 2.5 

  print "At this moment, what is the reading on Voltmeter D, both magnitude and sign?"

  deltaV = -1.0 * (VB + VA + VC)
  print "ΔV = {} V".format(pPrint(deltaV))

  print "What is the magnitude of the electric field inside the bar?"

  # ΔV = E * Δl
  # E = ΔV / Δl
  E = deltaV / d
  print "|E| = {} V/m".format(pPrint(abs(E)))



def Problem70():
  print "What is the electric potential at a location 5.5 × 10-9 m from an electron?"
  r = 5.5e-9 # distance from charge, in meters
  V = ke * chargeOfElectron / r

  print "V = {} V".format(pPrint(V))


def Problem71():
  print "Calculate the potential at location A in the figure. Assume that q1 = 5 nC, q2 = -6 nC, a = 20 cm, b = 15 cm and c = 25 cm. (1 nC is 10-9 C.)"
  q1 = 5e-9
  q2 = -6e-9
  a = 20e-2
  b = 15e-2
  c = 25e-2

  V1 = ke * q1 / a 
  V2 = ke * q2 / b 
  V = V1 + V2
  print "{} V".format(pPrint(V))
  
  

####################

def Checkpoint11():
  print "A capacitor with a 3 mm gap has a potential difference of 6 volts (see the figure). A disk of glass 1.88 mm thick, with area the same as the area of the metal plates, has a dielectric constant of 3.4. It is inserted in the middle of the gap between the metal plates. Now what is the potential difference of the two metal disks? (It helps to make a diagram showing the electric field along a path.)"
  K = 3.4 # dielectric / insulator constant
  # K = 2.5

  V = 6.0 # potential difference in volts 
  sTot = 3.0e-3 # total capacitor gap in meters

  sGlass = 1.88e-3 # width of glass, in meters
  # sGlass = 1.0e-3

  sSpace = (sTot - sGlass) # total width of air gaps
  s1 = sSpace / 2.0 # there are two air gaps, each s1 wide

  sRatio = sTot / s1
  print "s1 = sTot / ratio, ratio = sTot / s1 = {}".format(pPrint(sRatio))
  
  # with no insulator in the gap, 
  # the potential difference across the capacitor is 
  # |ΔV| = E * s
  # E = ΔV / s
  Espace = V / sTot
  # potential difference of the insulator is the 
  # potential difference of the capacitor divided by the dielectric constant
  Eglass = Espace / K
  Vglass = Eglass * sGlass

  Vspace = Espace * sSpace

  # potential difference of the air left is the original electric field * the new air gap

  Vtot = Vglass + Vspace

  print "Vspace = {}".format(pPrint(Vspace)) 
  print "Vglass = {}".format(pPrint(Vglass))
  print "{} V".format(pPrint(Vtot))
  # print "should be 4.8 V"
  
  

def Extra161001():
  print "The energy density inside a certain capacitor is 15 J/m3. What is the magnitude of the electric field inside the capacitor?"
  # Field Energy Density:
  # 1/2 ε_0 E^2 (in J/m^3)
  density = 15.0
  E = math.sqrt(density * 2.0 / vacuumPermittivity)
  print "{} V/m".format(pPrint(E))


def Problem83():
  print "An isolated large-plate capacitor (not connected to anything) originally has a potential difference of 850 volts with an air gap of 2 mm. Then a plastic slab 1 mm thick, with dielectric constant 3.3, is inserted into the middle of the air gap as shown in the figure. As shown in the diagram, location 1 is at the left plate of the capacitor, location 2 is at the left edge of the plastic slab, location 3 is at the right edge of the slab, and location 4 is at the right plate of the capacitor. All of these locations are near the center of the capacitor. Calculate the following potential differences."

  V = 850.0 # potential difference in volts
  sTot = 2.0e-3 # air gap, in meters
  sPlastic = 1.0e-3 # width of plastic slab
  K = 3.3 
  
  sSpace = sTot - sPlastic
  s1 = sSpace / 2.0
  sRatio = sTot / s1
  V1to2 = V / sRatio
  Vspace = 2.0 * V1to2
  Vplastic = Vspace / K
  
  print "V1 - V2 = {} V".format(pPrint(V1to2))
  print "V2 - V3 = {} V".format(pPrint(Vplastic))
  print "V3 - V4 = {} V".format(pPrint(V1to2))
  print "V1 - V4 = {} V".format(pPrint(Vplastic + Vspace))





# 16.1 - 16.3
Problem22()
Problem24()
Problem27()
Problem31()
Problem40()
Problem41()


# 16.4 - 16.6
# Problem48()
Problem60()
Problem65()
Problem66()
Problem68()

# 16.7
Problem70()


# 16.9 - 16.10
Checkpoint11()
Extra161001()
Problem83()
