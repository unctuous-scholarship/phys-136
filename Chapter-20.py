#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys 
import math
import re # regular expression
from decimal import *
from lib import *

# Chapter 20: Magnetic Force
# from Matter & Interactions (4th ed., 2015) 
# by Ruth W. Chabay and Bruce A. Sherwood




def Problem28():
  print "To become familiar with the order of magnitude of magnetic effects, consider the situation in a television cathode ray tube (not a flat-panel TV). In order to bend the electron trajectory so that the electron hits the top of the screen rather than going straight through to the center of the screen, you need a radius of curvature in the magnetic field of about r = 18 cm (see figure below). If the electrons are accelerated through a 14911 V potential difference, they have a speed of 0.709 × 108 m/s."
  R = 18.0e-2 # radius in meters
  # V = 14911.0 # potential difference in volts, not used :-/
  v = 0.709e8 # speed of electrons in meters per second
  m = massOfElectron

  # diagram: https://imgur.com/a/oUB4P84

  # other vars:
  # R = 19.0e-2
  # V = 14912.0
  # v = 0.709e8

  p = m * v

  print "Calculate the magnitude of the magnetic field required to make the electrons hit the top of the screen. "

  # answer for those other vars:
  # 0.00212 T

  # p = m * v
  # p = |q| B R

  # B = p / (q * R)
  B = p / (chargeOfProton * R)

  print "{} T".format(pPrint(B))

  print "Is the magnetic field into or out of the page?"

  print "into the page"




######################



def Problem31():
  print "Mass spectrometer"

  print "A mass spectrometer is a tool used to determine accurately the mass of individual ionized atoms or molecules, or to separate atoms or molecules that have similar but slightly different masses. For example, you can deduce the age of a small sample of cloth from an ancient tomb, by using a mass spectrometer to determine the relative abundances of carbon-14 (whose nucleus contains 6 protons and 8 neutrons) and carbon-12 (the most common isotope, whose nucleus contains 6 protons and 6 neutrons). In organic material the ratio of 14C to 12C depends on how old the material is, which is the basis for \"carbon-14 dating.\" 14C is continually produced in the upper atmosphere by nuclear reactions caused by \"cosmic rays\" (high-energy charged particles from outer space, mainly protons), and 14C is radioactive with a half-life of 5700 years. When a cotton plant is growing, some of the CO2 it extracts from the air to build tissue contains 14C which has diffused down from the upper atmosphere. But after the cotton has been harvested there is no further intake of 14C from the air, and the cosmic rays that create 14C in the upper atmosphere can't penetrate the atmosphere and reach the cloth. So the amount of 14C in cotton cloth continually decreases with time, while the amount of non-radioactive 12C remains constant."

  print "Here is a particular kind of mass spectrometer (see the figure). Carbon from the sample is ionized in the ion source at the left. The resulting singly ionized 12C+ and 14C+ ions have negligibly small initial velocities (and can be considered to be at rest). They are accelerated through the potential difference ΔV_1. They then enter a region where the magnetic field has a fixed magnitude B = 0.16 T. The ions pass through electric deflection plates that are 1 cm apart and have a potential difference ΔV_2 that is adjusted so that the electric deflection and the magnetic deflection cancel each other for a particular isotope: one isotope goes straight through, and the other isotope is deflected and misses the entrance to the next section of the spectrometer. The distance from the entrance to the fixed ion detector is a distance of w = 29 cm. There are controls that let you vary the accelerating potential ΔV_1 and the deflection potential ΔV_2 in order that only 12C+ or 14C+ ions go all the way through the system and reach the detector. You count each kind of ion for fixed times and thus determine the relative abundances. The various deflections insure that you count only the desired type of ion for a particular setting of the two voltages."

  # diagram: https://imgur.com/a/1yatJI2

  B = 0.16 # magnetic field, in teslas
  s = 1.0e-2 # gap of electric deflection plates, in meters
  w = 29.0e-2 # distance from entrance to fixed ion detector, in meters

  # worksheet variables:
  # B = 0.2 # teslas
  # w = 20.0e-2
  
  
  print "(a) Which accelerating plate is positive?"
  print "left"

  print "Which deflection plate is positive?"
  print "top"

  print "What is the direction of the magnetic field inside the spectrometer?"
  print "into the page"


  print "(b) Determine the appropriate numerical values of ΔV_1 and ΔV_2 for 12C. Carry out your intermediate calculations algebraically, so that you can use the algebraic results in the next part."
  # W = q ΔV = 1/2 m v^2

  # F_c = m v^2 / R
  
  molarMass12C = 12.0e-3 # kg / mol
  molarMass14C = 14.0e-3 # kg / mol
  massOf12C = molarMass12C / Na
  print "mass of 12C = {}".format(pPrint(massOf12C))
  massOf14C = molarMass14C / Na

  # v = q B (w / 2.0) / (molar mass of 12C / Avogadro's constant)
  v = (chargeOfProton * B * (w / 2.0)) / (molarMass12C / Na)
  print "v = {}".format(pPrint(v))

  # ΔV_1 = |q| m v_f^2 / 2 
  deltaV1 = (massOf12C * v**2) / (2.0 * chargeOfProton)

  # print "{}".format(pPrint(multiplyVectorByScalar(crossProduct((0, 0, -2.9e6), (0, -0.27, 0)), chargeOfProton)))
  

  print "ΔV_1 = {} V".format(pPrint(deltaV1))

  # ΔV_2 = d v B
  deltaV2 = s * v * B
  print "ΔV_2 = {} V".format(pPrint(deltaV2))


  print "(c) Determine the appropriate numerical values of ΔV_1 and ΔV_2 for 14C. "

  v = (chargeOfProton * B * (w / 2.0)) / (molarMass14C / Na)

  deltaV1 = (massOf14C * v**2) / (2.0 * chargeOfProton)
  print "ΔV_1 = {} V".format(pPrint(deltaV1))

  
  # ΔV_2 = d v B
  deltaV2 = s * v * B
  print "ΔV_2 = {} V".format(pPrint(deltaV2))





######################




def Problem33():


  print "The thin circular coil in the figure has radius r = 14 cm and contains N = 3 turns of resistive wire. The coil has a resistance of 0.5 ohms. A small compass is placed at the center of the coil. With the battery disconnected, the compass needle points to the right, in the plane of the coil. The apparatus is located near the equator, where the Earth's magnetic field B_Earth is horizontal, with a magnitude of 4 × 10−5 tesla. (Assume that B_Earth is pointing to the right.)"

  r = 14.0e-2 # radius in meters
  R = 0.5 # resistance in ohms
  N = 3.0 # number of turns in resistive wire
  Bearth = 4.0e-5 # Earth's magnetic field

  # diagram: https://imgur.com/a/NMhS6Fu

  print "(a) When the 1.5-volt battery is connected, predict the deflection of the compass needle. (If you have to make any approximations, think carefully about what they are.)"
  V = 1.5 # volts
  
  # other examples:
  # r = 10e-2
  V = 1.6
  # theta is 56.45 with these other vars.

  R = 0.6
  
  

  print "Is the deflection outward or inward as seen from above?"
  print "inward"

  print "What is the magnitude of the deflection?"
  # tan = B_loop / B_Earth
  # degrees = arctan(B_loop / B_Earth)


  # I = V/R
  I = V / R

  # B_loop = (μ_0 / 4π) * (N * 2π R^2 I) / (z^2 + R^2)^(3/2)
  # z = 0 because we're at the center of the loop. 
  # B_loop = (μ_0 / 4π) * (N * 2π * I) / R

  Bloop = (1.0e-7 * N * 2.0 * math.pi * I) / r
  print "B_loop = {} T".format(pPrint(Bloop))

  theta = math.degrees(math.atan(Bloop / Bearth))

  print "{} degrees".format(pPrint(theta))

  print "(b) The compass is removed. The current in the coil continues to run. An electron is at the center of the coil and is moving with speed v = 9 × 10^6 m/s into the page (perpendicular to the plane of the coil). In addition to the magnetic fields in the region due to the Earth and the coil, there is an electric field at the center of the coil (due to charges not shown in the figure), and this electric field points upward and has a magnitude E = 740 volts/meter. Predict the net force on the electron at this moment. (You can neglect the gravitational force, which easily can be shown to be negligible. Assume the +x axis is to the right, the +y axis is up, and the +z axis is out. Pay close attention to the orientation of the coil with respect to the Earth.)"
  vmag = 9.0e6 # velocity of electron when at the center of the coil, in meters / second
  vmag = 3.0e6
  v = (0.0, 0.0, -1.0 * vmag)
  Emag = 740.0 # electric field in volts per meter
  Emag = 780.0
  E = (0.0, Emag, 0.0)
  B = (Bearth, 0.0, -1.0 * Bloop)

  # Net Electric and Magnetic Force
  # F_net = qE + qv x B
  Fmagnetic = multiplyVectorByScalar(crossProduct(v, B), chargeOfElectron)
  print "F_magnetic = {} N".format(pPrint(Fmagnetic))
  
  Felectric = multiplyVectorByScalar(E, chargeOfElectron)
  print "F_electric = {} N".format(pPrint(Felectric))

  Fnet = addVectors(Fmagnetic, Felectric)
  print "F_net = {} N".format(pPrint(Fnet))





######################



def Problem43():
  print "Magnetic levitation"
  print "A metal rod of length w = 19 cm and mass m = 90 grams has metal loops at both ends, which go around two metal poles (see the figure)."
  m = 90.0e-3
  w = 19.0e-2

  print "The rod is in good electrical contact with the poles but can slide freely up and down. The metal poles are connected by wires to a battery, and a current I = 2 amperes flows through the rod. A magnet supplies a large uniform magnetic field B in the region of the rod, large enough that you can neglect the magnetic fields due to the 2 ampere current. The large magnetic field is oriented to have the maximum effect."
  I = 2.0 # amps

  print "The rod floats at rest h = 8 cm above the table. What is the magnetic field in the region of the rod? Assume +x is to the right, +y is up, and +z is out of the page."
  # F_magnetic = -F_gravity
  Fmag = -1.0 * m * gE
  
  # F_magnetic = I Δl x B
  # B = Fmag / I * Δl
  B = Fmag / (I * w)

  print "B = <0, 0, {}>T".format(pPrint(B))
  
  print "Which of the following statements are true?"
  print "If the conventional current in the bar were less than 2 A, the bar would fall."
  print "The magnetic force on the bar is in the  direction (upward)."

  



def Problem55():
  print "Measuring the properties of a slab of material"

  print "A slab made of unknown material is connected to a power supply as shown in the figure. There is a uniform magnetic field of 0.6 tesla pointing upward throughout this region (perpendicular to the horizontal slab). Two voltmeters are connected to the slab and read steady voltages as shown. (Remember that a voltmeter reads a positive number if its positive lead is connected to the higher potential location.) The connections across the slab are carefully placed directly across from each other. The distance w = 0.18 m. Assume that there is only one kind of mobile charges in this material, but we don't know whether they are positive or negative."
  Bmag = 0.6 # magnetic field, pointing up, in Teslas
  w = 0.18 # distance for voltmeter leads on the right side of the slab, in meters

  # Diagram: https://imgur.com/a/6X5XxZm
  width = 8.0e-2 # width of slab, in meters
  height = 1.2e-2 # height of slab, in meters
  Vperpendicular = -0.00027 # voltmeter reading across W, along the full width of the slab
  Vparallel = 0.73 # voltmeter reading across w, along part of the length of the slab

  print "(a) Determine the (previously unknown) sign of the mobile charges, and state which way these charges move inside the slab. Which of the following are true?"
  print "The mobile charges are positive and move out of the page."

  print "(b) In the steady state, the current moves straight along the bar, so the net sideways force on a moving charge must be zero. Use this fact to determine the drift speed v of the mobile charges."
  # transverse magnetic force = electric force
  # F_electric = q E
  # F_magnetic = q v x B
  # q E⊥ = qvB
  # E⊥ = vB
  # E⊥ = ΔV⊥ / d
  # v = ΔV⊥  / Bd
  v = abs(Vperpendicular) / (Bmag * width)
  print "{} m/s".format(pPrint(v))

  print "(c) Knowing the drift speed, determine the mobility u of the mobile charges. (Note that there are two contributions to the electric field in the bar. Think about which one drives the current.)"
  # v = u E
  # E∥ = ΔV∥ / d
  # v = u ΔV∥ / d
  # u = v d / ΔV∥
  u = (v * w) / Vparallel
  print "{} (m/s) / (volts/m)".format(pPrint(u))


  print "(d) The current running through the slab was measured to be 0.3 ampere. If each mobile charge is singly charged (|q| = e), how many mobile charges are there in 1 m^3 of this material?"
  I = 0.3 # current in amps
  A = width * height # area in meters squared
  # I = |q| n A v
  # n = I / (q A v)
  n = I / (chargeOfProton * A * v)
  print "{} carriers/m^3".format(pPrint(n))
  
  print "(e) What is the resistance in ohms of a 0.18 m length of this slab?"
  # I = |ΔV| / R
  # R = |ΔV| / I
  R = Vparallel / I
  print "{} ohms".format(pPrint(R))


def Problem59():
  print "In the figure below a bar 11 cm long with a rectangular cross section 3 cm high and 2 cm deep is connected to a 1.2 V battery and an ammeter. The resistance of the copper connecting wires and the ammeter, and the internal resistance of the battery, are all negligible compared to the resistance of the bar."
  length = 11.0e-2 # meters
  height = 3.0e-2 # meters
  depth = 2.0e-2 # meters
  V = 1.2 # volts

  # diagram: https://imgur.com/a/CUzcqkS
  # see figure 20.20 in chapter 20, section 4 (pg. 816)
  
  print "Using large coils not shown on the diagram, a uniform magnetic field of 2.3 T was applied perpendicular to the bar (out of the page, as shown). A voltmeter was connected across the bar, with the connections across the bar carefully placed directly across from each other."
  B = 2.3 # magnetic field, in teslas

  print "The mobile charges in the bar have charge +e, their density is 7.4 × 10^23/m3, and their mobility is 4.2 × 10-5 (m/s)/(V/m)."
  n = 7.4e23 # electron density
  u = 4.2e-5 # mobility

  print "Predict the readings of the voltmeter and ammeter, including signs."

  # v = ΔV⊥  / Bd
  # v = u E
  # v = I / (q n A)
  # E = ΔV / Δl
  # E∥ = ΔV∥ / length
  Eparallel = V / length
  v = u * Eparallel
  A = height * depth
  Vhall = v * B * height
  # ΔV_Hall = E⊥ * h = vB h = I / (q n A) * Bh
  print "V = {} V".format(pPrint(Vhall))
  # I = |q| n A v
  I = chargeOfProton * n * A * v
  print "I = {} A".format(pPrint(I))


def Problem63():
  print "A neutral metal rod of length 0.45 m slides horizontally at a constant speed of 9 m/s on frictionless insulating rails through a region of uniform magnetic field of magnitude 0.5 tesla, directed out of the page as shown in the diagram. Before answering the following questions, draw a diagram showing the polarization of the rod, and the direction of the Coulomb electric field inside the rod. "
  L = 0.45 # length in meters
  v = 9.0 # speed in meters / second
  B = 0.5 # magnetic field in teslas
  
  # Diagram: https://imgur.com/a/XVjmWHB

  print "Which of the following statements is true?"
  print "The top of the moving rod is negative."

  print "After the initial transient, what is the magnitude of the net force on a mobile electron inside the rod?"
  # at equilibrium the net forces are 0
  print "|F_net| = 0 N"

  print "What is the magnitude of the electric force on a mobile electron inside the rod?"
  # F_el = q E
  # in a steady state:
  # E = vB
  Fel = chargeOfProton * v * B
  print "|F_el| = {} N".format(pPrint(Fel))

  print "What is the magnitude of the magnetic force on a mobile electron inside the rod?"
  # F_mag = q v x B
  Fmag = chargeOfProton * v * B
  # should be the same as F_electric
  print "|F_mag| = {} N".format(pPrint(Fmag))

  print "What is the magnitude of the potential difference across the rod?"
  # ΔV = E * L = vBL
  deltaV = v * B * L
  print "|ΔV| = {} volts".format(pPrint(deltaV))

  print "In what direction must you exert a force to keep the rod moving at constant speed?"
  print "No force is needed"


def Problem69():
  print "Pull a rectangular loop through a magnetic field"
  print "In the figure a rectangular loop of wire L = 18 cm long by h = 4 cm high, with a resistance of R = 0.8 ohms, moves with constant speed v = 3 m/s as shown. The moving loop is partially inside a rectangular region where there is a uniform magnetic field (gray area) and partially in a region where the magnetic field is negligibly small."
  # L = 18.0e-2 # length in meters, not used - can be ignored
  h = 4.0e-2 # height in meters
  R = 0.8 # ohms
  v = 3.0 # speed in meters / second

  # diagram: https://imgur.com/a/Nosjw8K

  print "In the gray region, the magnetic field points into the page, and its magnitude is B = 1.4 tesla."
  B = 1.4 # magnetic field in teslas

  print "(a) Which of the following diagrams correctly shows the charge distribution on the moving loop?"
  print "B"
  # Diagram B: https://imgur.com/a/JVhx3bN

  print "(b) What is the direction of the conventional current?"
  print "counterclockwise"

  print "(c) What is the conventional current in the loop?"
  # ΔV = E * L = vBL
  deltaV = v * B * h
  # I = |ΔV| / R
  I = deltaV / R
  print "{} A".format(pPrint(I))

  print "(d) Which of the following are true? Check all that apply."
  print "Because a current flows in the loop, there is a magnetic force on the loop."
  
  print "(e) What is the magnitude of the magnetic force on the loop?"
  # ΔF_magnetic = I Δl x B
  Fmag = I * h * B
  print "{} N".format(pPrint(Fmag))
  


def Problem71():
  print "A bar magnet whose magnetic dipole moment is <5,0,3.9> A·m2 is suspended from a thread in a region where external coils apply a magnetic field of <0.2,0,0> T. What is the vector torque that acts on the bar magnet? (Express your answer in vector form.)"
  mu = (5.0, 0.0, 3.9) # magnetic dipole moment in amp * meters squared
  B = (0.2, 0.0, 0.0) # magnetic field in teslas
  # τ = μ x B
  torque = crossProduct(mu, B)
  print "τ = {} N m".format(pPrint(torque))


def Problem73():
  print "A bar magnet whose magnetic dipole moment is 13 A·m2 is aligned with an applied magnetic field of 8.6 T. How much work must you do to rotate the bar magnet 180° to point in the direction opposite to the magnetic field?"
  mu = 13.0 # magnetic dipole moment in amp meters squared
  B = 8.6 # magnetic field in teslas
  theta = 180 # degrees
  # work = U_m = -μ * B * cosθ
  U = -1.0 * mu * B * math.cos(math.radians(theta))
  print "{} J".format(pPrint(2.0 * U))


def Problem74():
  print "The center of a bar magnet whose magnetic dipole moment is <6,0,0> A·m2 is located at the origin. A second bar magnet whose magnetic dipole moment is <5, 0, 0> A·m2 is located at x = 0.44 m. What is the vector force on the second magnet due to the first magnet? (Express your answer in vector form.) "
  mu1 = (6.0, 0.0, 0.0)
  mu2 = (5.0, 0.0, 0.0)
  x = 0.44 # meters
  r = (-1.0 * x**4, 0.0, 0.0)

  # F = 3μ_2 * (μ_0 / 4π) * (2μ_1 / x^4)
  F = multiplyVectorByScalar(divideVectors(multiplyVectors(mu2, mu1), r), 3.0 * 1.0e-7 * 2.0)
  
  print "F = {} N".format(pPrint(F))


def Problem75():
  print "A thin rectangular coil 4 cm by 9 cm has 50 turns of copper wire. It is made to rotate with angular frequency 118 rad/s in a magnetic field of 1.9 T."
  A = 4.0e-2 * 9.0e-2 # area in meters squared
  N = 50.0 # turns of wire
  omega = 118.0 # radians per second
  B = 1.9 # teslas

  # V=I/R
  # Umag = mu * B 
  # mu = Iwh = I A
  # Umag = VRBwh

  # emf = omega * B * A * sin(omega * t)  
  print "(a) What is the maximum emf produced in the coil?"
  emf = omega * B * A * N
  print "{} V".format(pPrint(emf))

  print "(b) What is the maximum power delivered to a 55 ohm resistor?"
  R = 55.0 # resistance in ohms

  # Resistive elements dissipate energy (aka power losses) they do not generate power 
  # You don't need the current 
  # You have the voltage aka emf

  # there are two situations The power generated by a device is VI But the power dissipated by a resistive element is V^2/R or I^2R

  # The power generated (i.e. output) is not always equal to the power dissipated by a resistive element, especially if there are more than one dissipative element in the circuit
  power = emf**2 / R
  print "{} W".format(pPrint(power))

# 20.1 - 20.3
Problem28()
Problem43()
Problem31()
Problem33()


# 20.4 - 20.6
Problem55()
Problem59()
Problem63()
Problem69()

# 20.7 - 20.9
Problem71()
Problem73()
Problem74()
Problem75()
