#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys 
import math
import re # regular expression
from decimal import *
from lib import *

# Chapter 22: Faraday's Law
# from Matter & Interactions (4th ed., 2015) 
# by Ruth W. Chabay and Bruce A. Sherwood


def Checkpoint22_03():
  print "A uniform magnetic field of 2.2 tesla points 30° away from the perpendicular to the plane of a rectangular loop of wire 0.1 m by 0.2 m (see the figure). What is the magnetic flux on this loop? "
  B = 2.2 # teslas
  theta = 30.0 # degrees
  A = 0.2 * 0.1
  # ϕ_magnetic = integral of B_perp dA
  flux = B * math.cos(math.radians(theta)) * A
   
  print "{} T * m^2".format(pPrint(flux))
  # Diagram: https://imgur.com/a/x7NIxZ1




def Checkpoint22_04():
  print "A wire of resistance 7 ohms and length 2.5 m is bent into a circle and is concentric with a solenoid in which the magnetic flux changes from 4 T·m2 to 1 T·m2 in 0.3 seconds. What is the emf in the wire?"
  R = 7.0 
  L = 2.5
  flux0 = 4.0
  flux1 = 1.0
  t = 0.3 

  # emf = - dϕ / dt
  emf = -1.0 * (flux1 - flux0) / t
  print "{} V".format(pPrint(emf))

  print "What is the non-Coulomb electric field in the wire?"
  # emf = surface integral of E * dl
  E = emf / L
  print "{} V/m".format(pPrint(E))

  print "What is the current in the wire?"
  # emf = I R
  # I = emf / R
  I = emf / R
  print "{} A".format(pPrint(I))






def Problem22_13():
  print "The north pole of a bar magnet points toward a thin circular coil of wire containing 35 turns (see the figure). The magnet is moved away from the coil, so that the flux through one turn inside the coil decreases by 0.1 tesla·m2 in 0.3 seconds. What is the average emf in the coil during this time interval? Viewed from the right side (opposite the bar magnet), does the current run clockwise or counterclockwise?"
  N = 35.0 
  deltaB = 0.1
  t = 0.3
  emf = N * deltaB / t 
  print "{} volts".format(pPrint(emf))
  print "counterclockwise"




def Problem22_18():
  print "The magnetic field is uniform and out of the page inside a circle of radius  R, and is essentially zero outside the circular region (see the figure). The magnitude of the magnetic field is changing with time; as a function of time the magnitude of the magnetic field is (B_0 + bt^3). r_1 = 3.8 cm, r_2 = 48 cm, B_0 = 1.5 T, b = 1.8 T/s^3, t = 1.6 s, and R = 9 cm."
  r1 = 3.8e-2 
  r2 = 48.0e-2
  B0 = 1.5 # teslas
  b = 1.8 # T/s^3
  t = 1.6 # seconds
  R = 9.0e-2 # meters

  # in class worksheet:
  r1 = 7.0e-2
  r2 = 42.0e-2
  B0 = 1.5
  b = 1.5
  t = 1.0
  R = 11.0e-2


  A = math.pi * R**2
  B1 = B0 + (b * t**3)
  # ϕ_magnetic = integral of B_perp dA
  # emf = - dϕ / dt
  # emf = - d(B_0 + b t^3 * A) / dt
  # B_0 is constant, 
  # emf = - 3 pi r1^2 b t^2 
  # |emf| = 3 b pi r1^2 t^2 
  emf = 3.0 * b * math.pi * r1**2 * t**2
  circumference = math.pi * 2.0 * r1
  # emf = surface integral of E * dl
  E = emf / circumference
  
  

  print "(a) What is the direction of the \"curly\" electric field at location P, a distance r_1 to the left of the center (r_1 < R)?"
  print "up"

  print "(b) What is the magnitude of the electric field at location P? (Hint: remember that \"emf\" is the integral of the non-Coulomb electric field around a closed path.)"
  # print "emf = {}".format(pPrint(emf))
  print "{} N/C".format(pPrint(E))


  print "(c) What is the direction of the \"curly\" electric field at location Q, a distance r_2 to the right of the center (r_2 > R)?"
  print "down"

  print "(d) What is magnitude of the \"curly\" electric field at location Q? "
  emf = 3.0 * b * math.pi * R**2 * t**2
  # print "emf = {}".format(pPrint(emf))
  circumference = math.pi * 2.0 * r2
  E = emf / circumference
  
  print "{} N/C".format(pPrint(E))





##################
def Problem22_20():
  print "A current-carrying wire moves toward a coil"
  print "A long straight wire carrying current I is moving with speed v toward a small circular coil of radius r containing N turns, which is attached to a voltmeter as shown. The long wire is in the plane of the coil. (Only a small portion of the wire is shown in the diagram.) "
  # diagram: https://imgur.com/a/uTEk9aN

  print "The radius of the coil is 0.02 m, and the coil has 11 turns. At a particular instant I = 6 amperes, v = 3.8 meters per second, and the distance from the wire to the center of the coil x = 0.14 m."
  R = 0.02
  N = 11.0
  I = 6.0 # amps
  v = 3.8
  x = 0.14

  I = 7.0
  v = 2.5
  x = 0.13

  A = math.pi * R**2

  print "(a) What is the magnitude of the rate of change of the magnetic field inside the coil? You will need to calculate this algebraically before you can get a number. Write an expression for the magnetic field due to the wire at the location of the coil. Use the approximate formula, since the wire is very long. Remember the chain rule, and remember that v = ⅆx/ⅆt."
  # B is inversely proportional to x:
  # dB/B = -dx/x
  # 
  # dB/dt = -B(dx/dt)/x = -Bv/x. 
  
  B = (1.0e-7) * (2.0 * I) / x
  
  print "ⅆB / ⅆt = {} T/s".format(pPrint(B * v / x))

  print "(b) What is the magnitude of the voltmeter reading? Remember that this includes all 11 turns of the coil."
  emf = N * A * B * v / x
  print "{} volts".format(pPrint(emf))

  print "(c) What is the direction of dvector ⅆB/ⅆt?"
  print "into the page"

  print "(d) What is the direction of the curly electric field in the coil?"
  print "counter-clockwise"


##################
def Problem22_26():
  print "In the figure below a very small circular metal ring of radius r = 0.5 cm and resistance x = 5 Ω is at the center of a large concentric circular metal ring of radius R = 50 cm. The two rings lie in the same plane. At t = 3 s, the large ring carries a clockwise current of 4 A. At t = 3.2 s, the large ring carries a counterclockwise current of 6 A."
  r = 0.5e-2 # radius of small circle
  x = 5.0 # resistance in ohms
  R = 50.0e-2 # radius of large circle 
  t0 = 3.0 # seconds
  t0 = 2.0
  t0 = 5.0

  I0 = 4.0 # amps
  # I0 = 3.0
  
  t1 = 3.2 # seconds
  t1 = 2.3
  t1 = 5.2

  I1 = 6.0 # amps 
  # I1 = 5.0



  # diagram: https://imgur.com/a/7J5e10A
  print "(a) What is the magnitude of the emf in the small ring during this time? "
  # B = μ_0 / 4π * (2π I) / R
  B0 = 1.0e-7 * (2.0 * math.pi * I0) / R
  B1 = 1.0e-7 * (2.0 * math.pi * I1) / R
  print "B0 = {}".format(pPrint(B0))
  print "B1 = {}".format(pPrint(B1))
  A = math.pi * r**2
  print "A = {}".format(pPrint(A))
  deltaB = B1 + B0
  print "delta B = {}".format(pPrint(deltaB))
  deltaT = t1 - t0
  print "delta T = {}".format(pPrint(deltaT))
  # See in the reading chapter 22 section 3 (22.3)
  # |emf| = |dϕ / dt| (sign given by the direction of magnetic force)
  # emf = - dϕ / dt
  emf = (deltaB * A) / deltaT
  print "{} V".format(pPrint(emf))

  print "(b) What are the magnitude and direction of the average current in the small ring?"
  # emf = I R
  # I = emf / R
  I = emf / x
  print "magnitude {} A".format(pPrint(I))
  print "direction: clockwise"

  print "(c) What is the average electric field in the small ring during this time? Give both the magnitude and direction of the electric field."
  # E = emf / L
  E = emf / (2.0 * math.pi * r)
  print "magnitude {} V/m".format(pPrint(E))
  print "direction: clockwise"
  


##################
def Problem22_28():
  print "Tall towers support power lines h = 64 m above the ground and l = 17 m apart that run from a hydroelectric plant to a large city, carrying 60 Hz alternating current with amplitude 1 × 104 A (see figure below). That is, the current in both of the power lines is I = (1 x 10^4 A) sin(2 pi * 60 Hz * t)."
  h = 64.0
  h = 59.0

  l = 17.0
  l = 18.0

  # h = 68.0
  # l = 18.0
  r = math.sqrt(h**2 + (l/2.0)**2)
  print "r = {}".format(pPrint(r))


  print "(a) Calculate the amplitude (largest magnitude) and direction of the magnetic field produced by the two power lines at the base of the tower, when a current of 1 × 104 A in the left power line is headed out of the page, and a current of 1 × 104 A in the right power line is headed into the page."
  I = 1.0e4 # amps
  I = 3.0e4
  I = 5.0e4
  
  # B = μ_0 / 4π * (2I) / r
  B = (1.0e-7) * 2.0 * I / r
  Bnet = 2.0 * B * ((l / 2.0) / r)
  print "magnitude {} T".format(pPrint(Bnet))
  print "direction: up"

  print "(b) This magnetic field is not large compared to the Earth's magnetic field, but it varies in time and so might have different biological effects than the Earth's steady field. For a person lying on the ground at the base of the tower, approximately what is the maximum emf produced around the perimeter of the body, which is about 2 m long by half a meter wide? "
  A = 2.0 * 0.5
  omega = 2.0 * math.pi * 60.0
  # emf = omega * B * A * sin(omega * t)
  # we are looking for max amplitude, so take sin to be its max value of 1
  emf = omega * Bnet * A * 1.0

  # print "emf = {}".format(pPrint(emf))
  print "{} mV".format(pPrint(emf / 1.0e-3))

  # emf = omega * Bnet * A * math.sin(omega * 1.37)
  # print "emf = {}".format(pPrint(emf / 1.0e-3))



def Checkpoint22_07():
  print "To get an idea of the order of magnitude of inductance, calculate the self-inductance in henries for a solenoid with 1300 loops of wire wound on a rod 13 cm long with radius 2 cm."
  N = 1300.0
  d = 13.0e-2
  R = 2.0e-2
  
  # self-inductance of a solenoid:
  # L = μ_0 N^2 / d * πR^2
  # L is the inductance
  # d is the length of the solenoid
  # R is the radius of the solenoid
  # N is the number of loops of wire around the solenoid
  
  L = (4.0 * math.pi * 1.0e-7) * N**2 / d * math.pi * R**2
  
  print "{} H".format(pPrint(L))




def Problem22_38():
  print "A thin coil has 14 rectangular turns of wire. When a current of 2 A runs through the coil, there is a total flux of 4 × 10-3 T·m2 enclosed by one turn of the coil (note that ϕ = kI, and you can calculate the proportionality constant k). Determine the inductance in henries. "
  N = 14.0
  I = 2.0 
  flux = 4.0e-3
  k = flux / I
  
  L = k * N
  print "{} H".format(pPrint(L))



def Problem22_39():
  print "A transformer has a primary coil with 100 turns and a secondary coil of 360 turns. The AC voltage across the primary coil has a maximum of 116 V and the AC current through the primary coil has a maximum of 4 A. What are the maximum values of the voltage and current for the secondary coil?"
  N1 = 100.0
  N2 = 360.0
  emf1 = 116.0
  I1 = 4.0

  # see "Transformers" pg. 920, figure 22.42
  emf2 = (N2/N1) * emf1

  I2 = (N1/N2) * I1

  print "voltage {} V".format(pPrint(emf2))

  print "current {} A".format(pPrint(I2))
  



def Problem22_40():
  print "A 1000-turn solenoid has a radius of 1.5 cm and a length of 22 cm. The current in the solenoid is 9 A."
  N = 1000.0
  R = 1.5e-2
  # R = 1.4e-2
  d = 22.0e-2
  I = 9.0 
  # I = 5.0

  print "(a) What is the inductance of this solenoid? "

  # self-inductance of a solenoid:
  # L = μ_0 N^2 / d * πR^2
  # L is the inductance
  # d is the length of the solenoid
  # R is the radius of the solenoid
  # N is the number of loops of wire around the solenoid
  
  L = (4.0 * math.pi * 1.0e-7) * N**2 / d * math.pi * R**2

  print "{} H".format(pPrint(L))


  print "(b) Inside the solenoid, what is the magnetic energy density (J/m3) far from the ends of the solenoid? "
  
  # Magnetic Energy Density:
  # 1/2 * 1/μ_0 * B^2

  # B = μ_0 * N * I / d
  B = (4.0 * math.pi * 1.0e-7) * N * I / d

  # Magnetic Energy Density = MED
  MED = 0.5 * (1.0 / vacuumPermeability) * B**2

  print "{} J/m^3".format(pPrint(MED))

  print "(c) What is the total magnetic energy, in the approximation that there is little magnetic field outside the solenoid and the magnetic field is nearly uniform inside the solenoid?"
  
  # energy density is energy / volume
  # to get just energy from this, multiply by the volume for the solenoid
  # which is the cross-section into 

  print "{} J".format(pPrint(MED * math.pi * R**2 * d))




def Problem22_42():
  print "A cylindrical solenoid 43 cm long with a radius of 6 mm has 275 tightly-wound turns of wire uniformly distributed along its length (see the figure). Around the middle of the solenoid is a two-turn rectangular loop 3 cm by 2 cm made of resistive wire having a resistance of 105 ohms. One microsecond after connecting the loose wire to the battery to form a series circuit with the battery and a 20 resistor, what is the magnitude of the current in the rectangular loop and its direction (clockwise or counter-clockwise in the diagram)? (The battery has an emf of 9 V.)"

  d1 = 43.0e-2 # length of solenoid in meters
  # d1 = 39.0e-2
  r1 = 6.0e-3 # radius of solenoid in meters 
  # r1 = 7.0e-3
  N1 = 275.0 # number of loops of wire around solenoid
  # N1 = 350.0

  A1 = math.pi * r1**2 # cross-sectional area of the solenoid in meters squared

  N2 = 2.0 # number of loops of the rectangular wire
  A2 = 3.0e-2 * 2.0e-2 # cross-sectional area defined by the rectangular wire
  R2 = 105.0 # resistance of the rectangular wire in ohms
  # R2 = 150.0
  
  R1 = 20.0 
  emf = 9.0

  t = 1.0e-6 # time after connecting battery to wire around solenoid, in seconds

  # L = μ_0 N^2 / d π R^2
  L = vacuumPermeability * N1**2 / d1 * math.pi * r1**2
  print "L = {}".format(pPrint(L))
  
  # |emf| = L |dI/dt|
  # dI/dt = emf / L * e^(-R/L * t)
  # note e^(-R/L * t) is the time constant
  dIdt = (emf / L) * (math.exp((-1.0 * R1 / L) * t))
  print "dI/dt = {}".format(pPrint(dIdt))


  # B = μ_0 N I / d
  # dB/dt = μ_0 N (dI/dt) / d
  dBdt = vacuumPermeability * N1 * dIdt / d1
  print "dB/dt = {}".format(pPrint(dBdt))
  
  # emf = N * A * dB/dt
  emf2 = A1 * dBdt
  print "emf2 = {}".format(pPrint(emf2))

  I2 = N2 * emf2 / R2
  

  # diagram: https://imgur.com/a/RElElpO

  print "{} A".format(pPrint(I2))

  print "clockwise"




  



# Checkpoint22_03()
# Checkpoint22_04()
# Problem22_13()
# Problem22_18()
# Problem22_20()
# Problem22_26()
# Problem22_28()

# Checkpoint22_07()
# Problem22_38()
# Problem22_39()
# Problem22_40()
Problem22_42()
