#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys
import math
import re # regular expression
from decimal import *
from lib import *

#############
# Question 1: 
# Which of the following distributions of charge could produce such a field pattern ?
# a positively charged plate

#############
# Question 2:
# What is a possible charge distribution inside the cylinder that could give this pattern of electric field?
#  An electric dipole along a horizon with the negative charge on the left end

#############
# Question 3:
# A Gaussian cylinder is drawn around some hidden charges.  Given this pattern of electric field on the surface of the cylinder, how much net charge is inside?
# 0

#############
# Question 4:
# All three surfaces have the same area, A and the magnitude of E is the same in all three cases. 
# Rank the three different cases shown in order of increasing electric flux, where 1 represents the least electric flux and 3 represents the greatest electric flux.
# Case B: 2
# Case C: 1
# Case A: 3

#############
# Question 5:
# The electric field is measured over a cylindrical Gaussian surface of radius 5.0 cm and height 15 cm. Everywhere on the curved part of the surface, the electric field E1 is found to be constant in magnitude (550 N/C) and to point radially outward. On the flat end caps, the field E2 varies in magnitude, but also points radially outward.  What surfaces contribute to the electric flux?
# The curved surfaces (i.e. the "sides" of the cylinder)


#############
# Question 6:
# The metal rod in the figure is sliding along a U-shaped track as shown. There is a magnetic field in the blue region which is into the figure. What is the direction of the current in the sliding rod?
# down

#############
# Question 7:
# In which of the above, if any, is the induced current up?
# A and B

#############
# Question 8:
# If the current in the dark wire loop is counter-clockwise and increasing, what is the direction of the induced current in the smaller loop in its middle?
# clockwise


#############
# Question 9:
# What will be the direction of the induced current in the small outer loop in the previous question?
# counterclockwise

#############
# Question 10:
# The following figure shows the current in the circuit I and the induced current Iind. Is the current I increasing or decreasing?
# decreasing



##################
def Question_11():
  print "-----------------"
  print "Question 11"
  print "A transformer has an input of 224 V AC and 5 A.  If the output is 20 V, what is the output current?"
  emf1 = 224.0 # volts
  emf1 = 154.0
  emf1 = 216.0

  I1 = 5.0
  I1 = 7.0
  I1 = 3.0

  emf2 = 20.0
  emf2 = 15.0
  emf2 = 17.0

  # emf2 = (N2/N1) * emf1
  proportionality = emf2 / emf1
  # I2 = (N1/N2) * I1
  I2 = (1.0 / proportionality) * I1
  print "{} A".format(pPrint(round(I2)))

#############
# Question 12:
# The unification of electricity and magnetism was achieved by 
# James Maxwell

#############
# Question 13:
# see: https://en.wikipedia.org/wiki/Electromagnetic_radiation#/media/File:EM_spectrumrevised.png
# Match the appropriate EM waves to their description.
# Lowest frequency EM radiation: radio waves
# Produced in the Sun's core during fusion: gamma rays
# Used for Thermal Imaging: IR
# Stimulates Vitamin D production: UV
# Used for diagnostic imaging and security: X-ray



#############
# Question 14:
# The following signal is representative of 
# FM (frequency modulation).


#############
# Question 15:
# Which of the following is NOT a characteristic of EM waves?
# The electric field and magnetic field oscillate at the same frequency and out of phase.


##################
def Question_16():
  print "-----------------"
  print "Question 16"
  print "Calculate the wavelength of an EM wave with a frequency 21,952,185,646 Hz. The EM wave is traveling with speed c. Use 2.99 x 10^8 m/s."
  f = 21952185646.0
  f = 16707926622.0
  f = 23855023550.0
  wavelength = 2.99e8 / f
  print "{} m".format(pPrint(wavelength))
  

#############
# Question 17:
# Consider an EM wave traveling to the left with the magnetic field component pointing out of the page.  What is the direction of the electric field component?
# down

#############
# Question 18:
# Which of the choices represents the correct order from left to right of the cone and rod cells?  The curve below represents the response of typical human vision.
# blue cones, rod cells, green cones, red cones

#############
# Question 19:
# Which of the following images shows the correct behavior of a converging lens (thick blue line)?  The focal points are the blue squares.
# b

#############
# Question 20:
# A pure blue object illuminated by pure red light will
# appear black

################
def Question_21():
  print "-----------------"
  print "Question 21"
  print "Unpolarized light is incident upon three polarizers.  The first polarizer has a vertical transmission axis, the second has a transmission axis rotated 30.0o with respect to the first, and the third has a transmission axis rotated 75.0o relative to the first.  If the initial light intensity is 1,033 W/m2, calculate the final light intensity."
  theta1 = 30.0 # degrees
  theta2 = 75.0 # degrees
  I_0 = 1033.0 # light intensity in W/m^2
  I_0 = 1862.0
  I_0 = 1279.0

  # Malus's law: 
  # I = I_0 cos^2(theta_i)
  # I_0 is the initial intensity 
  # theta_i is the angle between the light's initial polarization direction and the axis of the polarizer
  I_final = (I_0 / 2.0) * (math.cos(math.radians(theta1)))**2 * (math.cos(math.radians(theta2 - theta1)))**2
  print "I_final = {} W/m^2".format(pPrint(round(I_final)))

#############
# Question 22:
# According to the ray tracing shownn below, the image is
# virtual

#############
# Question 23:
# Mirrors used on the passenger side of vehicles are
# convex

Question_11()
Question_16()
Question_21()
  
  
