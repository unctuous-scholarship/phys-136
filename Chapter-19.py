#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys
import math
import re # regular expression
from decimal import *
from lib import *

# Chapter 19: Circuit Elements
# from Matter & Interactions (4th ed., 2015) 
# by Ruth W. Chabay and Bruce A. Sherwood

def Problem41():
  print "Problem 19.41"
  print "You connect a 9 V battery to a capacitor consisting of two circular plates of radius 0.057 m separated by an air gap of 1.7 mm. What is the charge on the positive plate?"
  V = 9 # volts of the battery connected to the capacitor
  R = 0.057 # radius in meters of capacitor's plates
  s = 1.7e-3 # air gap of capacitor in meters
  A = math.pi * R**2 # area in meters squared
  # Q = C * |ΔV|
  # E = (Q/A) / ε_0
  # |ΔV| = E * s = ((Q/A) / ε_0) * s
  # C = Q / |ΔV|  
  # C = (ε_0 * A) / s
  C = (vacuumPermittivity * A) / s
  
  Q = V * C
  print "{} C".format(pPrint(Q))

def Problem02():
  print "Problem 19.02"
  print "(a) The current through a particular high-resistance (long) bulb when connected to two batteries in series (2.7 volts) is about 100 milliampere (mA); connected to one battery (1.35 volts) the current is about 75 mA; and connected to a small voltage of only 50 millivolts the current is about 5 mA. (Different high-resistance (long) bulbs may differ from these values somewhat.) Using the formula I = |ΔV| / R, what is R for each of these cases?"
  V1 = 2.7 # volts
  I1 = 100e-3 # amps
  V2 = 1.35 # volts
  I2 = 75e-3 # amps
  V3 = 50e-3 # volts
  I3 = 5e-3 # amps
  
  R1 = V1 / I1
  R2 = V2 / I2
  R3 = V3 / I3
  
  print "R 2.7 = {} Ω".format(pPrint(R1))
  print "R 1.35 = {} Ω".format(pPrint(R2))
  print "R 50mV = {} Ω".format(pPrint(R3))

  print "(b) Is a high-resistance (long) bulb an ohmic resistor over this whole range of currents?"
  print "The bulb is not ohmic, because its resistance changes if the current through the bulb changes."
  


def Problem52():
  print "Problem 19.52"
  print "In the circuit shown in the figure below, the emf of the battery is 8.5 V. Resistor R1 has a resistance of 26 Ω, and resistor R2 has a resistance of 44 Ω. A steady current flows through the circuit."
  emf = 8.5 # volts
  R1 = 26 # resistance in ohms
  R2 = 44 # resistance in ohms
  
  print "(a) What is the absolute value of the potential difference across R1?"
  # I = |ΔV| / R
  # emf = I * (Rnet)
  I = emf / (R1 + R2)
  deltaV = I * R1
  print "{} V".format(pPrint(deltaV))

  print "(b) What is the conventional current through R2?"
  print "{} A".format(pPrint(I))
  

def Problem03():
  print "Problem 19.03"
  print "When glowing, a high-resistance (long) bulb has a resistance of about 35 ohms and a low-resistance (round) bulb has a resistance of about 11 ohms. If they are in parallel, what is their equivalent resistance?"
  R1 = 35.0 # ohms
  R2 = 11.0 # ohms
 
  # in parallel:
  # R equivalent = (R1 * R2) / (R1 + R2) 
  Requivalent = (R1 * R2) / (R1 + R2)
  print "{}".format(pPrint((1.0/R1) + (1.0/R2)))
  print "{}".format(pPrint(1.0/Requivalent))
  print "{} Ω".format(pPrint(Requivalent))


  print "How much current goes through two flashlight batteries (3 volts) in series if a high-resistance (long) bulb and a low-resistance (round) bulb are connected in parallel to the batteries?"
  emf = 3 # volts
  # I = |ΔV| / R
  # emf = I * (Requivalent)
  I = emf / Requivalent
  print "{} A".format(pPrint(I))
  


def Problem54():
  print "Problem 19.54"
  print "In the figure below the resistance R1 is 16 Ω, R2 is 6 Ω, and R3 is 32 Ω. If this combination of resistors were to be replaced by a single resistor with an equivalent resistance, what should that resistance be? "
  R1 = 16.0
  R2 = 6.0
  R3 = 32.0
  
  R = R1 + ((R2 * R3)/(R2 + R3))

  print "{} Ω".format(pPrint(R))



def Problem56():
  print "Problem 19.56"
  print "In the circuit shown in the figure below the emf of the battery is 7.2 V. Resistor R1 has a resistance of 29 Ω, resistor R2 has a resistance of 45 Ω, and resistor R3 has a resistance of 66 Ω. A steady current flows through the circuit."
  emf = 7.2 # volts
  R1 = 29.0
  R2 = 45.0
  R3 = 66.0 

  print "(a) What is the equivalent resistance of R1 and R2?"
  R12 = (R1 * R2) / (R1 + R2)
  print "{} Ω".format(pPrint(R12))

  print "(b) What is the equivalent resistance of all three resistors?"
  R = R3 + R12
  print "{} Ω".format(pPrint(R))

  print "(c) What is the conventional current through R3?"
  
  # I = |ΔV| / R
  # emf = I * (Requivalent)
  I = emf / R

  print "{} A".format(pPrint(I))



def Problem58():
  print "Problem 19.58"
  print "The conductivity of tungsten at room temperature, σ = 1.8 ×107 (A/m2)/(V/m), is significantly smaller than that of copper. At the very high temperature of a glowing light-bulb filament (nearly 3000 degrees Kelvin!), the conductivity of tungsten is 18 times smaller than it is at room temperature. The tungsten filament of one of your round bulbs has a radius of about 0.015 mm. Calculate the electric field required to drive 0.10 amperes of current through the glowing bulb and show that it is huge compared to the field in the connecting copper wires."
  sigmaTungsten = 1.8e7 # conductivity of tungsten at room temp.
  conductivityRatioTungsten = 18 # tungsten is 18 times smaller at high temperatures than at room temp
  RTungsten = 0.015e-3 # radius of tungsten filament, in meters
  I = 0.1 # amps 
  A = math.pi * RTungsten**2 # area in meters squared
  
  # For Problem 19.58, to compare the electric field in the filament to the copper wire, you need the conductivity of copper and the radius of the copper wire.
  # Copper's conductivity is  = 5.97 x 107 (A/m2)/(V/m) and a reasonable radius for the copper wire is r = 0.5 mm.
  sigmaCopper = 5.97e7 # conductivity of copper (presumably at room temperature)
  RCopper = 0.5e-3 # radius of copper wire in meters

  # current density
  # J = I / A
  J = I / A
  # J = σ E
  # through the glowing bulb = while hot, conductivity is 18 times smaller
  E = J / (sigmaTungsten / conductivityRatioTungsten)

  print "{} V/m".format(pPrint(E))
  



def Problem64():
  print "Problem 19.64"
  print "In circuit 1 (see the figure), ohmic resistor R1 dissipates 8 watts; in circuit 2, ohmic resistor R2 dissipates 20 watts. The wires and batteries have negligible resistance. The circuits contain 10 V batteries."
  P1 = 8.0 # watts
  P2 = 20.0 # watts
  emf = 10.0 # volts
  # power in watts = I * ΔV
  # I = power / ΔV
  I1 = P1 / emf
  I2 = P2 / emf
  # I = ΔV / R
  # R = ΔV / I
  R1 = emf / I1 
  R2 = emf / I2 

  print "(a) What is the resistance of R1 and of R2?"

  print "R1 = {} Ω".format(pPrint(R1))

  print "R2 = {} Ω".format(pPrint(R2))

  print "(b) Resistor R1 is made of a very thin metal wire that is 3 mm long, with a diameter of 0.1 mm. What is the electric field inside this metal resistor?"
  L1 = 3.0e-3 # meters
  d1 = 0.1e-3 
  r1 = d1 / 2.0
  A1 = math.pi * r1**2
  # ΔV = E * L
  # E = ΔV / L 
  E1 = emf / L1
  print "{} V/m".format(pPrint(E1))

  print "(c) The same resistors are used to construct circuit 3, using the same 10 V battery as before."
  
  print "(d) In circuit 3, calculate the number of electrons entering every second, and the number of electrons entering every second."
  # number of electrons entering every second is current i
  # i = I / |q|
  # I = ΔV / R
  I = emf / (R1 + R2)
  i = I / chargeOfProton

  print "{} electrons/s (R1)".format(pPrint(i))
  # current doesn't change 
  print "{} electrons/s (R2)".format(pPrint(i))

  print "(e) What is the power output of the battery in circuit 3?"
  # power = I * ΔV
  P = I * emf
  print "{} W".format(pPrint(P))


def Problem67():
  print "Problem 19.67"
  print "A certain 2 V battery delivers 18 A when short-circuited. How much current does the battery deliver when a 2 Ω resistor is connected to it? "
  emf = 2.0 # volts
  I = 18.0 # amps
  # R = ΔV / I
  rint = emf / I # internal resistance of the battery
  R = 2.0 # ohms
  # I = ΔV / R
  I2 = emf / (R + rint)
  print "{} A".format(pPrint(I2)) 


def Problem68():
  print "Problem 19.68"
  print "(a) You short-circuit a 12 volt battery by connecting a short wire from one end of the battery to the other end. If the current in the short circuit is measured to be 16 amperes, what is the internal resistance of the battery?"
  emf = 12.0 # volts
  I = 16.0 # amps
  rint = emf / I
  print "{} Ω".format(pPrint(rint))

  print "(b) What is the power generated by the battery?"
  # power in watts = I * ΔV
  P = I * emf
  print "{} W".format(pPrint(P))

  print "(c) How much energy is dissipated in the internal resistance every second? (Remember that one watt is one joule per second.)"
  # one watt = one joule per second
  # joules dissipated every second = power generated by battery
  print "{} J".format(pPrint(P))

  print "(d) This same battery is now connected to a 8 Ω resistor. How much current flows through this resistor?"
  R = 8.0 # ohms
  # I = ΔV / R
  I2 = emf / (R + rint)
  print "{} A".format(pPrint(I2))

  print "(e) How much power is dissipated in the 8 Ω resistor?"
  # ΔV_battery = emf - r_internal * I, where r_int is internal resistance of the battery
  deltaV = emf - rint * I2
  # power in watts = I * ΔV
  P2 = I2 * deltaV
  print "{} W".format(pPrint(P2))

  print "(f) The leads to a voltmeter are placed at the two ends of the battery of this circuit containing the 8 Ω resistor. What does the meter read?"
  print "{} V".format(pPrint(deltaV))


def Problem69():
  print "Problem 19.69"
  print "A 9 V battery is connected to a 230 Ω resistor, and a voltmeter shows the potential difference across the battery to be 7.5 V."
  emf = 9.0 # volts
  R = 230.0 # ohms
  deltaV = 7.5 # volts

  print "(a) What is the internal resistance of the battery? "
  # I = ΔV / R
  I = deltaV / R
  
  # ΔV_battery = emf - r_internal * I, where r_int is internal resistance of the battery
  # r_int * I = emf - ΔV
  # r_int = (emf - ΔV) / I
  rint = (emf - deltaV) / I 
  print "{} Ω".format(pPrint(rint))


  print "(b) If the resistor is replaced by a very low-resistance wire, what does the voltmeter read?"
  print "The voltmeter will read near zero."

  print "(c) What is the current through this wire?"
  # I = ΔV / R
  I2 = emf / rint
  print "{} A".format(pPrint(I2))
  

def Problem71():
  print "Problem 19.71"
  print "You connect an ammeter to a 1.5 V battery whose internal resistance is 0.30 ohm."
  emf = 1.5 # volts
  rint = 0.3 # ohms
  
  print "(a) What does the ammeter read?"
  # I = ΔV / R
  I = emf / rint
  print "{} A".format(pPrint(I))

  print "(b) Replace the ammeter with a voltmeter. What does the voltmeter read?"
  # they already gave us the voltage - 1.5 V battery, so voltmeter should read 1.5 V
  print "{} V".format(pPrint(emf))


def Problem72():
  print "Problem 19.72"
  print "A bent bar"
  print "A 45 cm long high-resistance wire with rectangular cross section 7 mm by 3 mm is connected to a 12 volt battery through an ammeter, as shown in the figure. The resistance of the wire is 56 ohms. The resistance of the ammeter and the internal resistance of the battery can be considered to be negligibly small compared to the resistance of the wire. Leads to a high-resistance voltmeter are connected as shown, with the - lead connected to the inner edge of the wire, at the top (location A), and the + lead connected to the outer edge of the wire, at the bottom (location C). The distance along the wire between voltmeter connections is 5 cm."
  L = 45e-2 # meters, length of high-resistant wire
  A = 7.0e-3 * 3.0e-3 # cross sectional area of wire in meters
  emf = 12.0 # volts
  R = 56.0 # ohms
  d = 5.0e-2 # distance between voltmeter connections, in meters

  print "(c) What is the magnitude of the electric field at location B?"
  # ΔV = E * L
  # E = ΔV / L 
  E = emf / L
  print "{} V/m".format(pPrint(E))

  print "(d) What does the voltmeter read, both magnitude and sign?"
  # ΔV = E * L
  V = E * d
  print "{} V".format(pPrint(V))

  print "(e) What does the ammeter read, both magnitude and sign?"
  # ΔV = IR, I = ΔV/R  
  I = emf / R
  print "{} A".format(pPrint(I * -1.0))

  print "(f) In a 60 second period, how many electrons are released from the - end of the battery?"
  i = I / chargeOfProton
  print "{} electrons".format(pPrint(i * 60.0))

  print "(g) There are 1.5 × 1026 free electrons per cubic meter in the wire. What is the drift speed of the electrons in the wire?"
  n = 1.5e26 # density
  # i = n A v
  # v = i / (n A)
  v = i / (n * A)
  print "{} m/s".format(pPrint(v))

  print "(h) What is the mobility of the material that the wire is made of?"
  # v = u E
  # u = v / E
  u = v / E
  print "{} (m/s)/(V/m)".format(pPrint(u))

  print "(i) Switch meter 1 from being an ammeter to being a voltmeter. Now what is the reading on meter 1, both magnitude and sign?"
  print "{} V".format(pPrint(-1.0 * emf))




def Problem74():
  print "Problem 19.74"
  print "A 24 Ω resistor and a 7 F capacitor are in series with a 3 V battery. What is the initial current when the circuit is first assembled?"
  R = 24.0 # ohms
  C = 7.0 # farads
  emf = 3.0 # V
  # I = (emf - Q/C) / R
  # Initially, though, I = emf / R
  Ii = emf / R
  print "{} A".format(pPrint(Ii))

  print "What is the current after 46 s?"
  t = 46.0 # seconds
  # I = (emf / R) * e^(-t / RC)
  If = Ii * math.exp(-t / (R * C))

  print "{} A".format(pPrint(If))


def Problem76():
  print "Problem 19.76"
  print "Establishing a potential difference"
  print "The deflection plates in an oscilloscope are 10 cm by 2 cm with a gap distance of 1 mm. A 100 volt potential difference is suddenly applied to the initially uncharged plates through a 1075 ohm resistor in series with the deflection plates. How long does it take for the potential difference between the deflection plates to reach 50 volts?"
  A = 10.0e-2 * 2.0e-2 # area of capacitor plate in meters 
  s = 1.0e-3 # capacitor gap in meters
  V = 100.0 # potential difference in volts
  R = 1075.0 # resistance in ohms
  Vf = 50.0 # final potential difference in volts
  
  # C = (ε_0 * A) / s
  C = (vacuumPermittivity * A) / s

  # Charge in a Series RC Circuit: 
  # Q = C * emf * (1 - e^(-t / CR))
  # Q / C = emf * (1 - e^(-t / CR))
  # ΔV_capacitor = Q / C
  # ΔV = emf * (1 - e^(-t / CR))
  # in my terms, Vf = ΔV, V = emf
  # Vf = V * (1 - e^(-t / CR))
  # Vf / V = 1 - e^(-t / CR)
  # Vf / V - 1 = -e^(-t / CR)
  # e^(-t / CR) = 1 - Vf/V
  # ln(e^(-t/CR)) = ln(1 - Vf / V)
  # -t/CR = ln(1 - Vf/V)
  # t / CR = -ln(1 - Vf / V)
  # t = -CR * ln(1 - Vf/V)
  t = -1.0 * C * R * math.log(1.0 - Vf / V)

  print "{} s".format(pPrint(t))


def Problem77():
  print "Problem 19.77"
  print "A capacitor consists of two rectangular metal plates 3 m by 4 m, placed a distance 2.5 mm apart in air (see figure below). The capacitor is connected to a 6 V power supply long enough to charge the capacitor fully, and then the battery is removed."
  A = 3.0 * 4.0 # area of capacitor plate in meters
  s = 2.5e-3 # capacitor gap in meters
  V = 6.0 # volts

  # V = 9.0
  

  # C = (ε_0 * A) / s
  C = (vacuumPermittivity * A) / s
  print "C = {}".format(pPrint(C))

  print "(a) How much charge is on the positive plate of the capacitor?"

  # Q = C * emf * (1 - e^(-t / RC))
  # at full charge, the exponential goes to 0, so 
  # Q = C * emf
  Q = C * V

  print "{} C".format(pPrint(Q))

  print "With the battery still disconnected, you insert a slab of plastic 3 m by 4 m by 1 mm between the plates, next to the positive plate, as shown in figure above. This plastic has a dielectric constant of 3."
  sPlastic = 1.0e-3 # width taken up by plastic in meters
  K = 3.0 # dielectric constant

  # K = 4.0 
  # K = 6.0

  sSpace = s - sPlastic


  # C = (K * ε_0 * A) / s
  Cspace = (vacuumPermittivity * A) / sSpace
  Cplastic = (K * vacuumPermittivity * A) / sPlastic
  C2 = (Cspace * Cplastic) / (Cspace + Cplastic)
  print "C2 = {}".format(pPrint(C2))
  
  # ΔV_capacitor = Q / C
  deltaV = Q / C2

  print "(b) After inserting the plastic, you connect a voltmeter to the capacitor. What is the initial reading of the voltmeter?"
  print "{} V".format(pPrint(deltaV))


  print "(c) The voltmeter has a resistance of 125 MΩ (1.25 × 10^8 Ω). What does the voltmeter read 5 s after being connected? "
  R = 1.25e8 # resistance in ohms
  t = 5.0

  # R = 1.35e8
  # t = 3.0
  # t = 6.0
  

  Vf = deltaV * math.exp(-1.0 * t / (R * C2))
  print "{} V".format(pPrint(Vf))
  


# 19.1 - 19.3
Problem41()
Problem02()
Problem52()
Problem03()
Problem54()
Problem56()
Problem58()


# 19.4 - 19.6
Problem64()
Problem67()
Problem68()
Problem69()
Problem71()
Problem72()


# 19.7 - 19.8
Problem74()
Problem76()
Problem77()
