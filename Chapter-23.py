#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys 
import math
import re # regular expression
from decimal import *
from lib import *

# Chapter 23: Electromagnetic Radiation
# from Matter & Interactions (4th ed., 2015) 
# by Ruth W. Chabay and Bruce A. Sherwood


###################
# Problem 23.19, from 23.2
def Problem23_19():
  print "Electromagnetic radiation is moving to the right, and at this time and place the electric field is horizontal and points out of the page (see the figure). The magnitude of the electric field is E = 3300 N/C. "
  # diagram: https://imgur.com/a/gTu4V5c
  E = 3300.0
  # E = cB
  # B = E / c
  B = E / c
  print "What is the magnitude of the associated magnetic field at this time and place?"
  print "B = {} T".format(pPrint(B))
  print "What is the direction of the associated magnetic field at this time and place?"
  # diagram: https://imgur.com/a/dIbUamF
  print "e, down, -y"




###################
# Problem 23.20, from 23.2
def Problem23_20():
  print "A pulse of electromagnetic radiation is propagating in the +y direction. You have two devices that can detect electric and magnetic fields. You place detector #1 at location <0, -3, 0> m and detector #2 at location <0, 3, 0> m."
  r = 3.0 * 2 
  print "(a) At time t = 0, detector #1 detects an electric field in the -x direction. At that instant, what is the direction of the magnetic field at the location of detector #1?"
  print "+z, out of the page"

  print "(b) At what time will detector #2 detect electric and magnetic fields?"
  # r = v * t
  # t = r / v
  t = r / c
  print "t = {} s".format(pPrint(t))




###################
# Problem 23.21, from 23.2
def Problem23_21():
  print "A pulse of radiation propagates with velocity vector v = <0, 0, -c>. The electric field in the pulse is E = <6.3 × 106, 0, 0> N/C. What is the magnetic field in the pulse?"
  E = 6.3e6
  # E = v B
  # B = E / v
  B = -1.0 * E / c
  print "B = <0, {}, 0> T".format(pPrint(B))




###################
# Problem 23.22, from 23.3
def Problem23_22():
  print "An electric field of 1 × 106 N/C acts on an electron, resulting in an acceleration of 1.4 × 1017 m/s2 for a short time. What is the magnitude of the radiative electric field observed at a location a distance of 1 cm away along a line perpendicular to the direction of the acceleration?"
  E = 1.0e6
  a = 1.4e17
  r = 1.0e-2

  # Producing a Radiative Electric Field
  # E_radiative = (1/4πε_0) * (-qa⊥ / c^2 r)
  E_radiative = (1.0 / (4.0 * math.pi * vacuumPermittivity)) * ((chargeOfProton * a) / (c**2 * r))
  print "{} N/C".format(pPrint(E_radiative))
  



###################
# Problem 23.23, from 23.3
def Problem23_23():
  print "A proton is briefly accelerated in the direction of the arrow labeled a in the figure below. The vector r indicates the location of an observation location relative to the initial position of the proton. The vector a⊥ for this observation location is indicated on the diagram."
  # diagram: https://imgur.com/a/9uM0MLG
  print "(a) If θ is 44°, and the magnitude of the acceleration is 2.6 × 1017 m/s2, what is the magnitude of a⊥?"
  theta = 44.0 # degrees
  a = 2.6e17
  a_perp = a * math.sin(math.radians(theta))
  print "{} m/s^2".format(pPrint(a_perp))

  print "(b) What is the magnitude of the radiative electric field at the indicated observation location, if the magnitude of r is 0.027 m?"
  r = 0.027 # meters

  # Producing a Radiative Electric Field
  # E_radiative = (1/4πε_0) * (-qa⊥ / c^2 r)
  E_radiative = (1.0 / (4.0 * math.pi * vacuumPermittivity)) * ((chargeOfProton * a_perp) / (c**2 * r))
  print "{} N/C".format(pPrint(E_radiative))


  print "(c) At the observation location, what is the direction of the radiative electric field?"
  print "opposite to the direction of a⊥"

  print "(d) At the observation location, what is the direction of the ordinary Coulomb electric field due to the proton?"
  print "the same as the direction of r"

  print "(e) At the observation location, what is the direction of the radiative magnetic field?"
  print "out of the page"





###################
# Problem 23.26, from 23.3
def Problem23_26():
  print "At time t = 0 an electron at the origin is subjected to a force that briefly accelerates it in the -z direction, with an acceleration of 6 × 1017 m/s2. Before trying to answer the following questions, draw a clear diagram."
  a = 6.0e17

  print "Location D is at <-22, 0, 0> m."

  r = 22.0

  print "At what time is a radiative electric field first detected at location D?"

  # r = v * t
  # t = r / v
  t = r / c
  print "{} s".format(pPrint(t))

  print "What is the direction of propagation of electromagnetic radiation that is detected at location D?"
  # The direction of propagation of the radiation is outward from the source charge, in the direction of the unit vector rhat, drawn as usual from the source charge to the observation location. This is also the direction of E x B.
  print "-x"

  print "What is the direction of the radiative electric field observed at location D?"
  print "-z"
  
  print "What is the magnitude of the radiative electric field observed at location D?"
  # E_radiative = (1/4πε_0) * (-qa⊥ / c^2 r)
  E_radiative = (1.0 / (4.0 * math.pi * vacuumPermittivity)) * ((chargeOfProton * a) / (c**2 * r))

  print "|E_rad| = {} V/m".format(pPrint(E_radiative))
  
  
  print "What is the direction of the radiative magnetic field observed at location D?"
  print "-y"


  print "What is the magnitude of the radiative magnetic field observed at location D?"

  # E = v B
  # B = E / v
  B = E_radiative / c
  print "|B_rad| = {} T".format(pPrint(B))

  print "Location H is at < 0, 17, 0 > m."
  print "At what time is a radiative electric field first detected at location H?"
  r = 17.0

  # r = v * t
  # t = r / v
  t = r / c
  print "{} s".format(pPrint(t))

  print "What is the direction of propagation of electromagnetic radiation that is detected at location H?"
  # same as direction of r vector
  print "+y"
  
  print "What is the direction of the radiative electric field observed at location H?"
  # same as direction of acceleration
  print "-z"

  print "What is the magnitude of the radiative electric field observed at location H?"
  # E_radiative = (1/4πε_0) * (-qa⊥ / c^2 r)
  E_radiative = (1.0 / (4.0 * math.pi * vacuumPermittivity)) * ((chargeOfProton * a) / (c**2 * r))
  print "|E_rad| = {} V/m".format(pPrint(E_radiative))

  print "What is the direction of the radiative magnetic field observed at location H?"
  # right hand rule where thumb is +y (E x B), index finger is -z (E), middle finger is -x (B)
  print "-x"

  print "What is the magnitude of the radiative magnetic field observed at location H?"
  # E = v B
  # B = E / v
  B = E_radiative / c
  print "|B_rad| = {} T".format(pPrint(B))






###################
def Problem23_27():
  print "(a) If the electric field inside a capacitor exceeds about 3 × 106 V/m, the few free electrons in the air are accelerated enough to trigger an avalanche and make a spark. In the spark shown in the diagram, electrons are accelerated upward and positive ions are accelerated downward. "
  E = 3.0e6
  # diagram: https://imgur.com/a/pRUKeOu
  # directional diagram: https://imgur.com/a/cJ0DcuS

  print "Which arrow best indicates the direction of propagation of electromagnetic radiation reaching location B?"
  print "c, +x, to the right"
  
  print "Which arrow best indicates the direction of propagation of electromagnetic radiation reaching location C?"
  print "d, +x and -y, to the southeast"
  
  print "What is the direction of the radiative electric field observed at location B?"
  print "a, +y, up, north"

  print "What is the direction of the radiative electric field observed at location C?"
  print "b, +x and +y, northeast"
  
  print "What is the direction of the radiative magnetic field observed at location B?"
  print "j, +z, out of the page"

  print "What is the direction of the radiative magnetic field observed at location C?"
  print "j, +z, out of the page"
  
  print "If you are at location A, 6.7 meters to the left of the capacitor, how long after the initiation of the spark could you first detect a magnetic field?"
  r = 6.7
  
  
  # r = v * t
  # t = r / v
  t = r / c
  print "{} seconds".format(pPrint(t))




###################
def Problem23_29():
  print "An accelerated electron"
  print "An electron is initially at rest. At time t1 = 0 it is accelerated upward with an acceleration of a = 1 × 1018 m/s2 for a very short time (this large acceleration is possible because the electron has a very small mass). We make observations at location A, x = 14 meters from the electron (see the figure)."
  t1 = 0.0
  a = 1.0e18
  x = 14.0
  x = 15.0
  # diagram: https://imgur.com/a/UHPO5sv
  

  print "(a) At time t2 = 4 ns (1 × 10-9 s), what is the magnitude and direction of the electric field at location A due to the electron?"
  t2 = 4.0e-9 # time in seconds
  t2 = 1.0e-9
  # Coulomb
  # E = (1/4πε_0) *  Q/r^2
  E = 9.0e9 * abs(chargeOfElectron) / x**2
  print "magnitude: {} N/C".format(pPrint(E))
  print "direction: to the left" # electron, so E points towards the electron itself

  print "(b) At what time t3 does the electric field at location A change?"

  # r = v * t
  # t = r / v
  t = x / c
  
  print "{} seconds".format(pPrint(t))

  print "(c) What is the direction of the electric field at location A at time t3?"
  print "upward"


  print "(d) What is the magnitude of this electric field?"
  # E_radiative = (1/4πε_0) * (-qa⊥ / c^2 r)
  E_radiative = (1.0 / (4.0 * math.pi * vacuumPermittivity)) * ((chargeOfProton * a) / (c**2 * x))
  print "{} N/C".format(pPrint(E_radiative))
  
  print "(e) A positively charged particle was initially at rest at location A. It is released from rest just after time t_3. Which of the following are true just after time t_3? (Select all that apply.)"
  
  print "✓ The electric force on the positive charge due to the radiative electric field is upward."
  print "✓ The radiative magnetic field at location A is out of the page."
  print "✓ There is a magnetic force to the right on the positive charge."
  print "✓ The positive charge begins to move because there is a force on it due to the radiative electric field."
  print "✗ The positive charge will never be affected by the radiative magnetic field, since the positive charge is always at rest."
  print "✗ The electric force on the positive charge is toward the electron."
  




###################
def Problem23_31():
  print "Radiation and a copper wire"
  print "A slab (pulse) of electromagnetic radiation that is 15 cm thick is propagating downward (in the -y direction) toward a short horizontal copper wire located at the origin and oriented parallel to the z axis, as shown in the figure."

  # the figure: https://imgur.com/a/mXdOD8E

  print "(a) The direction of the electric field inside the slab is out of the page (in the +z direction). What is the direction of the magnetic field inside the slab? "
  print "-x"


  print "(b) You stand on the x axis at location <14, 0, 0> m, at right angles to the direction of propagation of the pulse. Your friend stands on the axis at location <0, 0, 14> m. The pulse passes the copper wire at time t1 = 0, and at a later time t2 you observe new non-zero electric and magnetic fields at your location. What is t2? (give a numerical answer.)"
  r = 14.0
  r = 12.0
  t = r / c

  print "{} s".format(pPrint(t))


  # the electric field hits the copper wire,
  # accelerates electrons in that copper wire in the +z direction
  # now a new electromagnetic radiation is generated by the movement
  # of the electron in the copper wire
  # that new radiation propagates in the +x direction 

  print "(c) What is the direction of the new electric field at your location?"
  print "-z"
  
  print "(d) What is the direction of the new magnetic field at your location?"
  print "+y"

  print "(e) What is the direction of the new electric field at your friend's location?"
  print "none (zero magnitude)"

  print "(f) What is the direction of the new magnetic field at your friend's location?"
  print "none (zero magnitude)"

  print "{}".format(pPrint(0.0456 / (vacuumPermeability * vacuumPermittivity * 0.0032)))





###############
def Checkpoint23_04():
  print "In the vicinity of Earth's orbit around the Sun, the energy intensity of sunlight is about 1300 W/m2. What is the approximate magnitude of the electric field in the sunlight? (What you calculate is actually the \"root-mean-square\" or \"rms\" magnitude of the electric field, because in sunlight the magnitude of the electric field at a fixed location varies sinusoidally, and the intensity is proportional to E^2.)"
  S = 1300.0 # W / m^2
  
  # Poynting Vector
  # energy flux = ε_0 E B c^2
  # E = cB
  # B = E / c
  # so:
  # energy flux = ε_0 E (E / c) c^2
  # energy flux = ε_0 E^2 c
  # S = ε_0 E^2 c 
  # E = sqrt( S / ε_0 * c )
  E = math.sqrt(S / (vacuumPermittivity * c))
  print "{} N/C".format(pPrint(E))

  

  
###############
def Problem23_22():
  print "A particular AM radio station broadcasts at a frequency of 990 kilohertz. What is the wavelength of this electromagnetic radiation?"
  f = 990e3 # hertz
  # f = 1 / T
  # c = λf
  # λ = c / f
  wavelength = c / f
  print "{} m".format(pPrint(wavelength))

  print "How much time is required for the radiation to propagate from the broadcasting antenna to a radio 6 km away?"
  d = 6e3 # meters

  # v = λ / T
  # but here we know that v = c
  # v = d / Δt
  # Δt = d / v
  t = d / c
  print "{} s".format(pPrint(t))



###############
def Problem23_34():
  print "The wavelength of orange light is about 600 nm (1 nanometer = 1 × 10−9 m). What are the frequency and period of the light waves?"
  wavelength = 600e-9 # meters
  # f = 1 / T
  # c = λf
  # f = c / λ
  f = c / wavelength
  
  print "f = {} s^-1".format(pPrint(f))

  T = 1.0 / f
  print "T = {} s".format(pPrint(T))



################
def Problem23_35():
  print "Calculate the wave length for the following examples of sinusoidal electromagnetic radiation. (Note for comparison that an atomic diameter is about 1 × 10^-10 m.)"
  # λ = c / f
  print "radio, 1361 kilohertz {} m".format(pPrint(c/1361.0e3))
  print "television, 113 megahertz {} m".format(pPrint(c/113.0e6))
  print "heat lamp, 4.7 × 10^13 hertz {} m".format(pPrint(c/4.7e13))
  print "red light, 4.8 × 10^14 hertz {} m".format(pPrint(c/4.8e14))
  print "blue light, 7.4 × 10^14 hertz {} m".format(pPrint(c/7.4e14))
  print "x-ray, 4.6 × 10^18 hertz {} m".format(pPrint(c/4.6e18))
  print "gamma rays, 7.3 × 10^20 hertz {} m".format(pPrint(c/7.3e20))



###############
def Problem23_36():
  print "A small laser used as a pointer produces a beam of red light 3 mm in diameter, and has a power output of 4 milliwatts. What is the magnitude of the electric field in the laser beam?"
  d = 3.0e-3 # meters
  A = math.pi * (d/2.0)**2
  S = 4.0e-3 / A # watts/m^2

  # Poynting Vector
  # energy flux = ε_0 E B c^2
  # E = cB
  # B = E / c
  # so:
  # energy flux = ε_0 E (E / c) c^2
  # energy flux = ε_0 E^2 c
  # S = ε_0 E^2 c 
  # E = sqrt( S / ε_0 * c )
  E = math.sqrt(S / (vacuumPermittivity * c))
  print "{} N/C".format(pPrint(E))


#############
def Problem23_37():
  print "A 150 W light bulb is placed in a fixture with a reflector that makes a spot of radius 16 cm. Calculate approximately the amplitude of the radiative electric field in the spot."
  r = 16.0e-2 # radius in meters
  A = math.pi * r**2 # area in meters squared
  S = 150.0 / A # energy flux in Watts / meters squared
   
  # Poynting Vector
  # energy flux = ε_0 E B c^2
  # E = cB
  # B = E / c
  # so:
  # energy flux = ε_0 E (E / c) c^2
  # energy flux = ε_0 E^2 c
  # S = ε_0 E^2 c 
  # E = sqrt( S / ε_0 * c )

  # so far we have been taking the root-mean-square,
  # which is like an average over a wave
  # we want the max amplitude - the crest of the wave
  # the max and min are equidistant,
  # so we have to multiply by two to get the max
  E = math.sqrt(2.0 * S / (vacuumPermittivity * c))

  print "{} N/C".format(pPrint(E))



####################
def Checkpoint23_05():
  print "Since force is dp/dt, the force due to radiation pressure reflected off of a solar sail can be calculated as 2 times the radiative momentum striking the sail per second. In the vicinity of Earth's orbit around the Sun, the energy intensity of sunlight is about 1400 W/m^2. What is the approximate magnitude of the pressure on the sail? (For comparison, atmospheric pressure is about 105 N/m^2.)"
  # see 23.5 Energy and Momentum in Radiation
  # particularly section "Radiation Pressure" on pg. 958
  intensity = 1400.0 # energy intensity of sunlight in W/m^2
  pressure = 2.0 * intensity / c
  # 9.3e-6
  print "{} N/m^2".format(pPrint(pressure))



####################
def Extra23_7_01():
  print "The index of refraction of a particular liquid is 1.36. At what speed would a wave crest in a beam of light travel through this medium?"
  index = 1.36
  # n = c / v
  # v = c / n
  v = c / index
  print "{} m/s".format(pPrint(v))




####################
def Problem23_40():
  print "If a diver who is underwater shines a flashlight upward, toward the surface, at an angle of 30 degrees from the normal, at what angle does the light emerge from the water?"
  theta = 30.0 # degrees
  n_water = 1.33
  n_air = 1.00029
  # n_water * sin(30) = n_air * sin(theta_2)
  theta_2 = math.degrees(math.asin(n_water * math.sin(math.radians(theta)) / n_air))
  print "{} degrees".format(pPrint(theta_2))
  print "Indices of refraction: water: 1.33; air: 1.00029"



####################
def Problem23_41():
  print "To get total internal reflection at the interface of water (refractive index 1.33) and a plastic whose refractive index is 1.51:"
  n_water = 1.33
  n_plastic = 1.51
  print "Which material must the light start in?"
  # total internal reflection only happens when going from a higher index to a lower index, such as from water to air
  # plastic has a higher index than the water, so the light has to be coming from the plastic, and into the water
  print "plastic"

  # n_plastic * sin(theta) = n_water * sin(90) 
  
  theta = math.degrees(math.asin(n_water * math.sin(math.radians(90.0)) / n_plastic))
  print "What is the critical angle?"
  print "{} degrees".format(pPrint(theta))




####################
def Problem23_43():
  print "-------------"
  print "Problem 23.43"
  # Diagram: https://imgur.com/a/qlDCO3z
  print "A spotlight mounted beside a swimming pool sends a beam of light at an angle into the pool. The light hits the water a distance x = 2.4 m from the edge of the pool. The pool is d = 2.0 m deep and the light is mounted h = 1.2 m above the surface of the water. How far from the edge of the pool is the spot of light on the bottom of the pool? (The diagram is not drawn to scale.) Indices of refraction: water: 1.33; air: 1.00029."
  x = 2.4 
  x = 2.8
  d = 2.0
  h = 1.2
  n_water = 1.33
  n_air = 1.00029
  theta_1 = 90.0 - math.degrees(math.atan(h / x))
  print "90 - {} degrees".format(pPrint(math.degrees(math.atan(h / x))))
  print "Angle of the beam from the normal in the air: {} degrees".format(pPrint(theta_1))

  # n_air * sin(theta_1) = n_water * sin(theta_2)
  theta_2 = math.degrees(math.asin(n_air * math.sin(math.radians(theta_1)) / n_water))
  print "Angle of the beam from the normal in the water: {} degrees".format(pPrint(theta_2))

  opposite = math.tan(math.radians(theta_2)) * d
  print "opposite: {}".format(pPrint(opposite))
  print "Distance of spot from edge of pool: {} m".format(pPrint(opposite + x))
  


##################
def Problem23_44():
  print "-------------"
  print "Problem 23.44"
  print "A point source of green light is placed on the axis of a thin converging lens, 18 cm to the left of the lens. The lens has a focal length of 10 cm. Where is the location of the image of the source? (Take the origin to be at the lens and the positive direction to be to the right.) "
  d1 = 18.0e-2 # distance of light source to lens, in meters
  f = 10.0e-2 # length of focal lens in meters
  
  # thin lens equation:
  # 1/f = 1/d1 + 1/d2
  # 1/d2 = 1/f - 1/d1
  d2 = 1.0 / (1.0/f - 1.0/d1)
  print "{} m".format(pPrint(d2))

  print "Is it a real or a virtual image?"
  print "real"
  
  print "If you placed a sheet of paper at the location of the image, what would you see on the paper?"
  print "the image"




######################
def Problem23_45():
  print "-------------"
  print "Problem 23.45"
  print "A point source of green light is placed on the axis of a thin converging lens, 7 cm to the left of the lens. The lens has a focal length of 20 cm. Where is the location of the image of the source? (Take the origin to be at the lens and the positive direction to be to the right.)"
  d1 = 7.0e-2 
  f = 20.0e-2

  # thin lens equation:
  # 1/f = 1/d1 + 1/d2
  # 1/d2 = 1/f - 1/d1
  d2 = 1.0 / (1.0/f - 1.0/d1)
  print "{} m".format(pPrint(d2))


  print "Is it a real or a virtual image?"
  print "virtual"
  
  print "If you placed a sheet of paper at the location of the image, what would you see on the paper?"
  print "nothing"




####################
def Problem23_46():
  print "-------------"
  print "Problem 23.46"
  print "A thin diverging lens of focal length 38 cm is placed 13 cm to the right of a point source of blue light on the axis of the lens. Where is the image of the source? (Take the origin to be at the lens and the positive direction to be to the right.) "
  d1 = 13.0e-2
  f = -38.0e-2 
  f = -35.0e-2

  # thin lens equation:
  # 1/f = 1/d1 + 1/d2
  # 1/d2 = 1/f - 1/d1
  d2 = 1.0 / (1.0/f - 1.0/d1)
  print "{} m".format(pPrint(d2))

  print "Is it a real or a virtual image?"
  print "virtual"
  
  print "If you placed a sheet of paper at the location of the image, what would you see on the paper?"
  print "nothing"



###################
def Problem23_47():
  print "-------------"
  print "Problem 23.47"
  print "A thin converging lens of focal length 15 cm is located at the origin and its axis lies on the x axis. A point source of red light is placed at location <-32,1,0> cm. Where is the location of the image of the source? (Express your answer in vector form.)"
  f = 15.0e-2
  # f = 17.0e-2

  # d_o is the distance between the lens and the object (source of light)
  d_o = 32.0e-2
  # d_o = 26.0e-2

  # h_o is the height of the object (source of light), in particular from the axis through the center of the lens
  h_o = 1.0e-2
  
  # thin lens equation:
  # 1/f = 1/d1 + 1/d2
  # 1/d2 = 1/f - 1/d1
  d_i = 1.0 / (1.0/f - 1.0/d_o)
  # d_i is the distance between the lens and the image (created by the object going through the lens)

  print "d_2 = {}".format(pPrint(d_i))

  M = f / (f - d_o)

  M = - d_i/d_o
  # M = -d_i / d_o = h_i / h_o

  # h1/d1 = h2/d2

  h_i = (h_o/d_o) * d_i
  # h_i is the height of the image 

  print "h_2 = {}".format(pPrint(h_i))

  p = (d_i * 1.0e2, h_i * 1.0e2, 0.0)

  print "p = {} cm".format(pPrint(p))


  print "Is it a real or a virtual image? It helps to draw a diagram and trace the \"easy\" rays."
  # a real image is one which is on the opposite side of the lens from the object
  print "real"



#####################
def Problem23_50():
  print "You have a small but bright computer display that is only 4 cm wide by 2 cm tall. You will place a lens near the display to project an image of the display about 2.5 m tall onto a large screen that is 9 m from the lens of the projector."
  w = 4.0e-2

  h1 = 2.0e-2
  h1 = 3.0e-2

  h2 = 2.5
  h2 = 2.6 
  h2 = 2.4

  d2 = 9.0
  d2 = 6.0

  print "(a) Should you use a converging lens or a diverging lens?"
  print "converging"

  print "(b) What should the focal length of the lens be?"
  # h1/d1 = h2/d2
  d1 = h1 * d2 / h2
  
  # thin lens equation:
  # 1/f = 1/d1 + 1/d2
  f = 1.0 / (1.0/d1 + 1.0/d2)
  print "{} cm".format(pPrint(f * 1.0e2))
  
  print "(c) Suppose that you choose a lens that has the focal length that you calculated in part (b). What exactly should be the distance from the computer display to the lens?"
  print "{} cm".format(pPrint(d1 * 1.0e2))
  
  print "(d) In order that the screen display be right side up, should the computer display be inverted or right side up?"
  print "inverted"

  print "(e) On another occasion the screen is moved close, so that it is only 3 m from the lens. To focus the image, you have to readjust the distance between the computer display and the lens. What is the new exact distance from the computer display to the lens?"
  d2 = 3.0 # distance between screen and lens
  # d2 = 2.0

  # thin lens equation:
  # 1/f = 1/d1 + 1/d2
  # 1/d1 = 1/f - 1/d2
  d1 = 1.0 / (1.0/f - 1.0/d2)
  print "{} cm".format(pPrint(d1 * 1.0e2))
  
  print "(f) How tall is the image on the screen, now that the screen is only 3 m from the lens?"
  # h1/d1 = h2/d2
  h2 = h1/d1 * d2
  print "{} m".format(pPrint(h2))



# Problem23_19()
# Problem23_20()
# Problem23_21()
# Problem23_22()
# Problem23_23()
# Problem23_26()
# Problem23_27()
# Problem23_29()
# Problem23_31()

# Checkpoint23_04()
# Problem23_22()
# Problem23_34()
# Problem23_35()
# Problem23_36()
# Problem23_37()


# Checkpoint23_05()
# Extra23_7_01()

# Problem23_40()
# Problem23_41()
# Problem23_43()
# Problem23_44()
# Problem23_45()
# Problem23_46()
Problem23_47()
# Problem23_50()
