#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys
import math
import re # regular expression
from decimal import *
from lib import *

# Chapter 18: Electric Field and Circuits
# from Matter & Interactions (4th ed., 2015) 
# by Ruth W. Chabay and Bruce A. Sherwood

def Problem27():
  print "The drift speed in a copper wire is 3.53 × 10-5 m/s for a typical electron current. Calculate the magnitude of the electric field inside the copper wire. The mobility of mobile electrons in copper is 4.5 × 10-3 (m/s)/(N/C). (Note that though the electric field in the wire is very small, it is adequate to push a sizable electron current through the copper wire.)"
  v = 3.53e-5
  u = 4.5e-3
  # v bar = u * E
  # E = v / u
  E = v / u 
  print "{} N/C".format(pPrint(E))


def Problem33():
  print "The radius of the thin wire is 0.21 mm, and the radius of the thick wire is 0.4515 mm. There are 4 × 1028 mobile electrons per cubic meter of this material, and the electron mobility is 6 × 10-4 (m/s)/(V/m). If 5 × 1018 electrons pass location D each second, how many electrons pass location F each second?"
  Rthin = 0.21e-3 # radius in meters
  Rthick = 0.4515e-3 # radius in meters
  u = 6.0e-4 # mobility
  n = 4.0e28 # electrons per cubic meter
  ithin = 5.0e18 # electrons per second in location D

  # worksheet
  # Rthin = 0.22e-3
  # Rthick = 0.55e-3
  # ithin = 6.0e18

  Athin = math.pi * Rthin**2
  Athick = math.pi * Rthick**2

  print "Athick: {}"
  print "{} electrons per second".format(pPrint(ithin))

  print "What is the magnitude of the electric field at location F?"
  # v = u * E
  # i = n A v
  # i = n A u E
  # Ethick = i / (n A u)
  Ethick = ithin / (n * Athick * u) 
  print "{} V/m".format(pPrint(Ethick))


def Problem32():

  print "Inside a chemical battery it is not actually individual electrons that are transported from the + end to the − end. At the + end of the battery an \"acceptor\" molecule picks up an electron entering the battery, and at the − end a different \"donor\" molecule gives up an electron, which leaves the battery. Ions rather than electrons move between the two ends to transport the charge inside the battery."
  
  print "When the supplies of acceptor and donor molecules are used up in a chemical battery, the battery is dead, because it can no longer accept or release electrons. The electron current in electrons per second, times the number of seconds of battery life, is equal to the number of donor (or acceptor) molecules in the battery."

  print "A flashlight battery contains approximately half a mole of donor molecules. The electron current through a thick-filament bulb powered by two flashlight batteries in series is about 0.266 A. About how many hours will the batteries keep this bulb lit?"
  I = 0.266 # current in amps
  electrons = I / abs(chargeOfElectron)
  molecules = Na / 2.0 # half a mole of molecules
  # electrons per second * seconds of battery life = number of donor / acceptor molecules in battery
  # seconds of battery life = donor molecules / electrons per second
  seconds = molecules / electrons
  # 60 seconds in a minute, and 60 minutes in an hour
  hours = seconds / 60.0 / 60.0
  print "{} h".format(pPrint(hours))


def Problem35():
  print "(c) In the steady state 1.4 × 1018 electrons per second enter bulb 1. There are 6.3 × 1028 mobile electrons per cubic meter in tungsten. The cross-sectional area of the tungsten filament in bulb 1 is 1.3 × 10-8 m2. The electron mobility in hot tungsten is 1.2 × 10-4 (m/s)/(N/C). Calculate the magnitude of the electric field inside the tungsten filament in bulb 3."
  i1 = 1.4e18
  n1 = 6.3e28 # mobile electron density in tungsten
  A1 = 1.3e-8 # cross sectional area of tungsten filament in bulb 1
  u1 = 1.2e-4 # electron mobility of hot tungsten

  # worksheet variables
  i1 = 1.5e18
  A1 = 1.0e-8
  
  # E = i / (n A u)
  E = i1 / (n1 * A1 * u1)
  print "{} V/m".format(pPrint(E))
  
   

def Problem34():
  print "(b) In the steady state, 4 × 1018 electrons pass location P1 every second. How many electrons pass location P2 every second? "
  i = 4.0e18

  print "{} electrons/s".format(pPrint(i/2.0))

  print "(d) There are 6.3 × 1028 mobile electrons per cubic meter in tungsten. The cross-sectional area of the tungsten filament in bulb B1 is 0.01 mm2 (1 mm2 = 1 × 10-6 m2). The electron mobility in hot tungsten is 1.2 × 10-4 (m/s)/(N/C). Calculate the magnitude of the electric field inside the tungsten filament in bulb B1. "
  n = 6.3e28 # mobile electrons per cubic meter in tungsten
  A = 0.01e-6 # area of bulb 1 in meters squared
  u = 1.2e-4 # electron mobility in hot tungsten
  E = i / (n * A * u)

  print "|E| = {} V/m".format(pPrint(E))
  



def Problem37():
  print "A Nichrome wire 44 cm long and 0.20 mm in diameter is connected to a 2.3 V flashlight battery. What is the electric field inside the wire? (Enter the magnitude.)"
  L = 44.0e-2 # length of wire in meters
  d = 0.2e-3 # diameter of wire in meters
  V = 2.3 # volts of flashlight battery
  E = V / L
  print "{} V/m".format(pPrint(E))



def Problem46():
  print "In the circuit shown in the diagram below, two thick wires connect a 1.6 volt battery to a Nichrome (NiCr) wire. Each thick connecting wire is 15 cm long, and has a radius of 9 mm. The thick wires are made of copper, which has 8.36 × 1028 mobile electrons per cubic meter, and an electron mobility of 4.34 × 10-3 (m/s)/(V/m). The Nichrome wire is 6 cm long, and has a radius of 4 mm. Nichrome has 9 × 1028 mobile electrons per cubic meter, and an electron mobility of 7 × 10-5 (m/s)/(V/m)."
  V = 1.6 # volts of battery

  Lthick = 15.0e-2 # length of one thick connecting wire (there are two), in meters
  Rthick = 9.0e-3 # radius of thick wires
  nthick = 8.36e28 # mobile electrons / m^3 in copper
  uthick = 4.34e-3 # electron mobility of copper
  Athick = math.pi * Rthick**2

  Lthin = 6.0e-2 # length of thin Nichrome wire
  Rthin = 4.0e-3 # radius of thin Nichrome wire
  nthin = 9.0e28 # mobile electrons / m^3 in Nichrome wire
  uthin = 7.0e-5 # electron mobility of Nichrome wire
  Athin = math.pi * Rthin**2


  print "What is the magnitude of the electric field in the thick copper wire?"
  # we know the current for the thick copper wire
  # is the same as the current in thin nichrome wire
  # i = i
  # electron density (thick) * area (thick) drift speed (thick) = electron density (thin) * area (thin) drift speed (thin) 
  # nthick Athick uthick Ethick = nthin Athin uthin Ethin
  ratio = (nthick * Athick * uthick) / (nthin * Athin * uthin)
  print "ratio: {}".format(pPrint(ratio))
  # ratio * Ethick = Ethin

  ####################
  # Diagram of circut 
  # A is positive pole of battery
  # D is negative pole
  # B to C is the thin, nichrome wire
  # A to B and C to D is thick, copper wire

  # ____A| |D____
  # |    |      |
  # |           |
  # |___B==C____|

  # The sum of the potential differences along a round-trip path must be zero.
  # ΔV = 0
  # ΔV_AB + ΔV_BC + ΔV_CD + ΔV_DA = 0

  # ΔV_AB = -Ecopper * Lcopper 
  # ΔV_BC = -Enichrome * Lnichrome
  # ΔV_CD = -Ecopper * Lcopper 
  # ΔV_DA = 1.6 Volts
  # so, 1.6V = 2 * (Ecu * Lcu) + (Enich * Lnich) 
  # Ethick * ratio = Ethin
  # V = 2 * (Ethin/ratio * Lthick) + (Ethin * Lthin)
  # V = (Ethin) * [2 (1/ratio * Lthick) + (Lthin)]
  Ethick = V / ((2.0 * (Lthick)) + (ratio * Lthin))

  print "{} V/m".format(pPrint(Ethick))

  print "What is the magnitude of the electric field in the thin Nichrome wire?"
  Ethin = Ethick * ratio
  print "{} V/m".format(pPrint(Ethin))


def Problem50():
  print "Two Nichrome wires"
  print "The following questions refer to the circuit shown in the figure, consisting of two flashlight batteries and two Nichrome wires of different lengths and different thicknesses as shown (corresponding roughly to your own thick and thin Nichrome wires). The thin wire is 54 cm long, and its diameter is 0.25 mm. The thick wire is 15 cm long, and its diameter is 0.38 mm."
  Lthin = 54.0e-2 # length of thin wire in meters
  dthin = 0.25e-3 # diameter of thin wire
  Rthin = dthin / 2.0
  Athin = math.pi * Rthin**2
  
  Lthick = 15.0e-2 
  dthick = 0.38e-3
  Rthick = dthick / 2.0
  Athick = math.pi * Rthick**2

  print "(a) The emf of each flashlight battery is 1.5 volts. Determine the steady-state electric field inside each Nichrome wire. Remember that in the steady state you must satisfy both the current node rule and energy conservation. These two principles give you two equations for the two unknown fields."
  emf = 1.5 # volts
  
  # i = i
  # Athick Ethick = Athin Ethin
  # Ethin = (Athick / Athin) * Ethick
  # Ethin = ratio * Ethick
  ratio = Athick / Athin

  # The sum of the potential differences along a round-trip path must be zero.
  # ΔV = 0

  # V = (Ethick * Lthick) + (Ethin * Lthin)
  # now put it all in terms of Ethick using the ratio
  # V = (Ethick * Lthick) + (Ethick * ratio * Lthin)
  # V = (Ethick) [Lthick + (ratio * Lthin)]
  Ethick = (2.0 * emf) / (Lthick + (ratio * Lthin))
  Ethin = Ethick * ratio
  print "E inside, thin, wire = {} V/m".format(pPrint(Ethin))

  print "E inside, thick, wire = {} V/m".format(pPrint(Ethick))


  print "(b) The electron mobility in room-temperature Nichrome is about 7 × 10-5 (m/s)(N/C). How long (in minutes) does it take an electron to drift through the two Nichrome wires from location B to location A?"
  u = 7.0e-5 
  # v = u E
  vthick = u * Ethick
  vthin = u * Ethin

  thickseconds = Lthick / vthick
  thinseconds = Lthin / vthin

  minutes = (thinseconds + thickseconds) / 60.0
  print "{} min".format(pPrint(minutes))


  print "(c) On the other hand, about how long did it take to establish the steady state when the circuit was first assembled?"
  
  # light speed
  seconds = (Lthick + Lthin) / c
  print "{} s".format(pPrint(seconds))

  print "(d) There are about 9 × 1028 mobile electrons per cubic meter in Nichrome. How many electrons cross the junction between the two wires every second?"

  n = 9.0e28
  # i = n A u E 
  # ithick = n * Athick * u * Ethick
  ithin = n * Athin * u * Ethin
  # i = ithick + ithin
  print "{} electrons/s".format(pPrint(ithin))
  


def Problem53():
  print "A circuit is assembled that contains a thin-filament bulb and a thick-filament bulb as shown in the figure below, with four compasses placed underneath the wires (we're looking down on the circuit). When the thin-filament bulb is unscrewed from its socket, the compass on the left (next to the battery) deflects 16°. When the thin-filament bulb is screwed back in and the thick-filament bulb is unscrewed, the compass on the left deflects 8°."

  theta1nothin = 16 # in degrees
  theta1nothick = 8 # in degrees

  # in class:
  # theta1nothin = 15 # in degrees
  # theta1nothick = 4 # in degrees

  # Diagram of circuit:
  # Compasses by numbers: 1, 2, 3, and 4
  # two batteries (long ___ is positive pole, short _ is negative pole)
  # (Thick) and (thin) bulbs

  #         __2__(thin)__
  #   ___1__|            |
  #   |     |_3__(thick)_|
  #   _                  |
  #  ___                 |
  #   |              ____|
  #   _              |
  #  ___             |
  #   |              |
  #   |              |
  #   |__4___________|

  # I1 = I4
  # I1 = I2 + I3
  # I4 = I2 + I3
  
  # B1 = B2 + B3 = Bnet
  # tan theta = Bearth / Bnet
  # Bearth / B2 = tan(theta2)
  # Bearth / B1 = tan(theta1)
  # tan(theta) = tan(theta2) + tan(theta3)
  theta = math.degrees(math.atan(math.tan(math.radians(theta1nothin)) + math.tan(math.radians(theta1nothick))))
  print "{} degrees".format(pPrint(theta))

  print "With both bulbs screwed into their sockets, draw the orientations of the needle on each compass, and write the compass deflection in degrees beside the compass."

  # note that theta1 and theta2 are the same, but different directions - based on the change of conventional current direction (from Northward at 4 to Southward at 1)
  # tan(theta1) = tan(theta2) + tan(theta3)
  theta1 = math.degrees(math.atan(math.tan(math.radians(theta1nothin)) + math.tan(math.radians(theta1nothick)))) * -1
  print "compass 1 {} counterclockwise from the North".format(pPrint(theta1))

  theta2 = theta1nothick * -1
  print "compass 2 {} counterclockwise from the North".format(pPrint(theta2))

  theta3 = theta1nothin * -1
  print "compass 3 {} counterclockwise from the North".format(pPrint(theta3))

  # tan(theta4) = tan(theta2) + tan(theta3)
  theta4 = math.degrees(math.atan(math.tan(math.radians(theta1nothin)) + math.tan(math.radians(theta1nothick))))
  print "compass 4 {} counterclockwise from the North".format(pPrint(theta4))
  
  
def Problem40():
  print "In a circuit with one battery, connecting wires, and a 12 cm length of Nichrome wire, a compass deflection of 6° is observed. What compass deflection would you expect in a circuit containing two batteries in series, connecting wires, and a 24 cm length of thicker Nichrome wire (double the cross-sectional area of the thin piece)? (Take the horizontal component of the Earth's magnetic field to be 2 × 10-5 T.)"
  L1 = 12.0e-2 # length of Nichrome wire in meters
  theta1 = 6 # deflection in degrees
  L2 = 24.0e-2 # length of thicker nichrome wire
  Bearth = 2.0e-5
  
  # ΔV = E * L = emf
  # E = emf / L
  # with two batteries, emf is doubled
  emfRatio = 2.0
  # from 12 to 24 centimeters means length is doubled as well
  lengthRatio = L2 / L1

  # this is just 1 - since 2 / 2 = 1
  Eratio = emfRatio / lengthRatio
  # i = n * A * u * E
  # cross sectionar area, A, is doubled
  areaRatio = 2.0
  
  # tan = Bwire / Bearth
  # Bwire = tan(6°) * Bearth
  Bwire1 = math.tan(math.radians(theta1)) * Bearth
  
  # The magnetic field due to the wire is proportional to the current through the wire.
  # in this example, the areaRatio is 2 and the Eratio is 1
  # so the magnetic field is just doubled
  Bwire2 = Bwire1 * areaRatio * Eratio

  # then re-calculate the deflection based on this new magnetic field
  # tan = Bwire / Bearth
  # arctan(Bwire / Bearth) = our new deflection
  theta2 = math.degrees(math.atan(Bwire2 / Bearth))

  print "{} degree(s)".format(pPrint(theta2))
  

def Problem41():
  # this problem is the same as Checkpoint 8 in 18.9 in the book
  print "In the figure below, suppose that VC - VF = 7.3V, and VD - VE = 1.4V."
  VFC = 7.3 # volts
  VED = 1.4 # volts
  print "(a) What is the potential difference VC - VD?"
  print "{} V".format(pPrint(VFC - VED))
  

def Problem42():
  print "What would be the potential difference VC - VB across the thin resistor in the figure if the battery emf is 6.1V? Assume that the electric field in the thick wires is very small (so that the potential differences along the thick wires are negligible)."
  emf = 6.1 # volts

  # note: the potential differences along the thick wires are negligible
  # so.... ignore the thick wires, all you have left is the resistor, which must account for the whole emf ...
  print "{} V".format(pPrint(emf * -1))


def Problem51():
  print "A Nichrome wire 77 cm long and 0.25 mm in diameter is connected to a 1.6 volt flashlight battery. What is the electric field inside the wire? "
  L = 77.0e-2 # length of Nichrome wire
  # d = 0.25e-3 # diameter of nichrome wire
  # R = d / 2.0 # radius of nichrome wire
  V = 1.6 # volts of flashlight battery
  
  E = V / L
  print "{} V/m".format(pPrint(E))

  print "The Nichrome wire is replaced by a wire of the same length and diameter, and same mobile electron density but with electron mobility 5 times as large as that of Nichrome. Now what is the electric field inside the wire?"
  # uRatio = 5.0

  # it's a trick question - while drift speed is impacted by mobility,
  # the Electric field remains the same
  print "{} V/m".format(pPrint(E))



   

# 18.1 - 18.3
Problem27()
Problem33()
Problem32()
Problem35()
Problem34()

# 18.8 - 18.11
Problem37()
Problem46()
Problem53()
Problem40()
Problem41()
Problem42()
Problem51()
Problem50()
