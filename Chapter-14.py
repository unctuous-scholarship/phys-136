#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys
import math
import re # regular expression
from decimal import *
from lib import *

# Chapter 14: Electric Fields and Matter
# from Matter & Interactions by Ruth W. Chabay and Bruce A. Sherwood


def Problem03():

  print "A typical atomic polarizability is 1 × 10^-40 (C·m)/(N/C). If the q in p = qs is equal to the proton charge e, what charge separation s could you produce in a typical atom by applying a large field of 4 × 10^6 N/C, which is larger than the field required to cause a spark in air?"
  alpha = 1.0e-40 
  E = 4.0e6 # electric field in Newtons/Coulomb
  # p = alpha * E

  q = chargeOfProton

  # |p| = qs
  # s = |p| / q
  s = E * alpha / q
  print "{} m".format(pPrint(s))




def Problem27():

  print "You rub a plastic comb through your hair and it now carries a charge of -8.3×10^-9 C. What is the charge on your hair?"

  qcomb = -8.3e-9 # C

  # q of comb + q of hair = 0
  # q of hair = -(q of comb)
  qhair = -1.0 * qcomb
  print "{} C".format(pPrint(qhair))




def Problem37(): 

  print "An electron and a neutral carbon atom are initially 7.3 × 10^-6 m apart (about 73000 atomic diameters), and there are no other particles in the vicinity. The polarizability of a carbon atom has been measured to be 1.96 × 10^-40 C · m/(N/C)."
  p = 1.96e-40 # polarizability of Carbon atom
  r = 7.3e-6 # initial distance between atoms
  
  print "(a) Calculate the initial magnitude and direction of the acceleration of the electron."

  # Acceleration = Force / mass = Coulomb's constant^2 * (2 * alpha * charge^2) / (mass * distance^5)
  massOfElectron = 9.109e-31 # kg
  alpha = p # lol, not sure why polarizability could be used as alpha UNLESS the electric field is 1
  a = (ke**2) * (2.0 * alpha * (chargeOfElectron**2)) / (massOfElectron * (r**5))

  

  print "magnitude: {} m/s^2".format(pPrint(a))
  print "direction: toward the carbon atom"

  print "(b) If the electron and carbon atom were initially 3 times as far apart, how much smaller would the initial acceleration of the electron be? (Enter the ratio of the magnitudes of the accelerations.)"
  print "|α at 3r / α at r| = {}".format(pPrint(1.0/3.0**5))

def Problem38():
  print "A water molecule and a neutral carbon atom are initially 7.7 × 10^-6 m apart (about 77000 atomic diameters), and there are no other particles in the vicinity. The polarizability of a carbon atom has been measured to be  1.96 × 10^-40 C · m/(N/C). A water molecule has a permanent dipole moment whose magnitude is 6.2 × 10^−30 C · m, which is much larger than the induced dipole for this situation. Assume that the dipole moment of the water molecule points toward the carbon atom. (Also assume the carbon atom is ^12C.)"

  r = 7.7e-6 # distance between water molecule and carbon atom, in meters
  p = 1.96e-40 # polarizability
  moment = 6.2e-30 # dipole moment of water molecule, note: dipole moment = charge * distance between poles 
  

  print "(a) Calculate the initial magnitude and direction of the acceleration of the water molecule."

  alpha = p
  # mass of water molecule = mass of hydrogen * 2 + mass of oxygen
  # water is 18.01528 grams / mole
  massOfWaterMolecule = 18.01528e-3 / Na
  # formula given by prof: 
  # F is approximately (1/ 4 * pi * E0)^2 * (12 * alpha * μ^2 / r^7)
  a = (ke**2) * (12.0 * alpha * (moment**2)) / (massOfWaterMolecule * (r**7))
  print "magnitude: {} m/s^2".format(pPrint(a))
  print "direction: toward the carbon atom"

  print "(b) If the water molecule and carbon atom were initially 3 times as far apart, how much smaller would the initial acceleration of the water molecule be? (Enter the ratio of the magnitudes of the accelerations.)"
  print "|α at 3r / α at r| = {}".format(pPrint(1.0/3.0**7))





def Problem55():
  print "In a particular metal, the mobility of the mobile electrons is 0.0070 (m/s)/(N/C). At a particular moment the net electric field everywhere inside a cube of this metal is 0.029 N/C in the +x direction. What is the average drift speed of the mobile electrons in the metal at this instant?"
  m = 0.007 # mobility in (meters / seconds * Newtons / Coulombs)
  Enet = 0.029 # net electric field in Newtons / Coulombs
  v = m * Enet
  print "{} m/s".format(pPrint(v))




def Problem56():
  print "A neutral solid metal sphere of radius 0.1 m is at the origin, polarized by a point charge of 3 × 10−8 C at location <-0.4, 0, 0> m. At location <0, 0.02, 0> m, what is the electric field contributed by the polarization charges on the surface of the metal sphere? (Express your answer in vector form.)"
  radius = 0.1 # meters
  q = 3.0e-8 # Coulombs
  chargeLocation = (-0.4, 0.0, 0.0) # meters
  obsLocation = (0.0, 0.02, 0.0) # meters

  r = subtractVectors(obsLocation, chargeLocation)
  rmag = magnitudeOfVector(r)
  rhat = unitVector(r)
  # Enet = Echarges + Epol = 0
  Echarges = multiplyVectorByScalar(rhat, -1.0 * ke * q / rmag**2)
  print "E charges = {} N/C".format(pPrint(Echarges))

def Problem60():

  print "In the figure a very thin spherical plastic shell of radius 15 cm carries a uniformly-distributed negative charge of -5 nC (-5 × 10-9 C) on its outer surface. An uncharged solid metal block is placed nearby. The block is 10 cm thick, and the left surface of the block is 10 cm away from the surface of the sphere."

  q = -5.0e-9 # Coulombs
  radius = 15.0e-2 # meters
  
  print "Calculate the magnitude of E block, the electric field at the center of the metal block, due only to the charges on the block itself. (Hint: make sure you use the appropriate distance in your calculation.)"

  Eblock = ke * abs(q) / (radius + 10.0e-2 + 5.0e-2)**2  
  print "|E block| = {} N/C".format(pPrint(Eblock))


  print "What is the magnitude of the net electric field at the center of the metal block?"

  print "|E net| = 0 N/C"

def Problem63():
  print "Blocks A and B are identical metal blocks. Initially block A is neutral, and block B has a net charge of 8.6 nC. Using insulating handles, the blocks are moved so they touch each other. After touching for a few seconds, the blocks are separated (again using insulating handles)."

  qB = 8.6 # nano Coulombs (nC = C x 10^-9)
  
  print "(a) What is the final charge of block A?"

  # electrons move from A to B, leaving both blocks at equal charge - half of the original charge of B
  print "{} nC".format(pPrint(qB / 2.0))


Problem03()
Problem27()
Problem37()
Problem38()

# 
Problem55()
Problem56()
Problem60()

# 14.7 - 14.8
Problem63()





