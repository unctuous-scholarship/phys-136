#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys 
import math
import re # regular expression
from decimal import *
from lib import *


# 2020-04-03

def Problem1():
  
  print "Along the path shown in the figure,  the magnetic field is measured and is found to be uniform in magnitude and always tangent to the circular path."
  print "What is the current enclosed if B = 1.1 x 10-6 T?"
  # B_wire = μ_0 / 4π * 2I / r
  # I = B_wire * r / (2 * μ_0 / 4π)
  r = 3.0e-2 
  B = 1.1e-6 
  
  I = B * r / (2.0e-7)
  print "1: {} A".format(pPrint(I))

# 2:
# In reference to the previous question, what is the direction of the current enclosed?
# B spins clockwise, so I points into the page (-z)

def Problem2():
  print "The magnetic field has been measured to be horizontal everywhere along a rectangular path 𝑥=20.0 cm long and 𝑦= 3.00 cm high."
  x = 21.0e-2 
  y = 3.0e-2
  print "B1 = 1.60 x 10-4 T, B2 = 1.00 x 10-4 T, and  B3 = 0.800 x 10-4 T."
  B1 = 1.6e-4
  B2 = 1.0e-4 # perpendicular to the current we care about, can be ignored
  B3 = 0.8e-4
  B = B1 - B3
  I = (B * x) / (4.0 * math.pi * 1.0e-7)
  print "2: {} A".format(pPrint(I))


  print "What is the electric current in the area that is surrounded by the rectangular path?"


# 4:
# In reference to the previous question, what is the direction of the current?
# z
# imagine the B1 increasing and the B3 decreasing as a result of a current running out of the page, 
# creating a B that pushes against an external current's magnetic field



Problem1()
Problem2()
