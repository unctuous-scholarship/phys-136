#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys 
import math
import re # regular expression
from decimal import *
from lib import *

# Chapter 21: Patterns of Field in Space
# from Matter & Interactions (4th ed., 2015) 
# by Ruth W. Chabay and Bruce A. Sherwood

def Question05():
  print "In the figure the magnetic field in a region is vertical and was measured to have the values shown on the surface of a cylinder. Which of the following are true?"
  # diagram: https://imgur.com/a/zRPBdvE

  # read 21.5: magnetic fields don't have monopoles, Gauss's Law for magnetism says there is a net flux of zero for magnetism within a closed surface

  print "✗ The measurements imply that the box contains nothing at all."

  print "✗ The measurements imply that the box contains a current-carrying loop of wire."

  print "✓ The magnetic flux over the closed box is nonzero, which violates Gauss's Law for magnetism."

  print "✗ The measurements imply that the box contains a bar magnet."

  print "✓ The measurements are probably incorrect, since we have never yet found a magnetic monopole." 


def Extra21_2_01():
  # diagram: https://imgur.com/a/Xg5Ibl5
  print "(a) Suppose that the small area ΔA in the figure measures 1 mm by 3 mm, and it is oriented in such a way that its outward-going normal is at an angle of 29 degrees to the direction of the electric field. What is ΔA⊥?"
  A = 1.0e-3 * 3.0e-3 # area in meters
  theta = 29.0 # degrees

  # pg. 870: Perpendicular Field or Perpendicular Area
  # if E⊥ = E cos(θ), 
  # then ΔA⊥ = ΔA cos(θ) 
  deltaAperp = A * math.cos(math.radians(theta))
  print "{} m^2".format(pPrint(deltaAperp))

  
  print "(b) If the electric field is 1000 volts/meter, what is the flux EΔA⊥?"
  E = 1000.0 # volts / meter
  flux = E * deltaAperp

  print "{} V·m".format(pPrint(flux))

def Extra21_2_02():
  print "If the electric field in the figure is 1200 volts/meter, and the field is at an angle of 26 degrees to the outward-going normal, what is E⊥?"
  E = 1200.0 # V/m
  theta = 26.0 # degrees
  
  # E⊥ = E cos(θ) 
  Eperp = E * math.cos(math.radians(theta))

  # diagram: https://imgur.com/a/nkeFhpl

  print "{} V/m".format(pPrint(Eperp))


def Problem21_06():
  print "The figure shows a disk-shaped region of radius 2 cm, on which there is a uniform electric field of magnitude 289 volts/meter at an angle of θ = 43 degrees to the plane of the disk."

  R = 2.0e-2 # radius in meters
  E = 289.0 # electric field in Volts / Meter
  theta = 90.0 - 43.0 # degrees
  # diagram: https://imgur.com/a/fJVwSuO 
  deltaA = math.pi * R**2
  Eperp = E * math.cos(math.radians(theta))

  print "Calculate the electric flux on the disk, and include the correct units."
  # electric flux on a surface =  Σ E * n * ΔA 
  # electric flux on a surface =  ϕ_el
  # ϕ_el = ∫ E * n * ΔA
  
  flux = E * math.cos(math.radians(theta)) * deltaA
  print "{} V·m".format(pPrint(flux))


def Problem21_07():
  # diagram: https://imgur.com/a/VXm3eQe
  print "The figure shows a box on whose surfaces the electric field is measured to be horizontal and to the right. On the left face (3 cm by 2 cm) the magnitude of the electric field E1 is 220 volts/meter, and on the right face the magnitude of the electric field E2 is 1030 volts/meter. On the other faces only the direction is known (horizontal). Calculate the electric flux on every face of the box, the total flux, and the total amount of charge that is inside the box."

  E1 = 220.0
  E2 = 1030.0
  A = 3.0e-2 * 2.0e-2 

  print "Flux through top"
  print "0 Vm"
  print "Flux through bottom"
  print "0 Vm"

  print "Flux through left"
  flux_left = E1 * math.cos(math.pi) * A
  print "{} Vm".format(pPrint(flux_left))

  print "Flux through right"
  flux_right = E2 * math.cos(0) * A
  print "{} Vm".format(pPrint(flux_right))

  print "Flux through front"
  print "0 Vm"
  print "Flux through back"
  print "0 Vm"

  print "Total Flux"
  flux = flux_left + flux_right
  print "{} Vm".format(pPrint(flux))

  print "Total charge inside box (recall ε_0 = 8.85 × 10-12 C2/N·m2)"
  Q = flux * vacuumPermittivity
  print "{} C".format(pPrint(Q))

def Problem21_10():
  print "Simple applications"
  print "(a) The electric field has been measured to be vertically upward everywhere on the surface of a box 20 cm long, 4 cm high, and 3 cm deep, shown in the figure. All over the bottom of the box E1 = 1400 V/m, all over the sides E2 = 1050 V/m, and all over the top E3 = 800 V/m."
  length = 20.0e-2 
  height = 4.0e-2 
  depth = 3.0e-2
  E1 = 1400.0
  # E1 = 1100.0 # in class worksheet
  # E2 = 1050.0 # can ignore E2 - not contributing to flux
  E3 = 800.0 
  # E3 = 550.0 # in class worksheet
  A = length * depth
  
  # diagram: https://imgur.com/a/5sbBsxt

  # cos(θ) = 1 here because the E is parallel with n hat

  print "What is the amount of charge enclosed by the box? Use the accurate value ε_0 = 8.85 × 10-12 C2/N·m2."
  Q = ((E3 * A) - (E1 * A)) * vacuumPermittivity
  print "{} C".format(pPrint(Q))
  # for the in-class worksheet: two sig figs, in nanoCoulombs
  # -0.029 nC
  print "{} nC".format(pPrint(Q / 1.0e-9))

  # diagram: https://imgur.com/a/P9Zjo2g

  print "(b) The electric field is horizontal and has the values indicated on the surface of a cylinder shown in the figure. E1 = 1700 N/C, E2 = 1400 N/C, and E3 = 1100 N/C."
  length = 15.0e-2 
  diameter = 5.0e-2 
  radius = diameter / 2.0
  E1 = 1700.0
  E1 = 1800.0 # in class worksheet
  # E2 = 1400.0
  E3 = 1100.0
  E3 = 800.0 # in class worksheet
  A = math.pi * radius**2
  Q = ((E3 * A) - (E1 * A)) * vacuumPermittivity

  print "What is the amount of charge enclosed by the cylinder? Use the accurate value ε_0 = 8.85 × 10-12 C2/N·m2."
  print "{} C".format(pPrint(Q))
  # for the in-class worksheet: two sig figs, in nanoCoulombs
  # -0.017 nC
  print "{} nC".format(pPrint(Q / 1.0e-9))

  print "(c) The electric field has been measured to be horizontal and to the right everywhere on the closed box shown in the figure. All over the left side of the box E1 = 120 V/m, and all over the right, slanting, side of the box E2 = 450 V/m. On the top the average field is E3 = 260 V/m, on the front and back the average field is E4 = 150 V/m, and on the bottom the average field is E5 = 245 V/m."
  E1 = 120.0 # V/m
  # E1 = 110.0 # in class worksheet 
  E2 = 450.0 
  # E2 = 350.0 # in class worksheet
  # E3 = 260.0
  # E4 = 150.0
  # E5 = 245.0
  A1 = 4.0e-2 * 5.0e-2 
  A2 = 12.0e-2 * 5.0e-2 
  # based on angle of E2 to nhat which is perpendicular to A2
  theta = 90.0 - math.degrees(math.asin(4.0e-2 / 12.0e-2)) # in degrees

  Q = ((E2 * math.cos(math.radians(theta)) * A2) - (E1 * A1)) * vacuumPermittivity
  
  # diagram: https://imgur.com/a/3IH9X71
  
  print "What is the amount of charge enclosed by the cylinder? Use the accurate value ε_0 = 8.85 × 10-12 C2/N·m2."
  print "{} C".format(pPrint(Q))
  # for the in-class worksheet: two sig figs, in nanoCoulombs
  # 4.2 pC
  print "{} pC".format(pPrint(Q / 1.0e-12))


def Problem21_14():
  # diagram: https://imgur.com/a/q9SSVOf
  print "The electric field is measured all over a cubical surface, and the pattern of field detected is shown in the figure above. On the right side of the cube, the electric field has magnitude E1 = 344 V/m, and the angle between the electric field and the surface of the cube is θ = 12 degrees. On the bottom of the cube, the electric field has the same magnitude E1, and the angle between the electric field and the surface of the cube is also θ = 12 degrees. On the top of the cube and the left side of the cube, the electric field is zero. On half of the front and back faces, the electric field has magnitude E1 and is parallel to the face; on the other half of the front and back faces, the electric field is zero. One edge of the cube is 47 cm long."
  E1 = 344.0 # volts per meter
  theta = 90.0 - 12.0 # degrees
  A = 47.0e-2**2

  # for the in-class worksheet:
  E1 = 419.0
  theta = 90.0 - 18.0
  A = 42.0e-2**2
  
  
  
  flux = 2.0 * (E1 * math.cos(math.radians(theta)) * A)

  print "What is the net electric flux on this cubical surface?"

  print "Net electric flux = {} V·m".format(pPrint(flux))


  print "What is the net charge inside this cubical surface?"
  Q = flux * vacuumPermittivity
  print "Q = {} C".format(pPrint(Q))
  # for the in-class worksheet: two sig figs, in nanoCoulombs
  #  nC
  print "{} nC".format(pPrint(Q / 1.0e-9))



def Checkpoint21_02():
  print "Along the path shown in the figure the magnetic field is measured and is found to be uniform in magnitude and always tangent to the circular path."
  # diagram: https://imgur.com/a/X40SLFu
  print "If the radius of the path is 0.03 m and along the path is 1.6 × 10-6 T, use Ampere's law to calculate the magnitude of the conventional current enclosed by the path." 
  # B_wire = μ_0 / 4π * 2I / r
  # I = B_wire * r / (2 * μ_0 / 4π)
  r = 3.0e-2 # radius in meters
  B = 1.6e-6
  I = B * r / (2.0e-7)
  print "I = {} A.".format(pPrint(I))
  print "What is the direction of the current?"
  print "into the page"


def Problem21_22():
  print "The magnetic field has been measured to be horizontal everywhere along a rectangular path 19 cm long and 3 cm high, shown in the figure. Along the bottom the average magnetic field 1.6 × 10-4 tesla, along the sides the average magnetic field 0.9 × 10-4 tesla, and along the top the average magnetic field 0.8 ×10-4 tesla. What can you conclude about the electric currents in the area that is surrounded by the rectangular path?"
  x = 19.0e-2
  y = 3.0e-2 # can be ignored
  B1 = 1.6e-4 # bottom magnetic field
  B2 = 0.9e-4
  B3 = 0.8e-4
  B = B1 - B3
  I = (B * x) / (4.0 * math.pi * 1.0e-7)
  
  print "I = {} A.".format(pPrint(I))


def Problem21_23():
  print "The figure below shows a measured pattern of magnetic field in space."
  # the figure: https://imgur.com/a/unLmCye
  print "How much current I passes through the shaded area? (Assume B_1 = 7 × 10−5 T and B_2 = 9.5 × 10−4 T.)"
  B1 = 7.0e-5
  B2 = 9.5e-4
  # B1 = 5.0e-5 # from in-class worksheet
  # B2 = 6.4e-4 # from in-class worksheet
  theta = 35.0 # degrees
  x = 0.6 # width of box in meters
  y = 0.2 # height of the box

  I = (2.0 * x * (B2 * math.cos(math.radians(theta)))) / (4.0 * math.pi * 1.0e-7)
  print "{} A".format(pPrint(I))
  

  print "In what direction?"

  print "into the page"




# Extra21_2_01()
# Extra21_2_02()
# Problem21_06()
# Problem21_07()
# Problem21_10()
# Problem21_14()

# Checkpoint21_02()
# Problem21_22()
Problem21_23()








