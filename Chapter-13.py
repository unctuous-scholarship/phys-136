#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys
import math
import re # regular expression
from decimal import *
from lib import *

# Chapter 13 of Matter & Interactions by Ruth W. Chabay and Bruce A. Sherwood

def Problem27():
  print "You want to calculate the electric field at location <0.5, -0.1, -0.5> m, due to a particle with charge +6.0 nC located at <-0.6, -0.7, -0.2> m."
  q = 6.0e-9 # in Coulombs, note nC = nano Coulombs, which is 1 x 10^-9 Coulombs

  print "(a) What is the source location? (Express your answer in vector form.)"

  rsource = (-0.6, -0.7, -0.2)

  print "r source is {}".format(pPrint(rsource))
  
  print "(b) What is the observation location? (Express your answer in vector form.)"

  robs = (0.5, -0.1, -0.5)

  print "r obs is {}".format(pPrint(robs))

  print "(c) What is the vector that points from the source location to the observation location? (Express your answer in vector form.)"

  r = subtractVectors(robs, rsource)

  print "r is {}".format(pPrint(r))

  print "(d) What is |r|?"

  # magnitude of a vector is:
  # the square root of the sum of squares of the components of the vector  
  rmag = magnitudeOfVector(r)

  print "|r| is {}".format(pPrint(rmag)) 

  
  print "(e) What is the vector r̂ (Express your answer in vector form.)"

  # the unit vector from a position vector is the position vector divided by its magnitude
  rhat = divideVectorByScalar(r, rmag)
  
  print "r̂ is {}".format(pPrint(rhat))

  
  print "(f) What is the value of 1 / 4πε0 * q / |r|^2 ?"

  # 1 / 4πε0 is Coulomb's constant:
  # the universal constant for electrical charges
  # 1 / 4πε0 = 9 x 10^9 (N * m^2) / C^2

  Fmag =  ke * (q / rmag**2) 

  print "{} N/C".format(pPrint(Fmag))

  
  print "(g) Finally, what is the electric field, expressed as a vector? (Express your answer in vector form.)"

  E = multiplyVectorByScalar(rhat, Fmag) # in Newtons per Coulomb

  print "E = {}".format(pPrint(E))




def Problem23():

  print "An electron is observed to accelerate in the direction with an acceleration of 3.9 × 10^16 m/s2. Explain how to use the definition of electric field to determine the electric field at this location."
  # acceleration
  a = 3.9e16 # m/s^2
  print "Use Newton's second law to calculate the force on the electron, and use F = qE to calculate the electric field at the location of the electron."

  print "Give the direction and magnitude of the field."

  # assuming electron has mass 9.11e-31 kg
  m = 9.11e-31 # kg
  # Newton's second law: Force = mass * acceleration
  F = m * a

  # Electric Field: F = q E, E = F/q
  E = F / chargeOfElectron

  print "magnitude {} N/C".format(pPrint(abs(E)))

  print "direction: -z"

def Problem21():

  print "Now a particle whose charge is -6 × 10^-9 C is placed at location D."
  q = -6.0e-9 # Coulumbs

  print "The electric field at location D has the value < -7000, 7000, 0 > N/C. What is the unit vector in the direction of E at this location?"
  E = (-7000, 7000, 0)

  # unit vector of E is simply E divided by the magnitude of E
  # where magnitude is the square root of the sum of squares of the components of E
  Ehat = unitVector(E) 
  
  print "Ê = {}".format(pPrint(Ehat))

  print "What is the electric force on the -6 x 10^-9 C charge?"

  # F = q E 

  F = multiplyVectorByScalar(E, q)  
  
  print "F = {}".format(pPrint(F))

  
  print "What is the unit vector in the direction of this electric force?"

  Fhat = unitVector(F)
  print "F hat = {}".format(pPrint(Fhat))

def Problem44():
  print "The electric field at a location C points west, and the magnitude is 7.3 × 10^6 N/C. Give numerical answers to the following questions. (Let the axis point towards the east, the axis point towards the north and the axis point up from the ground. Express your answers in vector form.)"
  Emag = 7.3e6 # Newtons / Coulomb

  # "points west" = Ehat of <-1, 0, 0>
  E = multiplyVectorByScalar((-1, 0, 0), Emag)

  # Note: Electric Field of a Point Charge is
  # E1 = (1 / 4πε0) * (q1 / |r|^2) * r̂
  # where (1 / 4πε0) is Coulomb's constant ~ 9.0e9 (referred to with var `ke`)
  # where q1 is the source charge
  # where r is the relative position vector from the observation location to the source charge
  # where E1 is the electric field at the observation location 
   

  print "(a) Where relative to C should you place a single proton to produce this field?"
  
  # the equation |E1| = (1 / 4πε0) * (q1 / |r|^2) 
  # can be reformulated to calculate |r|:
  # |r| = sqrt((1 / 4πε0) * (q / |E|))

  # magnitude of position of proton
  rmagp = math.sqrt(ke * chargeOfProton / Emag)
  rp = multiplyVectorByScalar((1, 0, 0), rmagp) 
  print "rp = {}m".format(pPrint(rp))

  
  print "(b) Where relative to C should you place a single electron to produce this field?"

  
  # magnitude of position of electron
  rmage = math.sqrt(ke * abs(chargeOfElectron) / Emag)
  re = multiplyVectorByScalar((-1, 0, 0), rmage) 

  # should be the additive inverse of the position of the proton
  print "re = {}m".format(pPrint(re))

  
  print "(c) Where should you place a proton and an electron, at equal distances from C, to produce this field?"
  
  # E = 2 * (1 / 4πε0) * (q / |r|^2) 
  # |r|^2 = 2 * (1 / 4πε0) * (q / E)
  # |r| = sqrt(2 * (1 / 4πε0) * (q / E))
  rmag = math.sqrt(2.0 * ke * chargeOfProton / Emag)

  rp = multiplyVectorByScalar((1, 0, 0), rmag) 

  re = multiplyVectorByScalar((-1, 0, 0), rmag) 
   
  print "rp = {}m".format(pPrint(rp))

  print "re = {}m".format(pPrint(re))

def Problem45():
  print "A lithium nucleus consisting of three protons and four neutrons accelerates to the right due to electric forces, and the initial magnitude of the acceleration is 5.0 × 10^13 m/s/s. (Assume the initial position of the lithium nucleus to be at the origin. Assume that the axis is to the right.)"
  q = 3.0 * chargeOfProton
  a = 5.0e13 #m/s/s
  massOfProton = 1.7e-27 # kg
  massOfNeutron = massOfProton
  m = 3.0 * massOfProton + 4.0 * massOfNeutron

  F = m * a

  print "(a) What is the direction of the electric field that acts on the lithium nucleus?"
  print "to the right"

  print "(b) What is the magnitude of the electric field that acts on the lithium nucleus? Be quantitative (that is, give a number)."

  # F = q E
  # E = F / q
  E = F / q
    
  print "{} N/C".format(pPrint(E))

  print "(c) If this acceleration is due solely to a single helium nucleus (two protons and two neutrons), where is the helium nucleus initially located? Be quantitative (that is, give a number). (Indicate the direction with the sign of your answer.)"
  q = 2.0 * chargeOfProton
  # E = (1 / 4πε0) * (q / |r|^2)  
  # |r| = sqrt((1 / 4πε0) * (q / E))
  rmag = math.sqrt(ke * q / E)
  
  r = multiplyVectorByScalar((-1.0, 0.0, 0.0), rmag)
  print "{} m".format(pPrint(r))

def Problem62():
  print "A water molecule is asymmetrical, with one end positively charged and the other negatively charged. It has a dipole moment whose magnitude is measured to be 6.2 × 10^−30 C·m. If the dipole moment is oriented perpendicular to an electric field whose magnitude is 9 × 10^5 N/C, what is the magnitude of the torque on the water molecule?"
  # electric dipole moment p = q * s
  # where q is charge in Coulombs
  # and s is distance between poles
  p = 6.2e-30 
  Emag = 9.0e5 # Newtons / Coulomb 
  torque = p * Emag

  print "{} N * m".format(pPrint(torque))


def Problem46a():
  print "Two small charged objects are located as shown in the diagram. In the diagram each square represents 10^-2 m. Q1 = 4 nC and Q2 = -5 nC."
  q1 = 4.0e-9 # Coulombs
  q2 = -5.0e-9 # Coulombs
  r1 = (-2.0e-2, 5.0e-2, 0) # meters
  r2 = (3.0e-2, -3.0e-2, 0) # meters
  
  print "Remember that 1 nC (nanocoulomb) is 10^-9 coulombs. You will calculate the net electric field at location A = <-0.07, -0.04, 0> m."
  robs = (-0.07, -0.04, 0) # meters

  print "Before doing any calculations, on a copy of the diagram, draw two arrows representing the electric field at the observation location due to each of the charged particles. Use your diagram to answer the following questions."
  
  print "What is the electric field at A due to Q1?"

  r = subtractVectors(robs, r1)  

  E1 = multiplyVectorByScalar(unitVector(r), ke * q1 / magnitudeOfVector(r)**2)
  
  print "E1 = {}".format(pPrint(E1))

 
  print "What is the electric field at A due to Q2?"
  
  r = subtractVectors(robs, r2)

  E2 = multiplyVectorByScalar(unitVector(r), ke * q2 / magnitudeOfVector(r)**2)

  print "E2 = {} N/C".format(pPrint(E2))


  print "What is the net electric field at A?"

  Enet = addVectors(E1, E2)

  print "Enet = {} N/C".format(pPrint(Enet))




def Problem51():
  print "Three nested hollow spheres have the same center. The innermost sphere has a radius of 4 cm and carries a uniformly distributed charge of 6 nC (1 nC = 1 × 10^−9 C). The middle sphere has a radius of 8 cm and carries a uniformly distributed charge of −4 nC. The outermost sphere has a radius of 10 cm and carries a uniformly distributed charge of 8 nC."
  R1 = 4.0e-2 # radius in meters, given as 4cm
  Q1 = 6.0e-9 # charge in Coulombs, given as 6nC
  R2 = 8.0e-2 
  Q2 = -4.0e-9
  R3 = 10.0e-2
  Q3 = 8.0e-9

  # Example from book:
  # R1 = 2.0e-2 # radius in meters, given as 4cm
  # R2 = 5.0e-2 

  print "(a) What is the magnitude of the electric field at a distance of 3 cm from the center?"

  E1 = 0 # because it's inside the innermost sphere with uniformly distributed charge - at any point inside the sphere, the charges balance one another out - see Gauss's law

  print "{} N/C".format(pPrint(E1))


  print "(b) What is the magnitude of the electric field at a distance of 6 cm from the center?"
  distance2 = 6.0e-2 # meters

  # Example from book:
  # distance2 = 4.0e-2 # meters

  # use Q1 because it's between R1 and R2
  E2 = ke * Q1 / distance2**2

  print "{} N/C".format(pPrint(E2))

  
  print "(c) What is the magnitude of the electric field at a distance of 9 cm from the center?"

  distance3 = 9.0e-2 # meters

  # use Q2 because it's between R2 and R3
  # E3 = (ke * Q2 / distance3**2) + (ke * Q1 / R2**2)
  # E3 = ke * Q2 / (distance3)**2 + (ke * Q1 / (R2)**2)
  E3 = ke * (Q1 + Q2) / (distance3)**2 

  print "{} N/C".format(pPrint(E3))
  # answer from book: 2.22 x 10^3 N/C
  # print "should be 2.22 x 10^3 N/C"





def Problem47():
  print "At a particular moment, three charged particles are located as shown in the figure below. Q1 = -7 μC, Q2 = 8 μC, and Q3 = -9 μC. Your answers to the following questions should be vectors. (Recall that 1 μC = 1 × 10−6 C. Assume that the +x axis is to the right, the +y axis is up along the page and the +z axis points out of the page. Express your answers in vector form.)"
  # Charges:
  q1 = -7.0e-6 # Coulombs
  q2 = 8.0e-6
  q3 = -9.0e-6

  # Positions of objects:
  r1 = (0.0, 3.0e-2, 0.0) # meters
  r2 = (4.0e-2, 3.0e-2, 0.0) 
  r3 = (4.0e-2, 0.0, 0.0)
  A = (0.0, 0.0, 0.0)


  print "(a) Find the electric field at the location of Q3, due to Q1."
  
  r = subtractVectors(r3, r1)
  E1 = multiplyVectorByScalar(unitVector(r), ke * q1 / magnitudeOfVector(r)**2)

  print "E1 = {} N/C".format(pPrint(E1))

  print "(b) Find the electric field at the location of Q3, due to Q2."

  r = subtractVectors(r3, r2)
  E2 = multiplyVectorByScalar(unitVector(r), ke * q2 / magnitudeOfVector(r)**2)

  print "E2 = {} N/C".format(pPrint(E2))

  print "(c) Find the net electric field at the location of Q3."

  Enet = addVectors(E1, E2)
  print "Enet = {} N/C".format(pPrint(Enet))


  print "(d) Find the net force on Q3."
  
  # F net, n = Qn * Enet
  Fnet3 = multiplyVectorByScalar(Enet, q3)
  print "Fnet,3 = {} N".format(pPrint(Fnet3))

  print "(e) Find the electric field at location A due to Q1."
  r = subtractVectors(A, r1)
  E1 = multiplyVectorByScalar(unitVector(r), ke * q1 / magnitudeOfVector(r)**2)
  print "E1 = {} N/C".format(pPrint(E1))
  
  print "(f) Find the electric field at location A due to Q2."
  r = subtractVectors(A, r2)
  E2 = multiplyVectorByScalar(unitVector(r), ke * q2 / magnitudeOfVector(r)**2)
  print "E2 = {} N/C".format(pPrint(E2))

  print "(g) Find the electric field at location A due to Q3."
  r = subtractVectors(A, r3)
  E3 = multiplyVectorByScalar(unitVector(r), ke * q3 / magnitudeOfVector(r)**2)
  print "E3 = {} N/C".format(pPrint(E3))

  print "(h) What is the net electric field at location A?"
  EnetA = addVectors(addVectors(E1, E2), E3)
  print "EA = {} N/C".format(pPrint(EnetA))

  print "(i) If a particle with charge -6.5 nC were placed at location A, what would be the force on this particle?"
  FonA = multiplyVectorByScalar(EnetA, -6.5e-9)

  print "F on A = {} N".format(pPrint(FonA))




def Problem57():
  print "A dipole consists of two charges +q and -q, held apart by a rod of length s, as shown in the diagram. If q = 7 nC and s = 3 mm, what is the magnitude of the electric field due to the dipole at location A, a distance d = 5 cm from the dipole?"
  s = 3.0e-3 # meters
  q = 7.0e-9 # Coulombs
  d = 5.0e-2 # meters
  
  
  # |E axis| = Coulomb's Constant * (2 * q * s) / r^3
  E = ke * 2.0 * q * s / d**3
  print "E = {} N/C".format(pPrint(E))


  print "What is the magnitude of the electric field due to the dipole at location B, a distance d = 5 cm from the dipole?"

  # |E ⊥ | = Coulomb's Constant * (q * s) / r^3
  E = ke * q * s / d**3
  print "E = {} N/C".format(pPrint(E))





def Problem61():
  print "A charge of +1 nC (1 × 10−9 C) and a dipole with charges +q and -q separated by 0.3 mm contribute a net field at location A that is zero, as shown in the figure below. (Assume r1 = 28 cm and r2 = 15 cm.)"
  s = 0.3e-3 # meters, the distance between the dipole charges
  r1 = 28.0e-2 # meters, the horizontal distance from A (the origin) to the charge with +1 nC
  r2 = 15.0e-2 # meters, the vertical distance between A (the origin) and the dipole
  
  q1 = 1.0e-9 # Coulombs

  # Book variables:
  # r1 = 20.0e-2 # 20 cm
  # r2 = 10.0e-2 # 10 cm


  # E net is 0 at A, so E net = 0 = E dipole + E proton
  # so |E dipole| = |E proton|
  # E dipole = Coulomb's Constant * (q * s) / r^3
  # E proton = Coulomb's Constant * (q1 / |r|^2) * rhat
  Eproton = ke * (q1 / (r1**2))
  print "E proton = {} N/C".format(pPrint(Eproton))

  q = (r2**3 * Eproton) / (ke * s)
  # Coulomb's Constant * (q1 / |r1|^2) = Coulomb's Constant * (q * s) / r2^3
  # q1 / |r1|^2 = q * s / |r2|^3
  # q = |r2|^3 * q1 / |r1|^2 * s
  # q = (r2**3 * q1) / (r1**2 * s)
  print "q = {} nC".format(pPrint(q / 10.0e-10)) # NOT SURE WHY, but divide by 10e-10 not 10e-9 ... yikes
  # print "book answer: 8.3e-8 C" # note that the book answer is in Coulombs, but the WileyPLUS is in nC ...
  

def Problem64():
  print "You make repeated measurements of the electric field E due to a distant charge, and you find it is constant in magnitude and direction. At time t = 0 your partner moves the charge. The electric field doesn't change for a while, but at time t = 35 ns you observe a sudden change. How far away was the charge originally?"
  t1 = 0.0
  t2 = 35.0 # nanoseconds
  # speed of light: 30 centimeters per nanosecond
  distance = 30.0e-2 * t2

  print "{} m".format(pPrint(distance))
  

Problem27()
Problem23()
Problem21()
Problem44()
Problem45()

Problem62()

Problem46a()
Problem51()
Problem47()
Problem57()
Problem61()
Problem64()
