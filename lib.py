#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys
import math
import re # regular expression
from decimal import *

# Log Levels:
# CRITICAL
# ERROR
# WARNING
# INFO
# DEBUG
# NOTSET

logging.basicConfig(filename = 'PHYS136.log', level=logging.DEBUG)
logger = logging.getLogger(__name__)


# Speed of Light
c = 299792458; # m/s

# Gravity on Earth
gE = 9.8; # N / kg

# universal constant for gravitation
G = 6.7e-11; # in (N * m^2) / kg^2

# the universal constant for electrical charges
# 1 / 4πε0 = 9 x 10^9 (N * m^2) / C^2
# Coulomb's constant
ke = 9.0e9; 

# ε_0 in Coulomb's constant, in C^2 / N * m^2
vacuumPermittivity = 8.85e-12 

# μ_0 from (μ_0 / 4π) is vacuum permeability
vacuumPermeability = 4.0 * math.pi * 1.0e-7


# charge of a proton
# e = +1.6 x 10^-19 C
chargeOfProton = 1.6e-19; # in C

# mass of a proton, in kilograms (same as mass of neutron)
massOfProton = 1.7e-27


# charge of electron
chargeOfElectron = -1.6e-19; # in C

# mass of an electron, in kilograms
massOfElectron = 9e-31 



# Avogadro's Constant
Na = 6.022140857e23 

# 1 Electron Volt
eV = 1.6e-19 # J

# 1 Million Electron Volts
MeV = 1.0e6 * eV

# Planck's Constant
h = 6.6e-34 # J·s

# Boltzmann Constant
# The Boltzmann constant (kB or k) is a physical constant 
# relating the average kinetic energy of particles in a gas with the temperature of the gas
kB = 1.3806503e-23 # Joules / Kelvin


# Pretty Print (pPrint) is a function to assist the easy printing of tuples as vectors
# and of consistently formatting floats to a given significant figure, 
# and of consistently formatting large numbers in scientific notation
def pPrint(value):
  def decideFormat(value):
    standardFormattedValue = "{:.4e}".format(value);

    if (value == 0):
      return "0";
    else:
      factorTenPattern = re.compile('\d{2}$');
      factorTen = factorTenPattern.findall(standardFormattedValue)[0];
      
      # print "🌱 factorTen: {} of value: {}".format(factorTen, value);
      if (factorTen != None and abs(int(factorTen)) < 3):
        return trimZeroes("{:.4f}".format(value));
      else: 
        return trimZeroes(standardFormattedValue);

  def trimZeroes(value):
    if (re.search(r"[e][+,-]\d+$", value) is None): 
      value = re.sub(r"^([-]?\d+)[\.]0{4}$", r"\1", value);
      # normal decimal, e.g. 1.700 should become 1.7
      value = re.sub(r"^([-]?\d+)([\.][1-9]*?)0*$", r"\1\2", value);
    else: 
      value = re.sub(r"^([-]?\d+)[\.]0{4}([e][+,-])0*([1-9]*)$", r"\1\2\3", value);
      # scientific notation, e.g. 1.7000e+03 should become 1.7e+3
      value = re.sub(r"^([-]?\d+)([\.][1-9]*?)0*([e][+,-])0*([1-9]*)$", r"\1\2\3\4", value);
    return value; # remove extra zeroes, often at the end of decimals
    

  if (type(value) is tuple): 
    s = "<";
    for index, component in enumerate(value):

      if (index > 0):
        s += ", ";
      
      s += decideFormat(component);
      

    s += ">";

    return s;
  elif (type(value) is float):
    return decideFormat(value);
  else: 
    if (value > 1000000.0 or value < -1000000.0):
      return decideFormat(value);
    elif (value == 0):
      return "0";
    else: 
      return value;






def multiplyVectorByScalar(vector, scalar):
  result = [];

  component = "";

  for i in range(0, len(vector)):
    if (i == 0):
      component = "x";    
    elif (i == 1):
      component = "y";
    elif (i == 2):
      component = "z";
    else: 
      component = "";
    product = vector[i] * scalar;
    logger.debug("({}: {} =  {} * {})".format(component, pPrint(product), pPrint(vector[i]), pPrint(scalar)));
    result.append(product);
    
  result = tuple(result);

  logger.info("{} = {} * {}".format(pPrint(result), pPrint(vector), pPrint(scalar)));
  
  return result;

def multiplyVectors(vector1, vector2):
  result = [];
  component = "";
  if (len(vector1) != len(vector2)):
    sys.exit("❌ ERROR: The first vector of {} has {} elements, but the second vector of {} has {} elements, so we can't multiply the two vectors together.".format(vector1, len(vector1), vector2, len(vector2)));

  for i in range(0, len(vector1)):
    if (i == 0):
      component = "x";    
    elif (i == 1):
      component = "y";
    elif (i == 2):
      component = "z";
    else: 
      component = "";
    product = vector1[i] * vector2[i];
    logger.debug("({}: {} =  {} * {})".format(component, pPrint(product), pPrint(vector1[i]), pPrint(vector2[i])));
    result.append(product);
    
  result = tuple(result);

  logger.info("{} = {} * {}".format(pPrint(result), pPrint(vector1), pPrint(vector2)));
  
  return result;



def dotProduct(vector1, vector2):

  if (len(vector1) != len(vector2)):
    sys.exit("❌ ERROR: The first vector of {} has {} elements, but the second vector of {} has {} elements, so we can't get the dot product of the two vectors.".format(vector1, len(vector1), vector2, len(vector2)));

  component = "";
  sum = 0;

  for i in range(0, len(vector1)):
    if (i == 0):
      component = "x";    
    elif (i == 1):
      component = "y";
    elif (i == 2):
      component = "z";
    else: 
      component = "";
    sum += vector1[i] * vector2[i];
    logger.debug("({}: {} += {} * {})".format(component, pPrint(sum), pPrint(vector1[i]), pPrint(vector2[i])));
    
  logger.info("{} = {} · {}".format(pPrint(sum), pPrint(vector1), pPrint(vector2)));

  return sum;


def crossProduct(vector1, vector2):

  if (len(vector1) != len(vector2)):
    sys.exit("❌ ERROR: The first vector of {} has {} elements, but the second vector of {} has {} elements, so we can't get the cross product of the two vectors.".format(vector1, len(vector1), vector2, len(vector2)));

  if (len(vector1) != 3):
    sys.exit("Error: crossProduct() was only written to operate on vectors with three components.")

  product = [];
  
  product.append((vector1[1] * vector2[2]) - (vector1[2] * vector2[1]))
  product.append((vector1[2] * vector2[0]) - (vector1[0] * vector2[2]))
  product.append((vector1[0] * vector2[1]) - (vector1[1] * vector2[0]))
  
  return tuple(product);
  
  


def divideVectorByScalar(vector, scalar):
  result = [];
  
  component = "";
  
  if (scalar == 0):
    sys.exit("❌ ERROR: Cannot divide by 0!");
  
  for i in range(0, len(vector)):
    if (i == 0):
      component = "x";    
    elif (i == 1):
      component = "y";
    elif (i == 2):
      component = "z";
    else: 
      component = "";
    quotient = vector[i] / scalar;
    logger.debug("({}: {} =  {} / {})".format(component, pPrint(quotient), pPrint(vector[i]), pPrint(scalar)));
    result.append(quotient);
    
  result = tuple(result);

  logger.info("{} = {} / {}".format(pPrint(result), pPrint(vector), pPrint(scalar)));
  
  return result;



def divideVectors(vector1, vector2):
  result = [];
  component = "";
  if (len(vector1) != len(vector2)):
    sys.exit("❌ ERROR: The first vector of {} has {} elements, but the second vector of {} has {} elements, so we can't divide the one vector by the other.".format(vector1, len(vector1), vector2, len(vector2)));

  for i in range(0, len(vector1)):
    if (i == 0):
      component = "x";    
    elif (i == 1):
      component = "y";
    elif (i == 2):
      component = "z";
    else: 
      component = "";
    if (vector2[i] == 0):
      if (vector1[i] == 0):
        division = 0;
      else:
        sys.exit("❌ ERROR: Cannot divide {} by 0!".format(vector1[i]));
    else: 
      division = vector1[i] / vector2[i];

    logger.debug("({}: {} =  {} / {})".format(component, pPrint(division), pPrint(vector1[i]), pPrint(vector2[i])));
    result.append(division);
    
  result = tuple(result);

  logger.info("{} = {} / {}".format(pPrint(result), pPrint(vector1), pPrint(vector2)));
  
  return result;

def addVectors(vector1, vector2):
  result = [];

  component = "";

  if (len(vector1) != len(vector2)):
    sys.exit("❌ ERROR: The first vector of {} has {} elements, but the second vector of {} has {} elements, so we can't add the two vectors together.".format(vector1, len(vector1), vector2, len(vector2)));

  for i in range(0, len(vector1)):
    if (i == 0):
      component = "x";    
    elif (i == 1):
      component = "y";
    elif (i == 2):
      component = "z";
    else: 
      component = "";
    sum = vector1[i] + vector2[i];
    logger.debug("({}: {} =  {} + {})".format(component, pPrint(sum), pPrint(vector1[i]), pPrint(vector2[i])));
    result.append(sum);
    
  result = tuple(result);

  logger.info("{} = {} + {}".format(pPrint(result), pPrint(vector1), pPrint(vector2)));
  
  return result;

def addVectorByScalar(vector, scalar):
  result = [];

  component = "";

  for i in range(0, len(vector)):
    if (i == 0):
      component = "x";    
    elif (i == 1):
      component = "y";
    elif (i == 2):
      component = "z";
    else: 
      component = "";
    sum = vector[i] + scalar;
    logger.debug("({}: {} =  {} + {})".format(component, pPrint(sum), pPrint(vector[i]), pPrint(scalar)));
    result.append(sum);
    
  result = tuple(result);

  logger.info("{} = {} + {}".format(pPrint(result), pPrint(vector), pPrint(scalar)));
  
  return result;
  

def subtractVectors(vector1, vector2):
  result = [];

  component = "";

  if (len(vector1) != len(vector2)):
    sys.exit("❌ ERROR: The first vector of {} has {} elements, but the second vector of {} has {} elements, so we can't add the two vectors together.".format(vector1, len(vector1), vector2, len(vector2)));

  for i in range(0, len(vector1)):
    if (i == 0):
      component = "x";    
    elif (i == 1):
      component = "y";
    elif (i == 2):
      component = "z";
    else: 
      component = "";
    difference = vector1[i] - vector2[i];
    logger.debug("({}: {} =  {} - {})".format(component, pPrint(vector1[i] - vector2[i]), pPrint(vector1[i]), pPrint(vector2[i])));
    result.append(difference);
    
  result = tuple(result);

  logger.info("{} = {} - {}".format(pPrint(result), pPrint(vector1), pPrint(vector2)));
  
  return result;

#  _____ _             _   ____           _ _   _             
# |  ___(_)_ __   __ _| | |  _ \ ___  ___(_) |_(_) ___  _ __  
# | |_  | | '_ \ / _` | | | |_) / _ \/ __| | __| |/ _ \| '_ \ 
# |  _| | | | | | (_| | | |  __/ (_) \__ \ | |_| | (_) | | | |
# |_|   |_|_| |_|\__,_|_| |_|   \___/|___/_|\__|_|\___/|_| |_|
# Position Update Equation: final position vector equals the initial position vector plus the average velocity vector times change in time
# initialPosition: the initial position vector in meters, e.g. (1, 4, 5) for x = 1, y = 4, z = 5 
# averageVelocity: the average velocity vector in meters per second, e.g. (5, 3, 1) for x = 5, y = 3, z = 1 
# changeInTime: the delta of time, t2 - t1, in seconds, e.g. 5 for five seconds passed since t1
def finalPosition(initialPosition, averageVelocity, changeInTime):
  answer = [];

  if (len(initialPosition) != len(averageVelocity)):
    sys.exit("❌ ERROR: finalPosition: Initial Position of {} has {} elements, but Average Velocity of {} has {} elements, so we can't find the final position.".format(initialPosition, len(initialPosition), averageVelocity, len(averageVelocity)));
    
  component = "";

  for iterator in range(0, len(initialPosition)):
    
    if (iterator == 0):
      component = "x";    
    elif (iterator == 1):
      component = "y";
    elif (iterator == 2):
      component = "z";
    else: 
      component = "";

    logger.debug("{}: {} + {} * {}".format(component, pPrint(initialPosition[iterator]), pPrint(averageVelocity[iterator]), pPrint(changeInTime)));
    logger.debug("{}: {} + {}".format(component, pPrint(initialPosition[iterator]), pPrint(averageVelocity[iterator] * changeInTime)));
    logger.debug("{}: {}".format(component, pPrint(initialPosition[iterator] + averageVelocity[iterator] * changeInTime)));
    answer.append(initialPosition[iterator] + (averageVelocity[iterator] * changeInTime));
  
  answer = tuple(answer);

  logger.info("➡️  final position: {} = initial position: {} + avg. velocity: {} * Δt: {}.".format(pPrint(answer), pPrint(initialPosition), pPrint(averageVelocity), pPrint(changeInTime)));

  return answer;

# γ = 1/sqrt(1 - (v/c)^2)
def proportionalityFactor(magnitudeOfVelocity):
  return 1 / (math.sqrt(1 - (magnitudeOfVelocity / c)**2))


# __     __   _            _ _            __                     
# \ \   / /__| | ___   ___(_) |_ _   _   / _|_ __ ___  _ __ ___  
#  \ \ / / _ \ |/ _ \ / __| | __| | | | | |_| '__/ _ \| '_ ` _ \ 
#   \ V /  __/ | (_) | (__| | |_| |_| | |  _| | | (_) | | | | | |
#    \_/ \___|_|\___/ \___|_|\__|\__, | |_| |_|  \___/|_| |_| |_|
#                                |___/                           
#  __  __                            _                   
# |  \/  | ___  _ __ ___   ___ _ __ | |_ _   _ _ __ ___  
# | |\/| |/ _ \| '_ ` _ \ / _ \ '_ \| __| | | | '_ ` _ \ 
# | |  | | (_) | | | | | |  __/ | | | |_| |_| | | | | | |
# |_|  |_|\___/|_| |_| |_|\___|_| |_|\__|\__,_|_| |_| |_|
# Approximate Velocity from Momentum and Mass: velocity vector equals momentum vector divided by mass
# momentum: expected to be a three-component vector with x, y, & z in (kg * m / s), e.g. (1, 2, 3) for x = 1, y = 2, z = 3
# mass: in kilograms, e.g. 5 for 5kg
def velocityFromMomentum(momentum, mass):
  answer = [];

  magnitudeOfMomentum = magnitudeOfVector(momentum);
    
  denom = math.sqrt(1 + (magnitudeOfMomentum / (mass * c))**2.0);

  if (1 - denom < 0.01):
    logger.debug("(Speeds are low enough to approximate velocity.)");
    denom = 1;
  else: 
    logger.debug("(Speeds are high enough to require using the proportionality factor.)");

  for iterator in range(0, len(momentum)):
    answer.append((momentum[iterator] / mass) / denom);

  answer = tuple(answer);

  logger.info("➡️ velocity: {} = momentum: {} / mass: {}.".format(pPrint(answer), pPrint(momentum), pPrint(mass)));

  return answer;
    
#  __  __                   _ _             _               __ 
# |  \/  | __ _  __ _ _ __ (_) |_ _   _  __| | ___    ___  / _|
# | |\/| |/ _` |/ _` | '_ \| | __| | | |/ _` |/ _ \  / _ \| |_ 
# | |  | | (_| | (_| | | | | | |_| |_| | (_| |  __/ | (_) |  _|
# |_|  |_|\__,_|\__, |_| |_|_|\__|\__,_|\__,_|\___|  \___/|_|  
#               |___/                                          
# __     __        _             
# \ \   / /__  ___| |_ ___  _ __ 
#  \ \ / / _ \/ __| __/ _ \| '__|
#   \ V /  __/ (__| || (_) | |   
#    \_/ \___|\___|\__\___/|_|   
# Magnitude of a vector is the squared root of the sum of squared components,
# e.g. vector <1, 2, 3> would be the square root of 1^2 + 2^2 + 3^2
# which is the squared root of 1 + 4 + 9, which is the squared root of 14.
# vector: expecting a tuple with three elements, e.g. (1, 2, 3) where x = 1, y = 2, z = 3
def magnitudeOfVector(vector):
  sum = 0.0;
  toPrint = "Square Root of ";
  for index, component in enumerate(vector):
    if index > 0: 
      toPrint += " + "; 
    toPrint += "{}^2".format(component);
    sum += component**2.0;

  toPrint += " is {}".format(pPrint(math.sqrt(sum)));
  
  logger.info(toPrint);

  return math.sqrt(sum);

#  _   _       _ _    __     __        _             
# | | | |_ __ (_) |_  \ \   / /__  ___| |_ ___  _ __ 
# | | | | '_ \| | __|  \ \ / / _ \/ __| __/ _ \| '__|
# | |_| | | | | | |_    \ V /  __/ (__| || (_) | |   
#  \___/|_| |_|_|\__|    \_/ \___|\___|\__\___/|_|   
# Unit vector is the vector divided by the magnitude of that vector
# vector: expecting a tuple with three elements, e.g. (1, 2, 3) where x = 1, y = 2, z = 3
def unitVector(vector):
  magV = magnitudeOfVector(vector);
  unitVector = divideVectorByScalar(vector, magV);
  logger.info("➡️  unit vector: {} = vector: {} / magnitude of vector: {} ".format(pPrint(unitVector), pPrint(vector), pPrint(magV)));
  return unitVector;

  


#  __  __                            _                   
# |  \/  | ___  _ __ ___   ___ _ __ | |_ _   _ _ __ ___  
# | |\/| |/ _ \| '_ ` _ \ / _ \ '_ \| __| | | | '_ ` _ \ 
# | |  | | (_) | | | | | |  __/ | | | |_| |_| | | | | | |
# |_|  |_|\___/|_| |_| |_|\___|_| |_|\__|\__,_|_| |_| |_|
#                                                        
#   __                      __     __   _            _ _         
#  / _|_ __ ___  _ __ ___   \ \   / /__| | ___   ___(_) |_ _   _ 
# | |_| '__/ _ \| '_ ` _ \   \ \ / / _ \ |/ _ \ / __| | __| | | |
# |  _| | | (_) | | | | | |   \ V /  __/ | (_) | (__| | |_| |_| |
# |_| |_|  \___/|_| |_| |_|    \_/ \___|_|\___/ \___|_|\__|\__, |
# Definition of Momentum: proportionality factor times mass times velocity vector
# Proportionality Factor: 1 divided by square root of 1 minus the squared division of the magnitude of velocity by the speed of light
# velocity: a vector of velocity in meters per second, e.g. (1, 2, 3) for x = 1, y = 2, z = 3
# mass: in kilograms, e.g. 5 for 5kg
def momentumFromVelocity(velocity, mass):
  answer = [];
  
  magnitudeOfVelocity = magnitudeOfVector(velocity);

  denom = math.sqrt((1 - (magnitudeOfVelocity / c))**2.0);

  proportionalityFactor = 1 / denom;

  if (1.00000 - proportionalityFactor < 0.01):
    logger.debug("(Speeds are low enough to approximate velocity.)");
    proportionalityFactor = 1;
  else: 
    logger.debug("(Speeds are high enough to require using the proportionality factor.)");
    

  for iterator in range(0, len(velocity)):
    answer.append(proportionalityFactor * (velocity[iterator] * mass));

  answer = tuple(answer);

  logger.info("➡️ momentum: {} = velocity: {} * mass: {}.".format(pPrint(answer), pPrint(velocity), pPrint(mass)));

  return answer;

#  _____ _             _   __  __                            _                   
# |  ___(_)_ __   __ _| | |  \/  | ___  _ __ ___   ___ _ __ | |_ _   _ _ __ ___  
# | |_  | | '_ \ / _` | | | |\/| |/ _ \| '_ ` _ \ / _ \ '_ \| __| | | | '_ ` _ \ 
# |  _| | | | | | (_| | | | |  | | (_) | | | | | |  __/ | | | |_| |_| | | | | | |
# |_|   |_|_| |_|\__,_|_| |_|  |_|\___/|_| |_| |_|\___|_| |_|\__|\__,_|_| |_| |_|
                                                                               
# Update Form of the Momentum Principle: final momentum vector equals the initial position vector plus the average velocity vector times change in time
# initialMomentum: the initial momentum vector in meters per second, e.g. (1, 4, 5) for x = 1, y = 4, z =0 5 
# netForce: the net force vector in Newtons (kg * m / s / s), e.g. (5, 3, 1) for x = 5, y = 3, z = 1 
# changeInTime: the delta of time, t2 - t1, in seconds, e.g. 5 for five seconds passed since t1
def finalMomentum(initialMomentum, netForce, changeInTime): 
  answer = [];

  if (len(initialMomentum) != len(netForce)):
    sys.exit("❌ ERROR: finalMomentum: Initial Momentum of {} has {} elements, but Net Force of {} has {} elements, so we can't find the final momentum.".format(initialMomentum, len(initialMomentum), netForce, len(netForce)));
    
  component = "";

  for iterator in range(0, len(initialMomentum)):

    if (iterator == 0):
      component = "x";    
    elif (iterator == 1):
      component = "y";
    elif (iterator == 2):
      component = "z";
    else: 
      component = "";

    logger.debug("{}: {} + {} * {}".format(component, pPrint(initialMomentum[iterator]), pPrint(netForce[iterator]), pPrint(changeInTime)));
    logger.debug("{}: {} + {}".format(component, pPrint(initialMomentum[iterator]), pPrint(netForce[iterator] * changeInTime)));
    logger.debug("{}: {}".format(component, pPrint(initialMomentum[iterator] + netForce[iterator] * changeInTime)));
    answer.append(initialMomentum[iterator] + (netForce[iterator] * changeInTime));
  
  answer = tuple(answer);

  logger.info("➡️ final momentum: {} = initial momentum: {} + net force: {} * Δt: {}.".format(pPrint(answer), pPrint(initialMomentum), pPrint(netForce), pPrint(changeInTime)));

  return answer;


# Approximate Gravitational Force on Earth
# mass: in kg, e.g. 5 for 5kg
def approxEarthGravity(mass):
  fGrav = mass * gE; # m * g, where g = 9.8 N/kg, and m = mass in kg
  logger.info("Approx. Earth's Gravitational Force: {} =  mass: {} * g: {}.".format(pPrint(fGrav), pPrint(mass), pPrint(gE)));
  return fGrav;


# Iterative Prediction of Motion with a Constant Force:
# 1. calculate vector forces (to get net force vector) [not used since force is constant]
# 2. update momentum 
# 3. update position
# 4. repeat
# ASSUMES UNCHANGING NET FORCE
def iterativelyPredictMotion(mass, startTime, endTime, timeBetweenSteps, initialPosition, initialVelocity, initialMomentum, netForce):
  currentTime = startTime;

  currentPosition = initialPosition;
  currentVelocity = initialVelocity;
  currentMomentum = initialMomentum;

  while ( Decimal(currentTime) <= Decimal(endTime) or Decimal(currentTime) <= Decimal(endTime + 0.00000001) ):
    
    currentPosition = finalPosition(initialPosition, currentVelocity, (currentTime - startTime));

    currentMomentum = finalMomentum(initialMomentum, netForce, (currentTime - startTime)); 

    currentVelocity = velocityFromMomentum(currentMomentum, mass);

    logger.debug("at t of {}s: \nposition: {}, \nmomentum: {}, \nvelocity: {}".format(pPrint(currentTime), pPrint(currentPosition), pPrint(currentMomentum), pPrint(currentVelocity)));

    currentTime += timeBetweenSteps;
    
    # logger.debug("endTime: {}, currentTime: {}, Decimal(endTime) - Decimal(currentTime): {} ".format(endTime, currentTime, Decimal(endTime) - Decimal(currentTime)));
    
# Approximate Average Velocity from Change in Position and Time
# avg. velocity vector = delta position / delta time
# initialPosition: the initial position vector in meters, e.g. (1, 4, 5) for x = 1, y = 4, z = 5 
# finalPosition: the final position vector in meters, e.g. (1, 2, 3) for x = 1, y = 2, z = 3 
# initialTime: in seconds, e.g. 4 for 4 seconds. 
# finalTime: in seconds, e.g. 14 for 14 seconds. 
def averageVelocityFromPosAndTime(initialPosition, finalPosition, initialTime, finalTime):
  if (len(initialPosition) != len(finalPosition)):
    sys.exit("❌ ERROR: Initial Position of {} has {} elements, but Final Position of {} has {} elements, so we can't know which components are which to find change in position.".format(initialPosition, len(initialPosition), finalPosition, len(finalPosition)));

  deltaPosition = [];
  deltaTime = finalTime - initialTime;
  averageVelocity = [];

  component = "";

  for i in range(0, len(initialPosition)):

    if (i == 0): 
      component = "x";
    elif (i == 1): 
      component = "y";
    elif (i == 2): 
      component = "z";
    else: 
      component = "";

    logger.debug("{}: {} = Final Position: {} - Initial Position: {}".format(component, pPrint(finalPosition[i] - initialPosition[i]), pPrint(finalPosition[i]), pPrint(initialPosition[i])));
    deltaPosition.append(finalPosition[i] - initialPosition[i]);

  component = "";

  for i in range(0, len(initialPosition)):

    if (i == 0):
      component = "x";    
    elif (i == 1):
      component = "y";
    elif (i == 2):
      component = "z";
    else: 
      component = "";

    logger.debug("{}: {} / {} = {}".format(component, pPrint(deltaPosition[i]), pPrint(deltaTime), pPrint(deltaPosition[i] / deltaTime)));
    averageVelocity.append(deltaPosition[i] / deltaTime);

  deltaPosition = tuple(deltaPosition);
  averageVelocity = tuple(averageVelocity);
  
  logger.info("➡️ Avg. Velocity: {} = ( Final Position: {} - Initial Position: {} ) / ( Final Time: {} - Initial Time: {} )".format(pPrint(averageVelocity), pPrint(finalPosition), pPrint(initialPosition), pPrint(finalTime), pPrint(initialTime)));

  return averageVelocity;  
  
    

def averageVelocityFromTwoVelocities(initialVelocity, finalVelocity):

  if (len(initialVelocity) != len(finalVelocity)):
    sys.exit("❌ ERROR: Initial Velocity of {} has {} elements, but Final Velocity of {} has {} elements, so we can't know which components are which to find change in position.".format(initialVelocity, len(initialVelocity), finalVelocity, len(finalVelocity)));

  averageVelocity = [];

  component = "";

  for i in range(0, len(initialVelocity)):

    if (i == 0):
      component = "x";    
    elif (i == 1):
      component = "y";
    elif (i == 2):
      component = "z";
    else: 
      component = "";

    logger.debug("{}: ({} + {}) / 2".format(component, pPrint(finalVelocity[i]),  pPrint(initialVelocity[i])));
    logger.debug("{}: ({}) / 2".format(component, pPrint(finalVelocity[i] + initialVelocity[i])));
    logger.debug("{}: {}".format(component, pPrint((finalVelocity[i] + initialVelocity[i]) / 2)));
    averageVelocity.append((finalVelocity[i] + initialVelocity[i]) / 2);

  averageVelocity = tuple(averageVelocity);

  logger.info("➡️ Average Velocity: {} = (Final Velocity: {} + Initial Velocity: {}) / 2".format(pPrint(averageVelocity), pPrint(finalVelocity), pPrint(initialVelocity)));
  return averageVelocity;
  


  
# Depends on constant net force
# vf,y = vi,y + (net force / mass) * time
def finalVelocityComponent(initialVelocityComponent, netForce, mass, time): 
  vfc = initialVelocityComponent + (netForce / mass) * time;
  logger.info("➡️ final velocity component: {} = initial velocity component: {} + ( net force: {} / mass: {} ) * time: {}".format(pPrint(vfc), pPrint(initialVelocityComponent), pPrint(netForce), pPrint(mass), pPrint(time)));
  return vfc;

# depends on the only force being gravity
# vf,y = vi,y + force of gravity * time
def finalVelocityComponentFromGravity(initialVelocityComponent, gravityForce, time):
  # mass cancels out because 
  # final velocity component = initial velocity component + (net force divided by mass) times time
  # when net force = mass * -9.8 N/kg (Earth's gravity close to the surface)
  return finalVelocityComponent(initialVelocityComponent, -1.0 * gravityForce, 1.0, time);

# depends on constant net force, particularly of gravity
# Note: When the Y component of a velocity vector reaches 0, that is when a curve has reached the apex before going down
# In a system where gravity acts upon an object that has a positive Y component, the force slowly wears down the 
# Y component of the velocity until it is 0, then the net force begins to make the Y component NEGATIVE,
# until the position of the object is back to the surface of the Earth. 
# Therefore, the Y component of the velocity vectory is 0 at the max value of the Y component of the position vector (at max height)
# Starting with modified Momentum Principle: 1. vf,y = vi,y + (net force / mass) * time
# Fill in vf,y as 0 (final velocity of y is 0 at max height): 2. 0 = vi,y  - gravity * time 
# therefore: 3. time = vi,y / gravity
def timeOfMaxHeight(initialVelocityY, gravityForce):
  tMaxY = initialVelocityY / gravityForce;
  logger.info("time of max y: {} = initial velocity y: {} / force of gravity: {}".format(pPrint(tMaxY), pPrint(initialVelocityY), pPrint(gravityForce)));
  return tMaxY;

# assumes constant net force from gravity 
# initialPositionY: in meters, the Y component of the initial position vector, e.g. 5 from initial position vector of <0, 5, 2>
# initialVelocityY: in meters / second, the Y component of the initial velocity vector, e.g. 2 from initial velocity of <-1, 2, 0>
# gravityForce: in Newtons, the gravitational force for a given context (to be used as net force), e.g. -9.8 for the force of Earth's gravity on objects near the surface
# timeOfMaxY: in seconds, the point at which max height (the top Y value) is reached, based on timeOfMaxHeight() function.
def maxYFromTimeOfMaxY(initialPositionY, initialVelocityY, gravityForce, timeOfMaxY):
  maxY = initialPositionY + (initialVelocityY * timeOfMaxY) + (0.5 * (-1.0 * gravityForce)) * (timeOfMaxY**2); 
  logger.debug("Position of Max Height (Y): {} = initial position of Y: {} + (initial velocity Y: {} * time at which max height is reached: {}) + (1/2 * (-gravityForce: -{})) * time of max height squared: {}".format(pPrint(maxY), pPrint(initialPositionY), pPrint(initialVelocityY), pPrint(timeOfMaxY), pPrint(gravityForce), pPrint(timeOfMaxY**2)));
  logger.debug("Position of Max Height (Y): {} = initial position of Y: {} + (initial velocity Y * time max y): {} + (-1/2 * force of gravity): {} * time of max height squared: {}".format(pPrint(maxY), pPrint(initialPositionY), pPrint(initialVelocityY), pPrint(timeOfMaxY), pPrint(gravityForce), pPrint(timeOfMaxY**2)));
  logger.debug("Position of Max Height (Y): {} = initial position of Y: {} + (initial velocity Y * time max y): {} + (-1/2 * force of gravity * time max y squared): {}".format(pPrint(maxY), pPrint(initialPositionY), pPrint(initialVelocityY * timeOfMaxY), pPrint(-0.5 * gravityForce * timeOfMaxY**2)));
  logger.debug("Position of Max Height (Y): {} = initial position of Y: {} + (initial velocity Y * time max y + -1/2 * force of gravity * time max y squared): {}".format(pPrint(maxY), pPrint(initialPositionY), pPrint(initialVelocityY * timeOfMaxY + -0.5 * gravityForce * timeOfMaxY**2)));
  return maxY;
    


#  __  __                   _ _             _               __ 
# |  \/  | __ _  __ _ _ __ (_) |_ _   _  __| | ___    ___  / _|
# | |\/| |/ _` |/ _` | '_ \| | __| | | |/ _` |/ _ \  / _ \| |_ 
# | |  | | (_| | (_| | | | | | |_| |_| | (_| |  __/ | (_) |  _|
# |_|  |_|\__,_|\__, |_| |_|_|\__|\__,_|\__,_|\___|  \___/|_|  
#               |___/                                          
#  ____             _               _____                  
# / ___| _ __  _ __(_)_ __   __ _  |  ___|__  _ __ ___ ___ 
# \___ \| '_ \| '__| | '_ \ / _` | | |_ / _ \| '__/ __/ _ \
#  ___) | |_) | |  | | | | | (_| | |  _| (_) | | | (_|  __/
# |____/| .__/|_|  |_|_| |_|\__, | |_|  \___/|_|  \___\___|
#       |_|                 |___/                          
# The Magnitude of the Spring Force
# |Spring Force| = ks * |L - L0|
# stiffness: ks in Newtons per meter, e.g. 113 for 113N/m
# L0: relaxed spring length, in meters, e.g. 0.5 for 5cm
# L: elongated spring length, in meters, e.g. 0.67 for 67cm
def magnitudeOfSpringForce(stiffness, L0, L):
  f = stiffness * abs(L - L0);
  logger.info("magnitude of spring force: {}N = stiffness: {}N/m * |L: {}m - L0:{}m|: {}m".format(pPrint(f), pPrint(stiffness), pPrint(L), pPrint(L0), pPrint(abs(L - L0))));
  return f;
  

  
      
#  ____             _               _____                  
# / ___| _ __  _ __(_)_ __   __ _  |  ___|__  _ __ ___ ___ 
# \___ \| '_ \| '__| | '_ \ / _` | | |_ / _ \| '__/ __/ _ \
#  ___) | |_) | |  | | | | | (_| | |  _| (_) | | | (_|  __/
# |____/| .__/|_|  |_|_| |_|\__, | |_|  \___/|_|  \___\___|
#       |_|                 |___/                          
# __     __        _             
# \ \   / /__  ___| |_ ___  _ __ 
#  \ \ / / _ \/ __| __/ _ \| '__|
#   \ V /  __/ (__| || (_) | |   
#    \_/ \___|\___|\__\___/|_|   
# The Vector Spring Force
# force vector = -(ks) * s * unit vector L
# s = magnitude of vector L - L0
# stiffness: ks in Newtons per meter, e.g. 113 for 113N/m
# L0: relaxed spring length, in meters, e.g. 0.5 for 5cm
# magnitudeOfVectorL: in meters, e.g. 0.67 for 67cm
# unitVectorL: the unit vector of the spring, e.g. (1, 2, 3) for x = 1, y = 2, z = 3
def vectorSpringForce(stiffness, L0, magnitudeOfVectorL, unitVectorL):
  force = multiplyVectorByScalar(unitVectorL, -1.0 * stiffness * (magnitudeOfVectorL - L0));
  logger.info("➡️ spring force vector: {} N = -(ks): {} N/m * (magnitude of vector L: {} m - L0: {} m): {} m * unit vector L: {} m".format(pPrint(force), pPrint(stiffness * -1.0), pPrint(magnitudeOfVectorL), pPrint(L0), pPrint((magnitudeOfVectorL - L0)), pPrint(unitVectorL)));
  return force;
  

  
# Iterative Prediction of Motion with Non-constant Force:
# 1. calculate vector forces (to get net force vector)
# 2. update momentum 
# 3. update position
# 4. repeat
def iterativelyPredictMotionOfSpring(mass, startTime, endTime, timeBetweenSteps, initialPosition, initialVelocity, initialMomentum, stiffness, L0):
  currentTime = startTime;

  currentPosition = initialPosition;
  currentVelocity = initialVelocity;
  currentMomentum = initialMomentum;

  # We assume the only two forces acting on the block are:
  # 1. the variable force of the spring 
  # 2. the constant force of gravity on Earth
  fE = (0.0, -1.0 * approxEarthGravity(mass), 0.0); # force of Earth's gravity, expressed as a downward force on the Y component

  currentTime += timeBetweenSteps;

  while ( Decimal(currentTime) <= Decimal(endTime) or Decimal(currentTime) <= Decimal(endTime + 0.00000001) ):
    
    L = currentPosition[1];  
    logger.debug("L = current Y position: {}".format(pPrint(currentPosition[1])));

    unitVectorL = unitVector(currentPosition);    
    logger.debug("unit vector of L: {}".format(pPrint(unitVectorL)));
    
    # vector force of spring = negative stiffness * stretch (i.e. magnitude of vector L - L0) * unit vector L
    fSpring = vectorSpringForce(stiffness, L0, magnitudeOfVector(currentPosition), unitVectorL);

    logger.debug("Net Force:");
    netForce = addVectors(fE, fSpring);
    
    # final momentum = initial momentum + net force * change in time
    currentMomentum = finalMomentum(currentMomentum, netForce, timeBetweenSteps); 

    # average velocity = momentum / mass
    currentVelocity = velocityFromMomentum(currentMomentum, mass);

    currentPosition = finalPosition(currentPosition, currentVelocity, timeBetweenSteps);

    logger.debug("⚠️ At t of {}s: \nnet force: {} \nmomentum: {} \nvelocity: {} \nposition: {} \n\n".format(pPrint(currentTime), pPrint(netForce), pPrint(currentMomentum), pPrint(currentVelocity), pPrint(currentPosition)));

    currentTime += timeBetweenSteps;
    
    # logger.debug("endTime: {}, currentTime: {}, Decimal(endTime) - Decimal(currentTime): {} ".format(endTime, currentTime, Decimal(endTime) - Decimal(currentTime)));

#   ____                 _ _        _   _                   _ 
#  / ___|_ __ __ ___   _(_) |_ __ _| |_(_) ___  _ __   __ _| |
# | |  _| '__/ _` \ \ / / | __/ _` | __| |/ _ \| '_ \ / _` | |
# | |_| | | | (_| |\ V /| | || (_| | |_| | (_) | | | | (_| | |
#  \____|_|  \__,_| \_/ |_|\__\__,_|\__|_|\___/|_| |_|\__,_|_|
#                                                             
#  _____                  
# |  ___|__  _ __ ___ ___ 
# | |_ / _ \| '__/ __/ _ \
# |  _| (_) | | | (_|  __/
# |_|  \___/|_|  \___\___|
# the force of gravity on object 2 by object 1 = - G * (mass of object 2 * mass of object 1) / (the magnitude of the position vector from object 2 to 1)^2 * the unit vector of the position vector from object 2 to 1
def gravitationalForce(mass1, mass2, vector2to1):
  rhat = unitVector(vector2to1);
  rmag = magnitudeOfVector(vector2to1);

  fGrav = multiplyVectorByScalar(rhat, -1.0 * G * ((mass1 * mass2) / rmag**2));

  logger.info("➡️  The force of gravity on object 2 by object 1: {} N = G * (mass 1: {} kg * mass 2: {} kg) / (|r vector|: {})^2: {} * -(r unit vector): {}".format(pPrint(fGrav), pPrint(mass1), pPrint(mass2), pPrint(rmag), pPrint(rmag**2), pPrint(multiplyVectorByScalar(rhat, -1.0))));

  return fGrav;
  

# The magnitude of the gravitational force = G * (mass 1 * mass 2) / |vector r|^2
def magnitudeOfGravitationalForce(mass1, mass2, vector2to1):
  rmag = magnitudeOfVector(vector2to1);
  fGravMag = G * (mass1 * mass2) / rmag**2;
  
  logger.info("➡️  magnitude of gravitational force: {} N = G * (mass 1: {} kg * mass 2: {} kg) / (|vector r|: {} m)^2: {} m".format(pPrint(fGravMag), pPrint(mass1), pPrint(mass2), pPrint(rmag), pPrint(rmag**2)));
  return fGravMag;
  

# Fg = G * ((mass of planet * mass of object) / (radius of planet + distance of object above the surface of the planet)^2)
# g = G * (mass of planet / (radius of planet)^2)
def magnitudeOfTheGravitationalFieldOfPlanetSurface(mass, radius):
  g = G * (mass / radius**2);
  logger.info("➡️  g: {} = G * (mass: {} / (radius: {})^2: {})".format(pPrint(g), pPrint(mass), pPrint(radius), pPrint(radius**2)));
  return g;

  

#  _____ _            _____ _           _        _           _ 
# |_   _| |__   ___  | ____| | ___  ___| |_ _ __(_) ___ __ _| |
#   | | | '_ \ / _ \ |  _| | |/ _ \/ __| __| '__| |/ __/ _` | |
#   | | | | | |  __/ | |___| |  __/ (__| |_| |  | | (_| (_| | |
#   |_| |_| |_|\___| |_____|_|\___|\___|\__|_|  |_|\___\__,_|_|
#                                                              
#  _____                  
# |  ___|__  _ __ ___ ___ 
# | |_ / _ \| '__/ __/ _ \
# |  _| (_) | | | (_|  __/
# |_|  \___/|_|  \___\___|

# Coulomb's Law
# force vector of electricity on 2 by 1 = (1 / 4πε0) * ((q1 * q2) / |vector r|^2) * unit vector r
# vector2to1: the position vector from charge 1 to charge 2
def electricalForce(charge1, charge2, vector2to1):
  rmag = magnitudeOfVector(vector2to1);
  rhat = unitVector(vector2to1);
  fElect = multiplyVectorByScalar(rhat, (ke * ((charge1 * charge2) / rmag**2)));

  logger.info("➡️  vector force of electricity on 2 by 1: {} = k: {} * (q1: {} * q2: {}) / (|vector r|: {})^2: {} * unit vector: {}".format(pPrint(fElect), pPrint(ke), pPrint(charge1), pPrint(charge2), pPrint(rmag), pPrint(rmag**2), pPrint(rhat)));
  return fElect;
                        
# basically electrical force without the unit vector to provide direction
def magnitudeOfElectricalForce(charge1, charge2, vector2to1):
  rmag = magnitudeOfVector(vector2to1);
  fmag = ke * abs(charge1 * charge2) / rmag**2;
  logger.info("➡️  magnitude of electrical force on 2 by 1: {} = k: {} * |q1: {} * q1: {}|: {} / (|vector r|: {})^2: {}".format(pPrint(fmag), pPrint(ke), pPrint(charge1), pPrint(charge2), pPrint(abs(charge1 * charge2)), pPrint(rmag), pPrint(rmag**2)));
  return fmag;



#   ____           _                     __   __  __               
#  / ___|___ _ __ | |_ ___ _ __    ___  / _| |  \/  | __ _ ___ ___ 
# | |   / _ \ '_ \| __/ _ \ '__|  / _ \| |_  | |\/| |/ _` / __/ __|
# | |__|  __/ | | | ||  __/ |    | (_) |  _| | |  | | (_| \__ \__ \
#  \____\___|_| |_|\__\___|_|     \___/|_|   |_|  |_|\__,_|___/___/

def totalMass(listOfMasses):
  Mtotal = 0.0;
  totalMassExplanation = "";
  for i in range(0, len(listOfMasses)):

    totalMassExplanation += "m{}: {}".format(i, pPrint(listOfMasses[i]));
    if (i < len(listOfMasses)):
      totalMassExplanation += " + ";

    # Mtotal = m1 + m2 + mn...
    Mtotal += listOfMasses[i];
    
  logger.info("total mass: {} = {}".format(pPrint(Mtotal), totalMassExplanation));  
  return Mtotal;

  
# Center of Mass (position)
# position vector for center of mass = (mass 1 (m1) * position vector 1 (r1) + m2 * r2 + mn * rn ...) / total mass (i.e. m1 + m2 + mn ...)
def centerOfMass(listOfMasses, listOfPositionVectors):
  
  if (len(listOfMasses) != len(listOfPositionVectors)):
    sys.exit("❌ ERROR: The list of masses: {} has {} elements, but the list of position vectors: {} has {} elements. We can't calculate center of mass for a list of masses that don't map directly to position vectors (and vice versa)..".format(listOfMasses, len(listOfMasses), listOfPositionVectors, len(listOfPositionVectors)));

  # m1 + m2 + mn...
  Mtotal = totalMass(listOfMasses);

  # m1 * r1 + m2 * r2 + mn * rn ...
  totalMassPositions = [0.0, 0.0, 0.0];
  
  for i in range(0, len(listOfMasses)):
    totalMassPositions = list(addVectors(tuple(totalMassPositions), multiplyVectorByScalar(listOfPositionVectors[i], listOfMasses[i])));
    
  totalMassPositions = tuple(totalMassPositions);

  rCM = divideVectorByScalar(totalMassPositions, Mtotal);
  
  logger.info("position vector of the center of mass: {} = total sum of mass * positions: {} / total mass: {}".format(pPrint(rCM), pPrint(totalMassPositions), Mtotal));

  return rCM;
  
  
    
    
# Center of Mass (velocity)
# position vector for center of mass = (mass 1 (m1) * position vector 1 (r1) + m2 * r2 + mn * rn ...) / total mass (i.e. m1 + m2 + mn ...)
def velocityOfCenterOfMass(listOfMasses, listOfVelocityVectors):
  
  if (len(listOfMasses) != len(listOfVelocityVectors)):
    sys.exit("❌ ERROR: The list of masses: {} has {} elements, but the list of velocity vectors: {} has {} elements. We can't calculate center of mass for a list of masses that don't map directly to velocity vectors (and vice versa)..".format(listOfMasses, len(listOfMasses), listOfVelocityVectors, len(listOfVelocityVectors)));
  # m1 + m2 + mn...
  Mtotal = totalMass(listOfMasses);

  # m1 * v1 + m2 * v2 + mn * vn ...
  totalMassVelocity = [0.0, 0.0, 0.0];
  
  for i in range(0, len(listOfMasses)):
    totalMassVelocity = list(addVectors(tuple(totalMassVelocity), multiplyVectorByScalar(listOfVelocityVectors[i], listOfMasses[i])));
    
  totalMassVelocity = tuple(totalMassVelocity);

  vCM = divideVectorByScalar(totalMassVelocity, Mtotal);
  
  logger.info("velocity vector of the center of mass: {} = sum of each (mass * velocity): {} / total mass: {}".format(pPrint(vCM), pPrint(totalMassVelocity), pPrint(Mtotal)));

  return vCM;
  

# Momentum and the Center of Mass Velocity
# the momentum of a system = the total mass of the system * the velocity of the center of mass
# Note: this only holds true when speeds are small compared to the speed of light!
def momentumOfCenterOfMass(listOfMasses, listOfVelocityVectors):

  Mtotal = totalMass(listOfMasses);
  logger.info("total mass: {}".format(pPrint(Mtotal)));

  vCM = velocityOfCenterOfMass(listOfMasses, listOfVelocityVectors)
  logger.info("velocity of center of mass: {}".format(pPrint(vCM)));

  pSys = multiplyVectorByScalar(vCM, Mtotal); 

  logger.debug("the momentum of the system: {} = total mass: {} * the velocity of the center of mass: {}".format(pPrint(pSys), pPrint(Mtotal), pPrint(vCM)));

  return pSys;
    
# Effective Stiffness = force / stretch
# force of a heavy object hanging from a wire
# will be mass * Earth's gravitational constant
def stiffnessFromForceAndStretch(force, stretch):
  return force / stretch;

# force of tension (FT) = ks * stretch 
def forceOfTension(stiffness, stretch):
  return stiffness * stretch;

# 4.6 p.139
def stress(forceTension, area):
  return forceTension / area;

# 4.6 p.139
def strain(changeInLength, relaxedLength):
  return changeInLength / relaxedLength;

# 4.6 p.139
def youngsModulus(stress, strain):
  return stress / strain;

# 4.6 p.140
def youngsModulusFromAtomicQuantities(interatomicStiffness, interatomicDistance):
  return interatomicStiffness / interatomicDistance;

# ω = sqrt(ks / m)
# ω = 2π / T
def angularFrequency(stiffness, mass):
  return math.sqrt(stiffness / mass);

# 4.13 p.155 
# v = angular frequency * interatomic bond length 
def speedOfSoundInSolid(angularFrequency, lengthOfInteratomicBond):
  return angularFrequency * lengthOfInteratomicBond;


