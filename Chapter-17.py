#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys
import math
import re # regular expression
from decimal import *
from lib import *

# Chapter 17: Magnetic Field
# from Matter & Interactions (4th ed., 2015) 
# by Ruth W. Chabay and Bruce A. Sherwood



def Checkpoint1701():
  print "If 1.7 × 1016 electrons enter a light bulb in 4 milliseconds, what is the magnitude of the electron current at that point in the circuit?"
  e = 1.7e16 # electrons per 4ms
  t = 4e-3 # seconds
  
  print "{} electrons/second".format(pPrint(e/t))

  print "If the electron current at a particular location in a circuit is 12 × 1018 electrons/s, how many electrons pass that point in 7 minutes?"
  e = 12.0e18 # electrons per second
  t = 7.0 * 60.0 # 7 minutes in seconds
  print "{} electrons".format(pPrint(e * t))


def Problem18():
  print "To get an idea of the size of magnetic fields at the atomic level, consider the magnitude of the magnetic field due to the electron in the simple Bohr model of the hydrogen atom. In the ground state the Bohr model predicts that the electron speed would be 2.2 × 106 m/s, and the distance from the proton would be 0.5 × 10−10 m. What is B at the location of the proton?"
  v = 2.2e6 # speed of electron in meters / seconds
  r = 0.5e-10 # distance from proton in meters
  # Biot-Savart's law:
  # μ_0 / 4π  (q v x rhat) / rmag^2 
  B = 1.0e-7 * chargeOfProton * v / r**2
  print "{} T".format(pPrint(B))


def Problem19():
  print "An electron is moving horizontally to the right with speed 6 × 10^6 m/s. What is the magnetic field due to this moving electron at the indicated locations in the figure? Each location is 7 cm from the electron, and the angle 32°. Give both magnitude and direction of the magnetic field at each location."
  v = 6e6 # speed in m/s
  r = 7e-2 # distance from electron in meters
  theta = 32 # degrees
  # Biot-Savart's law:
  # μ_0 / 4π  (q v x rhat) / rmag^2 
  P1 = 1.0e-7 * (chargeOfElectron * v * math.sin(math.radians(theta))) / r**2
  print "P1 = {}".format(pPrint(abs(P1)))
  # the rest have the same magnitude, except P2 and P5 - those are 0 (sin(0) = 0)
  
def Problem21():
  print "A proton is moving upward with speed 4×10^6 m/s. What is the magnetic field due to this moving proton at the P1, P2, and P3 locations? Each location is a distance d = 1 cm from the proton, and the angle θ = 40°. Give both magnitude and direction of the magnetic field."
  v = 4.0e6 # speed in m/s
  r = 1.0e-2 # distance from proton in meters
  theta = 40 # degrees

  # Biot-Savart's law:
  # μ_0 / 4π  (q v x rhat) / rmag^2 
  P3 = 1.0e-7 * (chargeOfProton * v * math.sin(math.radians(theta))) / r**2

  print "|P1| = |P3| = {} T".format(pPrint(P3))


def Problem22():
  print "At a particular instant a proton is at the origin, moving with velocity <5 × 104, −7 × 104, −9 × 104> m/s."
  v = (5.0e4, -7.0e4, -9.0e4) # speed in m/s

  print "(a) At this instant, what is the electric field at location <5 × 10−3, 5 × 10−3, 5 × 10−3> m due to the proton? (Express your answer in vector form.)"
  r = (5.0e-3, 5.0e-3, 5.0e-3) # observation location, in meters

  E = multiplyVectorByScalar(unitVector(r), ke * chargeOfProton / magnitudeOfVector(r)**2)
  print "E = {} N/C".format(pPrint(E))

  print "(b) At this instant, what is the magnetic field at the same location due to the proton? (Express your answer in vector form.)"
  B = multiplyVectorByScalar(crossProduct(v, unitVector(r)), 1.0e-7 * chargeOfProton / magnitudeOfVector(r)**2)
  print "B = {} T".format(pPrint(B))





def Problem25():
  print "A conventional current of 12 A runs to the right in a horizontal metal wire."
  I = 12.0 # amperes
  print "(a) How many electrons pass some point in the wire per second?"
  # I = |q| i, where i is electrons / second, and q is charge
  i = I / chargeOfProton
  print "{} electrons / s".format(pPrint(i))

  print "(c) The electron mobility in this metal is 1.7 × 10−4 (m/s)/(N/C) and the electric field in the wire is 0.11 N/C. What is the average drift speed of the electrons?"
  u = 1.7e-4 # electron mobility in the metal in m/s / N/C
  E = 0.11 # electric field, in N/C
  v = u * E
  print "{} m/s".format(pPrint(v))


def Problem44():
  print "How much conventional current must you run in a solenoid with radius = 0.05 m and length = 0.32 m to produce a magnetic field inside the solenoid of 2 × 10−5 T, the approximate field of the Earth? The solenoid has 230 turns."
  R = 0.05 # radius, meters
  L = 0.32 # length of solenoid, in meters
  B = 2.0e-5 # magnetic field in Teslas
  N = 230.0 # turns in the solenoid


  # μ_0 / 4π = 1e-7
  muZero = 1.0e-7 * 4.0 * math.pi

  # Magnetic field along the centerline of a solenoid of length L:
  # B = μ_0 N I / L
  # I = B * L / μ_0 N
  I = (B * L) / (N * muZero)
  print "{} A".format(pPrint(I))


def Problem38():
  print "A long current-carrying wire, oriented North-South, lies on a table (it is connected to batteries which are not shown). A compass lies on top of the wire, with the compass needle about 3 mm above the wire. With the current running, the compass deflects 17 degrees to the West. At this location, the horizontal component of the Earth's magnetic field is about 2e-5 tesla."
  r = 3.0e-3 # meters
  theta = 17.0 # degrees
  Bearth = 2.0e-5 # teslas
  Bwire = math.tan(math.radians(theta)) * Bearth


  k = 1.0e-7 # μ_0 / 4π
  # Bwire = μ_0 / 4π * 2 * I / r
  # I = Bwire * r / (μ_0 / 4π * 2)
  I = (Bwire * r) / (k * 2.0)

  print "What is the magnitude of the magnetic field at location A, on the table top, a distance 2.6 cm to the East of the wire, due only to the current in the wire?"
  d = 2.6e-2 # distance in meters

  BwireA = k * 2.0 * I / d

  print "|B| = {} tesla".format(pPrint(BwireA))


def Problem40():
  print "Magnetic fields in the home"
  print "At one time, concern was raised about the possible health effects of the small alternating (60 hertz) magnetic fields created by electric currents, in houses and near power lines. In a house, most wires carry a maximum of 15 amperes (there are 15 ampere fuses that melt and break the circuit if this current is exceeded). The two wires in a home power cord are about s = 3 millimeters apart, as shown in the figure, and at any instant they carry currents in opposite directions (both of which change direction 60 times per second)."

  I = 15.0 # amperes
  s = 3.0e-3 # meters

  print "(a) Calculate the maximum magnitude of the alternating magnetic field, d = 63 cm away from the center of a long straight power cord that carries a current of 15 amperes. Both wires are at the same height as the observation location."
  d = 63.0e-2 # meters

  k = 1.0e-7 # μ_0 / 4π
  Bout = k * (2.0 * I) / (d - s / 2.0)
  print "Bout = {}".format(pPrint(Bout))
  Bin = k * (2.0 * I) / (d + s / 2.0)
  print "Bin = {}".format(pPrint(Bin))

  B = Bout - Bin 
  print "{} T".format(pPrint(B))


def Problem46():
  print "Wire with a loop in it"
  print "A very long wire carrying a conventional current of 2.3 amperes is straight except for a circular loop of radius 6.7 cm (see the figure). Calculate the approximate magnitude and the direction of the magnetic field at the center of the loop."
  I = 2.3 # amperes
  R = 6.7e-2 # radius in meters
  # Magnetic Field of a Loop:
  # Bloop = μ_0 / 4π * (2πR^2 I) / (z^2 + R^2)^(3/2)

  # Magnetic Field at the center of a loop:
  # B = μ_0 / 4π * (2π I) / R
  Bloop = 1.0e-7 * (2.0 * math.pi * I) / R 
  Bwire = 1.0e-7 * (2.0 * I) / R 
  B = Bwire + Bloop
  print "magnitude |B| = {} T".format(pPrint(B))

  print "direction: out of the page ⊙"
  
  


def Problem48():
  print "A thin circular coil of radius r = 20 cm contains N = 3 turns of Nichrome wire. A small compass is placed at the center of the coil, as shown in the figure below. With the battery disconnected, the compass needle points to the right, in the plane of the coil. Assume that the horizontal component of the Earth's magnetic field is about BEarth = 2 × 10−5 T."
  r = 20.0e-2 
  N = 3.0
  Bearth = 2.0e-5 # tesla

  print "When the battery is connected, a current of 0.18 A runs through the coil. Predict the deflection of the compass needle. If you have to make any approximations, state what they are."
  print "Is the deflection outward or inward as seen from above?"
  I = 0.18 # amperes

  # Bloop = μ_0 / 4π * (2π N I) / r
  Bloop = 1.0e-7 * 2.0 * math.pi * N  * I / r
  print "inward"

  theta = math.degrees(math.atan(Bloop / Bearth))
  print "What is the magnitude of the deflection?"
  print "{} °".format(pPrint(theta))


def Problem50():
  print "Two thin coils of radius R = 2 cm are d = 26 cm apart and concentric with a common axis. Both coils contain 10 turns of wire with a conventional current of I = 3 amperes that runs counter-clockwise as viewed from the right side (see the figure)."
  R = 2.0e-2
  d = 26e-2
  N = 10.0
  I = 3.0
  
  print "(a) What is the magnitude and direction of the magnetic field on the axis, halfway between the two loops, without making the approximation z >> r? (For comparison, remember that the horizontal component of magnetic field in the United States is about 2 × 10-5 tesla)."
  Bearth = 2.0e-5

  # Magnetic Field of a Loop:
  # Bloop = μ_0 / 4π * (2πR^2 I) / (z^2 + R^2)^(3/2)
  Bloopleft = 1.0e-7 * (2.0 * math.pi * N * R**2 * I) / ((d/2.0)**2 + R**2)**(3.0/2.0)

  print "magnitude |B| = {} T".format(pPrint(Bloopleft * 2.0))
  print "direction: to the right"

  print "(b) In this situation, the observation location is not very far from either coil. Calculate the magnitude of the magnetic field at the same location, using the 1/z^3 approximation."

  Bapprox = 1.0e-7 * (2.0 * math.pi * N * R**2 * I) / (d/2.0)**3

  print "|B approx| = {} T".format(pPrint(Bapprox * 2.0))


  print "The percent error of an approximate result can be found by 100 * |1 - approx / exact|. What percentage error results if you calculate the magnetic field using the approximate formula for a current loop instead of the exact formula?"
  error = 100.0 * abs(1.0 - Bapprox / Bloopleft)
  print "{} % error".format(pPrint(error))

  print "(c) What is the magnitude and direction of the magnetic field midway between the two coils if the current in the right loop is reversed to run clockwise?"
  # they're opposite and equal
  print "magnitude 0"
  
  


def Checkpoint1706():
  print "What is the magnetic dipole moment of a 3900 turn rectangular coil that measures 3 cm by 9 cm and carries a current of 1 amperes?"
  N = 3900.0 # number of turns in coil
  A = 3.0e-2 * 9.0e-2 # area in meters squared
  I = 1.0 # current in amperes
  
  # Magnetic Dipole Moment: 
  # μ = N I A
  mu = N * I * A
  print "{} A m^2".format(pPrint(mu))

def Problem58():
  print "A particular Alnico (aluminum, cobalt, nickel, and iron) bar magnet (magnet A) has a mass of 10 g. It produces a magnetic field of magnitude 7 × 10-5 T at a location 0.19 m from the center of the magnet, on the axis of the magnet."
  m1 = 10e-3 # mass in kg
  B1 = 7.0e-5 # magnetic field in teslas
  r1 = 0.19 # distance in meters
  # Bmagnet = μ_0 / 4π * 2μ/r^3
  mu = (B1 * r1**3) / (2.0 * 1.0e-7)
  print "(a) Approximately what is the magnitude of the magnetic field of magnet A a distance of 0.38 m from the center of the magnet, along the same axis?"
  r2 = 0.38 # distance in meters
  B2 = 1.0e-7 * (2.0 * mu) / r2**3
  print "|B_A| = {} T".format(pPrint(B2))

  print "(b) If you removed the original magnet, and replaced it with a magnet made of the same material, but with a mass of 20 g (magnet B), approximately what would be the magnetic field at a location 0.19 m from the center of the magnet, on the axis of the magnet?"
  m2 = 20.0e-3 # mass in kg
  r3 = 0.19 # distance in meters
  B3 = 1.0e-7 * (2.0 * (m2 / m1) * mu) / r3**3
  
  print "|B_B| = {} T".format(pPrint(B3))


def Problem54():
  print "In the region shown in the diagram, the magnitude of the horizontal component of the Earth's magnetic field is about 2e-5 T. Originally a compass placed at location A points North. Then a bar magnet is placed at the location shown in the diagram, with its center 20 cm from location A. With the magnet present, the compass needle points 75 degrees West of North. (Take North to be toward the top of the page.)"
  Bearth = 2.0e-5 # horizontal component of Earth's magnetic field, in teslas
  r = 20.0e-2 # distance from A, in meters
  theta = 75 # degrees

  print "What is the magnetic dipole moment of the bar magnet?" 
  # Magnetic Dipole Moment: 
  # μ = I A

  B = math.tan(math.radians(theta)) * Bearth 

  # Bmagnet = μ_0 / 4π * 2μ/r^3
  mu = (B * r**3) / (2.0 * 1.0e-7)
  print "μ = {} A m^2".format(pPrint(mu))
  
  
def Problem55():
  print "A bar magnet with magnetic dipole moment <4, 0, 0> A · m2 is located at the origin. A second bar magnet with magnetic dipole moment <μ, 0, 0> is located at <0.3, 0.2, 0> m. Calculate the value of μ that makes the magnetic field at <0.3, 0, 0> m be zero."
  mu1 = (4.0, 0.0, 0.0)
  r1 = (0.0, 0.0, 0.0)
  r2 = (0.3, 0.2, 0.0)
  obs = (0.3, 0.0, 0.0)
  
  # B magnet, perpendicular = μ_0 / 4π * 2.0 * μ/r^3
  B1 = 1.0e-7 * 2.0 * magnitudeOfVector(mu1) / magnitudeOfVector(subtractVectors(obs, r1))**3
  print "B magnet,1 = {} T".format(pPrint(B1))
  
  mu2 = 2.0 * B1 * (magnitudeOfVector(subtractVectors(obs, r2))**3) / (2.0 * 1.0e-7)
  print "{} A m^2".format(pPrint(mu2))
  

def Problem57():
  print "A bar magnet with magnetic dipole moment 0.46 A · m2 lies on the negative axis, as shown in the figure below. A compass is located at the origin. Magnetic north is in the negative direction. Between the bar magnet and the compass is a coil of wire of radius 3.5 cm, connected to batteries not shown. The distance from the center of the coil to the center of the compass is 8.5 cm. The distance from the center of the bar magnet to the center of the compass is 22.4 cm. A steady current of 0.96 A runs through the coil. Conventional current runs clockwise in the coil when viewed from the location of the compass. (Assume the approximation for the coil applies.)"
  mumag = 0.46 # A * m^2
  R = 3.5e-2 # radius in meters
  rmagnet = 22.4e-2 # meters
  rcoil = 8.5e-2 # meters
  
  I = 0.96 # amps
  
  # |B magnet| = |B coil| 
  mucoil = (rcoil**3 / rmagnet**3) * mumag
  
  # μ coil = N * I * A 
  A = math.pi * R**2
  # N = μ coil / I * A
  N = mucoil / (I * A)
  print "(c) How many turns of wire are in the coil?"
  print "{}".format(pPrint(N))
  

  
# 17.1 - 17.4
Checkpoint1701()
Problem18()
Problem19()
Problem21()
Problem22()

# 17.5 - 17.8
Problem25()
Problem44()
Problem38()
Problem40()
Problem46()
Problem48()
Problem50()


# 17.9 - 17.12
Checkpoint1706()
Problem58()
Problem54()
Problem55()
Problem57()
