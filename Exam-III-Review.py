#!/usr/bin/python
# This Python file uses the following encoding: utf-8
import logging
import sys
import math
import re # regular expression
from decimal import *
from lib import *

#################
def Question_1():
  print "A swimmer underwater shines a flashlight upward toward a glass bottom boat at an angle of 26.0 degrees from the normal. What is the angle of the refracted ray? Use 1.333 for water's refractive index and 1.52 for glass's refractive index."
  theta1 = 26.0 # degrees
  n_water = 1.333
  n_glass = 1.52
  # n_water sin(theta1) = n_glass sin(theta2)
  # theta2 = arcsin(n_water sin(theta1) / n_glass)
  theta2 = math.asin(n_water * math.sin(math.radians(theta1)) / n_glass)
  print "{} degrees".format(pPrint(math.degrees(theta2)))
  print "✓ (d) 22.6 degrees"


#################
def Question_2():
  # same as problem 21.06 in the homework
  print "The figure shows a disk-shaped region of radius 2.00 cm, on which there is a uniform electric field of magnitude 290.0 V/m at an angle of θ = 40.0 degrees to the plane of the disk."
  r = 2.0e-2 # radius in meters
  E = 290.0 # electric field in Volts / meter, or Newtons / Coulomb
  theta = 90.0 - 40.0 # degrees
  A = math.pi * r**2 # area of the disk-shaped region in meters squared
  # E⊥ = E cos(θ) 
  Eperp = E * math.cos(math.radians(theta))
  
  print "What is the electric flux on the disk?"
  # diagram: https://imgur.com/a/yQYflVl

  # electric flux on a surface =  ϕ_el
  # ϕ_el = ∫ E * n * ΔA

  flux = Eperp * A
  print "{} Vm".format(pPrint(flux))
  print "✓ (a) 0.234 Vm"


#################
def Question_3():
  print "A magnetic field near the ceiling points up and is increasing. Looking up at the ceiling, does the non-Coulomb electric field curl clockwise or counter-clockwise?"
  # the non-Coulomb electric field follows the right-hand rule,
  # thumb points the direction opposite of the change in magnetic field
  # magnetic field here is changing in the +y direction, 
  # so thumb points down - fingers curl counter-clockwise
  print "✓ counterclockwise"


#################
def Question_4():
  print "Along the path shown in the figure the magnetic field is measured and is found to be uniform in magnitude and always tangent to the circular path. If the radius of the path is 0.0300 m and B along the path is 1.60 x 10^-6 T, the conventional current enclosed by the path is:"
  r = 0.03 # radius in meters
  B = 1.6e-6 # magnetic field in teslas

  # diagram: https://imgur.com/a/US3I4xq

  # by process of elimination, we know current must flow in the -z direction
  # leaving the only option:
  print "✓ <0, 0, -0.24> A"

  # however, we also know:
  # B_wire = μ_0 / 4π * 2I / r
  # so:
  # I = B_wire * r / (2 * μ_0 / 4π)
  I = B * r / (2.0e-7)
  print "|I| = {} A.".format(pPrint(I))


#################
def Question_5():
  # same as homework problem 22.39
  print "A transformer has a primary coil with 124 turns and a secondary coil of 360 turns. The AC voltage across the primary coil has a maximum of 122 V and the AC current through the primary coil has a maximum of 4 A. What is the maximum value of the voltage?"
  N1 = 124.0 # turns in first coil
  N2 = 360.0 # turns in second coil
  emf1 = 122.0 # AC voltage in primary coil, in volts
  I1 = 4.0 # AC current through primary coil in amps

  # see "Transformers" pg. 920, figure 22.42
  emf2 = (N2/N1) * emf1

  print "voltage {} V".format(pPrint(emf2))
  print "✓ (b) 354 V"

  # and to find max current of the second coil:
  I2 = (N1/N2) * I1
  # print "current {} A".format(pPrint(I2))
  


##################
def Question_6():
  # same as checkpoint 22.03
  print "A uniform magnetic field of 2.20 tesla points 30.0° away from the perpendicular to the plane of the rectangular loop of wire 0.100 m by 0.200 m (see the figure). What is the magnetic flux on this loop?"
  # diagram: https://imgur.com/a/1Q6qcIp
  B = 2.2 # teslas
  theta = 30.0 # degrees
  A = 0.1 * 0.2
  # ϕ_magnetic = integral of B_perp dA
  flux = B * math.cos(math.radians(theta)) * A
   
  print "{} T * m^2".format(pPrint(flux))
  print "✓ (a) 0.0381 Wb"


###################
def Question_7():
  print "Electromagnetic radiation is moving to the right, while the electric field points out of the page and has a magnitude of E = 3100 N/C. What is the direction and magnitude of the corresponding magnetic field?"
  E = 3100.0
  # B_radiative = E_radiative / c
  B = E / c
  print "{} T".format(pPrint(B))
  
  # propagation, electric field, and magnetic field are all pointing perpendicular to one another
  # so, by right hand rule:
  # the thumb points the direction of propagation, +x to the right
  # the index finger points the direction of electric field, +z out of the page (towards you)
  # the middle finger is left to point perpendicular to the index finger and thumb, pointing down, -y
  print "✓ (c) -y; 1.03 x 10^-5 T"
  

###################
def Question_8():
  # see homework problem 20.69 for a similar problem setup
  print "A very long wire carries a current I_1 upward as shown in the figure below and this current is decreasing with time. Nearby is a rectangular loop of wire lying in the plane of the long wire, and containing a resistor R; the resistance of the rest of the loop is negligible compared to R. How does the current in the loop run?"
  # diagram: https://imgur.com/a/GYiFFHL

  # magnetic field from the long wire curls in the -z direction to the right of the wire
  # because the current is decreasing, the magnetic field is decreasing as well
  # this means the change in current points against the direction of the magnetic field, in the +z direction
  # there is a non-Coulomb curly electric field 
  # generated by this decreasing magnetic field 
  # by the right-hand rule, the thumb points in the direction of the opposite of the change
  # since the magnetic field is decreasing, the thumb points the direction of the magnetic field - in the -z direction (into the page)
  # and the electric field curls clockwise, 
  # and electrons are pushed the opposite of that electric field, 
  # so the current is clockwise
  print "✓ clockwise"
  
  
  
###################
def Question_9():
  print "Which of the following lenses is converging?"
  # diagram: https://imgur.com/a/KUX4a7c
  print "✓ (c) b, d and e" 



###################
def Question_10():
  # see homework problem 23.22
  print "A particular AM radio station broadcasts at a frequency of 1070 kilohertz. What is the wavelength of this electromagnetic radiation?"
  f = 1070.0e3 # frequency in hertz
  # frequency is 
  # frequency = 1 / period (T)
  # c = λf
  # λ = c / f
  wavelength = c / f

  print "{} m".format(pPrint(wavelength))
  print "✓ (b) 280 m"
  


###################
def Problem_1():
  print "A proton is briefly accelerated as shown in the figure below. r indicates the location of an observation location relative to the initial position of the proton."
  # diagram: https://imgur.com/a/VlBVV0u
    
  print "(a) If θ is 34°, and the magnitude of the acceleration is 3.4 x 10^17 m/s^2, what is the magnitude of a⊥?"
  theta = 34.0
  a = 3.4e17
  # a⊥ would be opposite of the angle, so we need to use sin
  a_perp = math.sin(math.radians(theta)) * a

  print "|a⊥| = {} m/s^2".format(pPrint(a_perp))
  # 1.9e17 

  print "(b) What is the magnitude of the radiative electric field at the indicated observation location, if the magnitude of r is 0.023 m?"
  r = 0.023 # meters
  # Producing a Radiative Electric Field
  # E_radiative = (1/4πε_0) * (-qa⊥ / c^2 r)
  E_radiative = (1.0 / (4.0 * math.pi * vacuumPermittivity)) * ((chargeOfProton * a_perp) / (c**2 * r))
  print "{} N/C".format(pPrint(E_radiative))
  # 1.32e-7


  print "(c) What is the direction of this radiative electric field? Use the conventional coordinate designations."
  # see pg. 949, figure 23.21 in 23.3
  print "E_radiative is in the +x direction, to the right"
  




#######################
def Problem_2():
  # same as homework problem 21.10, part (c)
  print "The electric field has been measured to be horizontal and to the right everywhere on the closed box shown in the figure. All over the left side of the box E1 = 80.0 V/m, and all over the right, slanting, side of the box E2 = 400.0 V/m. On the top the average field is E3 = 110.0 V/m, on the front and back the average field is E4 = 150.0 V/m, and on the bottom the average field is E5 = 190.0 V/m."
  # diagram: https://imgur.com/a/3IH9X71

  E1 = 80.0 # V/m
  E2 = 400.0 
  A1 = 4.0e-2 * 5.0e-2 # the area of the rectangle formed by the back left of the figure
  A2 = 12.0e-2 * 5.0e-2 # the area of the slanted rectangle to the right of the figure

  # based on angle of E2 to nhat which is perpendicular to A2
  theta = 90.0 - math.degrees(math.asin(4.0e-2 / 12.0e-2)) # in degrees

  Q = ((E2 * math.cos(math.radians(theta)) * A2) - (E1 * A1)) * vacuumPermittivity
  
  
  print "How much charge is inside the box?"
  print "{} C".format(pPrint(Q))
  # 5.66e-12 




#######################
def Problem_3():
  # same as homework problem 22.26
  print "In the figure below a very small circular metal ring of radius r = 0.5 cm and resistance x = 5.0 Ω is at the center of a large concentric circular metal ring of radius R = 50.0 cm. The two rings lie in the same plane."
  # figure: https://imgur.com/a/bsARtu5
  r = 0.5e-2 # little ring's radius in meters
  R = 50.0e-2 # large ring's radius in meters
  x = 5.0 # resistance in ohms

  print "At t = 6.0 s, the large ring carries a clockwise current of 3.0 A. At t = 6.3 s, the large ring carries a counterclockwise current of 6.0 A."
  t0 = 6.0 
  I0 = 3.0 
  t1 = 6.3 
  I1 = 6.0 
  
  
  print "(a) What is the magnitude of the emf in the small ring during this time?" 
  # B = μ_0 / 4π * (2π I) / R
  B0 = 1.0e-7 * (2.0 * math.pi * I0) / R
  B1 = 1.0e-7 * (2.0 * math.pi * I1) / R
  print "B0 = {}".format(pPrint(B0))
  print "B1 = {}".format(pPrint(B1))
  A = math.pi * r**2 # area of the small ring in meters squared
  print "A = {}".format(pPrint(A))
  deltaB = B1 + B0
  print "delta B = {}".format(pPrint(deltaB))
  deltaT = t1 - t0
  print "delta T = {}".format(pPrint(deltaT))
  # emf = - dϕ / dt
  emf = (deltaB * A) / deltaT
  print "{} V".format(pPrint(emf))
  # 2.96e-9 V


  print "(b) What are the magnitude and direction of the average current in the small ring?"
  # emf = I R
  # I = emf / R
  I = emf / x
  print "magnitude {} A".format(pPrint(I))
  # 5.92e-10 A

  print "direction: clockwise"




#######################
def Problem_4():
  print "A real object is placed 6 cm in front of a diverging lens with a focal length of 4 cm. Mathematically determine the location and the properties of the image. There is no need to convert."
  d_o = 6.0e-2 # distance between real object and diverging lens
  f = -4.0e-2 # focal length, negative because it's diverging 

  # from 23.9:
  # thin lens equation:
  # 1/f = 1/d1 + 1/d2
  # 1/d2 = 1/f - 1/d1
  d_i = 1.0 / (1.0/f - 1.0/d_o)
  # d_i is the distance between the lens and the image (created by the object going through the lens)
  print "distance between lens and image: {} m".format(pPrint(d_i))
  # -2.4 cm
  
  # because the distance between the lens and image is negative, 
  # it is to the left and thus on the same side as the object
  # this makes the image virtual

  print "the image is virtual"

  # Magnification:
  # M = -d_i / d_o = h_i / h_o

  M = -d_i / d_o
  print "Magnification (M) = {}".format(pPrint(M))
  # 0.4

  # if the magnification is positive, the image is upright
  # otherwise if magnification is negative, the image is inverted

  print "the image is upright"

  # if the magnification is > 1, the image is enlarged
  # otherwise if magnification is < 1, the image is reduced

  print "the image is reduced"


######################
def Problem_5():
  print "Consider the system below. How many times larger is the second refractive index compared to the first refractive index?"
  # diagram: https://imgur.com/a/rwzIp4
  theta_1 = 90.0 - 41.4 # degrees
  theta_2 = 90.0 - 55.0 # degrees
  
  # see 23.8
  # Snell's Law:
  # n1 sin(theta_1) = n2 sin(theta_2)
  # n2 / n1 = sin(theta_1) / sin(theta_2)

  proportionality = math.sin(math.radians(theta_1)) / math.sin(math.radians(theta_2))
  print "{} times".format(pPrint(proportionality)) 
  # 1.31 times larger
  





# Question_1()
# Question_2()
# Question_3()
# Question_4()
# Question_5()
# Question_6()
# Question_7()
# Question_8()
# Question_9()
# Question_10()

# Problem_1()
# Problem_2()
# Problem_3()
# Problem_4()
Problem_5()

